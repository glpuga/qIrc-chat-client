/***************************************************************************
                          resource.h  -  description
                             -------------------
    begin                : mi� sep 13 10:25:05 ART 2000
    copyright            : (C) 2000 by gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RESOURCE_H
#define RESOURCE_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

// version del programa
#define CTCP_VERSION_REPLY					"Q-Irc Ver. " VERSION " for Linux"

// sobre el idioma
#define IDIOMA "English"

// NO HABILITAR ESTO!!!! TOTALMENTE EXPERIMENTAL E INESTABLE!!
// #define ENABLE_QT2 // habilita los cambios para compatibilizar con QT2.1.0

// constantes de pantalla
#define ALTO_CUADRO_ENTRADA 25
#define ANCHO_LISTA_NICKS 	140

#define MARCO 							10
#define DISTANCIA 					5
#define ALTO_LABEL					20
#define ALTO_ENTRADA				ALTO_LABEL
#define ANCHO_BOTON					100
#define ALTO_BOTON					30
#define ANCHO_CHECKBOX 			200
#define ALTO_CHECKBOX 			ALTO_LABEL
#define ALTO_BARRA_PROGRESO 20
#define ALTO_TAB						30

// etiquetas del archivo de configuracion
#define CONFIG_MAINWIDTH					"mainwidth"
#define CONFIG_MAINHEIGHT					"mainheight"
#define CONFIG_HOSTPORDEFECTO 		"hostpordefecto"
#define CONFIG_PUERTOPORDEFECTO		"puertopordefecto"
#define CONFIG_NICK1							"nick1"
#define CONFIG_NICK2							"nick2"
#define CONFIG_USER								"user"
#define CONFIG_AUTOUSER						"autouser"
#define CONFIG_USERINFO						"userinfo"
#define CONFIG_PASS								"pass"
#define CONFIG_USARPASS						"usarpass"
#define CONFIG_FLAGS							"flags"
#define CONFIG_SERVIDOR_H 				"servidorh"
#define CONFIG_SERVIDOR_C 				"servidorc"
#define CONFIG_SERVIDOR_P 				"servidorp"
#define CONFIG_CANTSERVIDORES 		"cantservidores"
#define CONFIG_IGNORELISTCOUNT		"ignorelistcount"
#define CONFIG_IGNOREDNICK				"ignorednick"
#define CONFIG_AWAYMESSAGE				"awaymessage"
#define CONFIG_BORRARMIRC					"borrarmirc"
#define CONFIG_FONT1							"font1"
#define CONFIG_FONTSIZE1					"fontsize1"
#define CONFIG_LISTENCONECCION		"listenconeccion"
#define CONFIG_QUITMESSAGE				"quitmessage"
#define CONFIG_PARTMESSAGE				"partmessage"
#define CONFIG_INVISIBLE					"invisibleinconn"
#define CONFIG_WALLOPS						"receivewallops"
#define CONFIG_VERMOTD						"vermotd"
#define CONFIG_RECONNECT					"autoreconnect"
#define CONFIG_CTCPFINGER					"ctcp_finger"
#define CONFIG_CTCPUSERINFO				"ctcp_userinfo"
#define CONFIG_MAXLINEASREPETIDAS "maxlineasrepetidas"
#define CONFIG_VERCTCP						"verctcp"
#define CONFIG_RESPONDERCTCPVERSION "responderctcpversion"
#define CONFIG_CTCPINTERVAL				"ctcpinterval"
#define CONFIG_VACIARIGNORELIST   "vaciarignorelist"
#define CONFIG_ELEVARPRIVADOS			"elevarprivados"
#define CONFIG_LOGCHANNELS		   	"logchannels"
#define CONFIG_LOGPRIVATES		   	"logprivates"
#define CONFIG_BORRARMIRCLOGS			"borrarmirclogs"
#define CONFIG_DCCCONNECTTIMEOUT	"dccconnecttimeout"
#define CONFIG_DCCTRANSFERTIMEOUT	"dcctransfertimeout"
#define CONFIG_DCCBLOCKSIZE				"dccblocksize"
#define CONFIG_DCCMAXWINDOWSCOUNT	"dccmaxwindowscount"
#define CONFIG_NOTIFIESLISTCOUNT	"notifieslistcount"
#define CONFIG_NOTIFYNICK					"notifynick"
#define CONFIG_PAUSANOTIFY				"pausanotify"

// valores por defecto de la configuraci�n
#define DEF_MAINWIDTH							600
#define DEF_MAINHEIGHT						400
#define DEF_HOSTPORDEFECTO 				"chat.clarin.com.ar"
#define DEF_PUERTOPORDEFECTO			6667
#define DEF_NICK1									"nick1"
#define DEF_NICK2									"nick2"
#define DEF_USER									"user"
#define DEF_AUTOUSER							true
#define DEF_USERINFO						  "userinfo"
#define DEF_PASS						  		""
#define DEF_USARPASS							false
#define DEF_FLAGS									0
#define DEF_AWAYMESSAGE						"Currently I'm not at the computer, call me in five minutes"
#define DEF_BORRARMIRC						false
#define DEF_FONT1									"fixed"
#define DEF_FONTSIZE1							12
#define DEF_LISTENCONECCION				false
#define DEF_QUITMESSAGE						""
#define DEF_PARTMESSAGE						""
#define DEF_INVISIBLE							false
#define DEF_WALLOPS								true
#define DEF_VERMOTD								true
#define DEF_RECONNECT							false
#define DEF_CTCPFINGER						""
#define DEF_CTCPUSERINFO					""
#define DEF_MAXLINEASREPETIDAS    0
#define DEF_VERCTCP								true
#define DEF_RESPONDERCTCPVERSION  true
#define DEF_CTCPINTERVAL					0
#define DEF_VACIARIGNORELIST   		false
#define DEF_ELEVARPRIVADOS				false
#define DEF_LOGCHANNELS		   			true
#define DEF_LOGPRIVATES		   			true
#define DEF_BORRARMIRCLOGS				true
#define DEF_DCCCONNECTTIMEOUT			180
#define DEF_DCCTRANSFERTIMEOUT		60
#define DEF_DCCBLOCKSIZE					2048
#define DEF_DCCMAXWINDOWSCOUNT		0
#define DEF_PAUSANOTIFY						60

//  CONSTANTES DE PREFIJOS
#define PREFIJO_NICK				 			"+++"
#define PREFIJO_TOPIC     				"(topic("
#define PREFIJO_NOTIFY						"-NOTIFY-"
#define PREFIJO_ACTION						"*"
#define PREFIJO_ENTRAR_CANAL     	"-->"
#define PREFIJO_PART							"<--"
#define PREFIJO_KICK   						"<<-"
#define PREFIJO_QUIT  						"<<<"
#define PREFIJO_MENSAJE_SERVIDOR 	"*"
#define PREFIJO_AVISOPROGRAMA   	">"
#define PREFIJO_MOTD							"-"
#define PREFIJO_MODE							")mode("
#define PREFIJO_INVITE						"|invite|"
#define PREFIJO_ERROR							"|close|"
#define PREFIJO_AWAY							"|away|"
#define PREFIJO_COMANDO   				"---"
#define PREFIJO_CTCP	          	")ctcp("
#define PREFIJO_AYUDA							""
#define PREFIJO_DCCSEND						"DCC Send"

// CONSTANTES DE COLORES
#define COLOR_ENTRADASySALIDAS		12
#define COLOR_NICKSyMODESyTOPIC		2
#define COLOR_INVITES							7
#define COLOR_CTCPQUERY						6
#define COLOR_CTCPREPLY						7
#define COLOR_PROGRAMMESSAGE		  2
#define COLOR_COMANDO							14
#define COLOR_SERVERMESSAGE				11
#define COLOR_ERROR								7
#define COLOR_AWAY								3
#define COLOR_ACTION							7
#define COLOR_NOTICE							3
#define COLOR_DCC									7

#define DEFAULT_FOREGROUND 1  // negro
#define DEFAULT_BACKGROUND 0  // blanco

// CONSTANTES GENERALES
#define ARCHIVO_CONFIGURACION ".qircconfig"
#define CARPETA_LOGS					"Chatlogs"
#define HOME_ENVVAR						"HOME"
#define LOGFILEPREFIX					"log_"
#define DCCBASEPORT						5554
#define DCCPORTSCANMAX				30

// constantes de los codigos del mirc
#define MIRC_CTRL_B		2
#define MIRC_CTRL_U		31
#define MIRC_CTRL_R		22
#define MIRC_CTRL_O		15
#define MIRC_CTRL_K		3

// CONSTANTES DE TIPOS DE DATOS
#define U_4BYTES_INT	unsigned int

///////////////////////////////////////////////////////////////////
// General application values
#define IDS_APP_ABOUT               "Q-Icq\nVersion " VERSION \
                                    "\n(w) 2000 by gerardo Puga"
#define IDS_STATUS_DEFAULT          "Ready."

#endif // RESOURCE_H

