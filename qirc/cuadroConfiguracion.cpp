/***************************************************************************
                          cuadroConfiguracion.cpp  -  description
                             -------------------
    begin                : Tue Oct 10 2000
    copyright            : (C) 2000 by gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cuadroConfiguracion.h"

// esta variable est� definida en tellme.cpp
extern Config *config;

cuadroConfiguracion::cuadroConfiguracion() : QWidget(NULL, NULL, WStyle_Customize | WStyle_DialogBorder)
{
 // tama�o y t�tulo
 setFixedSize(ANCHO_CONFIGURACION, ALTO_CONFIGURACION);
 setCaption("QIrc Setup");

 // creo el panel
 crearPanel();

 // creo los widgets de cada rama del panel
 crearHojas();

 // por �ltimo pongo los botones de aceptar, restaurar y cancelar
 crearBotonera();

 // Y me muestro al mundo
 show();
}

void cuadroConfiguracion::crearPanel()
{
 // creo los controles
 panel = new QListView(this);
 panel->setGeometry(MARCO, MARCO, ANCHO_PANEL_CONFIGURACION, ALTO_CONFIGURACION - 3 * MARCO - ALTO_BOTON);
 panel->addColumn("Panel", panel->width());
 panel->setRootIsDecorated(true);
 panel->setSorting(-1);
 QListViewItem *raiz = new QListViewItem(panel, "Setup");
 raiz->setOpen(true);
 // creo los items en orden inverso
 itemVarios = new QListViewItem(raiz, "Misc");
 itemLogging  = new QListViewItem(raiz, "Log");
 itemDisplay = new QListViewItem(raiz, "Display");
 itemCanales = new QListViewItem(raiz, "Channels");
 itemDcc     = new QListViewItem(raiz, "DCC");
 itemCtcp  = new QListViewItem(raiz, "CTCP");
 itemConn  = new QListViewItem(raiz, "Connection");
 itemModos = new QListViewItem(raiz, "Modes");
 itemIdent = new QListViewItem(raiz, "Identity");
 panel->setSelected(itemIdent, true); // para que sea el seleccionado

 connect(panel, SIGNAL(selectionChanged(QListViewItem *)), SLOT(otraHoja(QListViewItem *)));
}

void cuadroConfiguracion::crearHojas()
{
 int x, y, cursor, ancho, alto;

 titulo = new QLabel("Identity", this);
 titulo->setAlignment(AlignCenter);
 titulo->setFont(QFont("Arial", 15, QFont::Bold));

 x = MARCO + ANCHO_PANEL_CONFIGURACION + DISTANCIA;
 y = MARCO;
 ancho = width() - 2*MARCO - DISTANCIA - ANCHO_PANEL_CONFIGURACION;
 alto = height() - 2*MARCO - ALTO_LABEL - 2 * DISTANCIA - ALTO_BOTON;

 titulo->setGeometry(x, y, ancho, ALTO_LABEL);
 y += ALTO_LABEL + DISTANCIA;

 // general es el �nico widget visible en un primer momento
 identidad	= new QWidget(this);
 identidad->setGeometry(x, y, ancho, alto);
 identidad->hide(); // as� no se ve como se dibujan los controles
 widgetActual = identidad;

 modos 	= new QWidget(this);
 modos->setGeometry(x, y, ancho, alto);
 modos->hide();

 conexion 	= new QWidget(this);
 conexion->setGeometry(x, y, ancho, alto);
 conexion->hide();

 ctcp		= new QWidget(this);
 ctcp->setGeometry(x, y, ancho, alto);
 ctcp->hide();

 varios 	= new QWidget(this);
 varios->setGeometry(x, y, ancho, alto);
 varios->hide();

 canales	= new QWidget(this);
 canales->setGeometry(x, y, ancho, alto);
 canales->hide();

 display 	= new QWidget(this);
 display->setGeometry(x, y, ancho, alto);
 display->hide();

 logging = new QWidget(this);
 logging->setGeometry(x, y, ancho, alto);
 logging->hide();

 dcc = new QWidget(this);
 dcc->setGeometry(x, y, ancho, alto);
 dcc->hide();

 // los controles de general
 cursor = 0;
 nick1 	= putEntry(identidad, cursor, "Nickname:", 100, 200);
 nick2 	= putEntry(identidad, cursor, "Alternative:", 100, 200);
 cursor += DISTANCIA;	
 userInfo = putEntry(identidad, cursor, "Real Name:", 100);
 cursor += DISTANCIA;	
 usarPass = putCheck(identidad, cursor, "Use Password", 130, 2, 1);
 autoUser = putCheck(identidad, cursor, "Find out username", 130, 2, 2);
 cursor += ALTO_CHECKBOX + DISTANCIA;
 pass 	= putEntry (identidad, cursor, "Password:", 100, 200);
 pass->setEchoMode(QLineEdit::Password);
 user 	= putEntry(identidad, cursor, "User", 100);
 connect(usarPass, SIGNAL(toggled(bool)), SLOT(habilitarPassYUser(bool)));
 connect(autoUser, SIGNAL(toggled(bool)), SLOT(habilitarPassYUser(bool)));

 // los controles de Modos
 cursor = 0;
 invisible = putCheck(modos, cursor, "Invisible", 70, 2, 1);
 wallops = putCheck(modos, cursor, "WallOps", 70, 2, 2);

 // los controles de coneccion
 cursor = 0;
 verMotd = putCheck(conexion, cursor, "Show MOTD on connect");
 cursor += ALTO_CHECKBOX;
 listEnConn = putCheck(conexion, cursor, "Get channel list on connect");
 cursor += ALTO_CHECKBOX;
 reconnect = putCheck(conexion, cursor, "Reconnect");
 cursor += ALTO_CHECKBOX + 2*DISTANCIA;

 // ctcp
 cursor = 0;
 verCtcp			= putCheck(ctcp, cursor, "Show CTCP Querys on the main window");
 cursor += ALTO_CHECKBOX + DISTANCIA;
 responderCtcpVersion = putCheck(ctcp, cursor, "Reply CTCP VERSION queries");
 cursor += ALTO_CHECKBOX + DISTANCIA;
 ctcpFinger 	= putEntry(ctcp, cursor, "CTCP Finger:", 90);
 ctcpUserInfo = putEntry(ctcp, cursor, "CTCP User Info:", 90);
 cursor += DISTANCIA;
 ctcpInterval = putSpin(ctcp, cursor, "Wait before answering new CTCPs:", 200, 0, 10, 100);
 ctcpInterval->setSpecialValueText("Don't wait");
 ctcpInterval->setSuffix(" seconds");

 // canales
 cursor = 0;
 partMessage = putEntry(canales, cursor, "Part message:", 90);
 quitMessage = putEntry(canales, cursor, "Quit message:", 90);
 cursor += DISTANCIA;
 lineasRepetidas = putSpin(canales, cursor, "Don't show repeated messages:", 255, 0, 20, 115);
 lineasRepetidas->setSuffix(" repetitions");
 lineasRepetidas->setSpecialValueText("Always show");

 // display
 cursor = 0;
 font = putEntry(display, cursor, "Font:", 65, 200);
 // font->setEnabled(false); // por ahora no quiero que nadie modifique esto
 fontSize = putSpin(display, cursor, "Font Size:", 65, 6, 40, 40);
 verFont = new QPushButton("show font", display);
 verFont->setGeometry((int)((display->width() - ANCHO_BOTON) / 2), cursor, ANCHO_BOTON, ALTO_BOTON);
 connect(verFont, SIGNAL(clicked()), SLOT(mostrarFont()));

 muestra = new QLabel("Both uppercase and lowercase lines should have the same lenght.\nIf they don't try with other font", display);
 muestra->setGeometry(MARCO, display->height() - 4*ALTO_LABEL - MARCO, display->width() - 2*MARCO, 2*ALTO_LABEL);
 muestra = new QLabel("abcdefghijklmnopqrstuvwxyz0123456789\nABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", display);
 muestra->setGeometry(MARCO, display->height() - 2*ALTO_LABEL - MARCO, display->width() - 2*MARCO, 2*ALTO_LABEL);
 muestra->setBackgroundColor(gray);
 muestra->setAlignment(AlignCenter);

 // varios
 cursor = 0;
 borrarIgnore = putCheck(varios, cursor, "Clean Ignored Nicks list when QIrc quits");
 cursor += ALTO_CHECKBOX + DISTANCIA;
 elevarPrivados = putCheck(varios, cursor, "Show private windows when receiving a new message");
 cursor += ALTO_CHECKBOX + DISTANCIA;
 borrarMirc = putCheck(varios, cursor, "Strip mIrc colors");

 // logging
 cursor = 0;
 logChannels = putCheck(logging, cursor, "Log channels");
 cursor += ALTO_CHECKBOX + DISTANCIA;
 logPrivates = putCheck(logging, cursor, "Log privates");
 cursor += ALTO_CHECKBOX + DISTANCIA;
 borrarMircLogs = putCheck(logging, cursor, "Strip mIrc colors from logs");

 // dcc
 cursor = 0;
 dccConnectTimeout = putSpin(dcc, cursor, "DCC connection timeout", 200, 5, 600, 105, 5);
 dccConnectTimeout->setSuffix(" seconds");
 dccTransferTimeout   = putSpin(dcc, cursor, "DCC transfer timeout", 200, 5, 600, 105, 5);
 dccTransferTimeout->setSuffix(" seconds");
 cursor += 2*DISTANCIA;
 dccMaxWindowsCount = putSpin(dcc, cursor, "Simultaneous DCC Get windows", 200, 0, 30, 105);
 dccMaxWindowsCount->setSpecialValueText("Unlimited");
 dccMaxWindowsCount->setSuffix(" windows");
 cursor += 2*DISTANCIA;
 dccBlockSize    = putSpin(dcc, cursor, "DCC Block Size", 200, 256, 10000, 105, 256);
 dccBlockSize->setSuffix(" bytes");

 // cargo la configuraci�n
 cargarConfiguracion();

 // recien ahora muestro el widget
 identidad->show();
}

void cuadroConfiguracion::crearBotonera()
{
 int x, x2, x3, y;

 y = ALTO_CONFIGURACION - MARCO- ALTO_BOTON;
 x = (int)((ANCHO_CONFIGURACION - 3 * ANCHO_BOTON) / 4);
 x2 = 2*x + ANCHO_BOTON;
 x3 = 3*x + 2*ANCHO_BOTON;

 aceptar 		= new QPushButton("Accept", this);
 restaurar	= new QPushButton("Restore", this);
 cancelar 	= new QPushButton("Cancel", this);
 aceptar->setGeometry(x, y, ANCHO_BOTON, ALTO_BOTON);
 restaurar->setGeometry(x2, y, ANCHO_BOTON, ALTO_BOTON);
 cancelar->setGeometry(x3, y, ANCHO_BOTON, ALTO_BOTON);
 connect(aceptar, SIGNAL(clicked()), SLOT(clickAceptar()));
 connect(restaurar, SIGNAL(clicked()), SLOT(clickRestaurar()));
 connect(cancelar, SIGNAL(clicked()), SLOT(clickCancelar()));
}

cuadroConfiguracion::~cuadroConfiguracion()
{
}

void cuadroConfiguracion::otraHoja(QListViewItem *item)
{
 // si el item es el mismo que el anterior no hago nada (evito parpadeos al pedos)
 QWidget *anterior;

 anterior = widgetActual;

 if (item == itemIdent)   { widgetActual = identidad; titulo->setText("Identity"); }
 if (item == itemModos) { widgetActual = modos; titulo->setText("Modes"); }
 if (item == itemConn)  { widgetActual = conexion; titulo->setText("Connection"); }
 if (item == itemCtcp)  { widgetActual = ctcp; titulo->setText("CTCP"); }
 if (item == itemVarios)  { widgetActual = varios; titulo->setText("Misc"); }
 if (item == itemCanales)  { widgetActual = canales; titulo->setText("Channels"); }
 if (item == itemDisplay)  { widgetActual = display; titulo->setText("Display"); }
 if (item == itemLogging)  { widgetActual = logging; titulo->setText("Log"); }
 if (item == itemDcc)  { widgetActual = dcc; titulo->setText("DCC"); }


 if (anterior != widgetActual)
 {
  anterior->hide();
  widgetActual->show();
 }
}

void cuadroConfiguracion::cargarConfiguracion()
{
 nick1->setText(config->readStrValue(CONFIG_NICK1, DEF_NICK1));
 nick2->setText(config->readStrValue(CONFIG_NICK2, DEF_NICK2));
 userInfo->setText(config->readStrValue(CONFIG_USERINFO, DEF_USERINFO));
 user->setText(config->readStrValue(CONFIG_USER, DEF_USER));
 pass->setText(config->readStrValue(CONFIG_PASS, DEF_PASS));
 habilitarPassYUser(false);

 usarPass->setChecked(config->readBoolValue(CONFIG_USARPASS, DEF_USARPASS));
 autoUser->setChecked(config->readBoolValue(CONFIG_AUTOUSER, DEF_AUTOUSER));

 invisible->setChecked(config->readBoolValue(CONFIG_INVISIBLE, DEF_INVISIBLE));
 wallops->setChecked(config->readBoolValue(CONFIG_WALLOPS, DEF_WALLOPS));

 verMotd->setChecked(config->readBoolValue(CONFIG_VERMOTD, DEF_VERMOTD));
 listEnConn->setChecked(config->readBoolValue(CONFIG_LISTENCONECCION, DEF_LISTENCONECCION));
 reconnect->setChecked(config->readBoolValue(CONFIG_RECONNECT, DEF_RECONNECT));

 verCtcp->setChecked(config->readBoolValue(CONFIG_VERCTCP, DEF_VERCTCP));
 responderCtcpVersion->setChecked(config->readBoolValue(CONFIG_RESPONDERCTCPVERSION, DEF_RESPONDERCTCPVERSION));
 ctcpUserInfo->setText(config->readStrValue(CONFIG_CTCPUSERINFO, DEF_CTCPUSERINFO));
 ctcpFinger->setText(config->readStrValue(CONFIG_CTCPFINGER, DEF_CTCPFINGER));
 ctcpInterval->setValue(config->readIntValue(CONFIG_CTCPINTERVAL, DEF_CTCPINTERVAL));

 partMessage->setText(config->readStrValue(CONFIG_PARTMESSAGE, DEF_PARTMESSAGE));
 quitMessage->setText(config->readStrValue(CONFIG_QUITMESSAGE, DEF_QUITMESSAGE));
 borrarMirc->setChecked(config->readBoolValue(CONFIG_BORRARMIRC, DEF_BORRARMIRC));
 lineasRepetidas->setValue(config->readIntValue(CONFIG_MAXLINEASREPETIDAS, DEF_MAXLINEASREPETIDAS));

 font->setText(config->readStrValue(CONFIG_FONT1, DEF_FONT1));
 fontSize->setValue(config->readIntValue(CONFIG_FONTSIZE1, DEF_FONTSIZE1));
 mostrarFont();

 borrarIgnore->setChecked(config->readBoolValue(CONFIG_VACIARIGNORELIST, DEF_VACIARIGNORELIST));
 elevarPrivados->setChecked(config->readBoolValue(CONFIG_ELEVARPRIVADOS, DEF_ELEVARPRIVADOS));

 logChannels->setChecked(config->readBoolValue(CONFIG_LOGCHANNELS, DEF_LOGCHANNELS));
 logPrivates->setChecked(config->readBoolValue(CONFIG_LOGPRIVATES, DEF_LOGPRIVATES));
 borrarMircLogs->setChecked(config->readBoolValue(CONFIG_BORRARMIRCLOGS, DEF_BORRARMIRCLOGS));

 dccConnectTimeout->setValue(config->readIntValue(CONFIG_DCCCONNECTTIMEOUT, DEF_DCCCONNECTTIMEOUT));
 dccTransferTimeout->setValue(config->readIntValue(CONFIG_DCCTRANSFERTIMEOUT, DEF_DCCTRANSFERTIMEOUT));
 dccMaxWindowsCount->setValue(config->readIntValue(CONFIG_DCCMAXWINDOWSCOUNT, DEF_DCCMAXWINDOWSCOUNT));
 dccBlockSize->setValue(config->readIntValue(CONFIG_DCCBLOCKSIZE, DEF_DCCBLOCKSIZE));
}

void cuadroConfiguracion::mostrarFont()
{
 muestra->setFont(QFont(font->text(), fontSize->value()));
}

QLineEdit *cuadroConfiguracion::putEntry(QWidget *father, int &y, const char *label, int anchoLabel, int anchoText)
{
 QLabel *l = new QLabel(label, father);
 l->setAlignment(AlignRight | AlignVCenter);
 l->setGeometry(DISTANCIA, y, anchoLabel, ALTO_LABEL);

 QLineEdit *e = new QLineEdit(father);
 int ancho = anchoText;
 if(anchoText == -1) ancho = father->width() - anchoLabel - 2*DISTANCIA;

 e->setGeometry(anchoLabel + 2*DISTANCIA, y, ancho, ALTO_ENTRADA);

 y += ALTO_LABEL + DISTANCIA;

 return e;
}

QSpinBox *cuadroConfiguracion::putSpin(QWidget *father, int &y, const char *label, int anchoLabel, int max, int min, int anchoSpin, int step = 1)
{
 QLabel *l = new QLabel(label, father);
 l->setAlignment(AlignRight | AlignVCenter);
 l->setGeometry(DISTANCIA, y, anchoLabel, ALTO_LABEL);

 QSpinBox *s = new QSpinBox(max, min, step, father);
 s->setGeometry(anchoLabel + 2*DISTANCIA, y, anchoSpin, ALTO_ENTRADA);

 y += ALTO_LABEL + DISTANCIA;

 return s;
}

QCheckBox *cuadroConfiguracion::putCheck(QWidget *father, int &y, const char *label, int anchoLabel = -1, int cantidad = 1, int indice = 1)
{
 QCheckBox *c = new QCheckBox(label, father);
 int x = (int)((father->width() - cantidad * anchoLabel)  / (cantidad + 1)) * indice + (indice-1)*anchoLabel;
 if (anchoLabel != -1) {
  c->setGeometry(x, y, anchoLabel, ALTO_CHECKBOX);
 } else {
  // es el �nico en la linea ( no lo centro y le doy todo el ancho)
  c->setGeometry(0, y, father->width(), ALTO_CHECKBOX);
 }

 return c;
}


void cuadroConfiguracion::guardarConfiguracion()
{
 config->writeStrValue(CONFIG_NICK1, nick1->text());
 config->writeStrValue(CONFIG_NICK2, nick2->text());
 config->writeStrValue(CONFIG_USERINFO, userInfo->text());
 config->writeStrValue(CONFIG_USER, user->text());
 config->writeStrValue(CONFIG_PASS, pass->text());
 config->writeBoolValue(CONFIG_USARPASS, usarPass->isChecked());
 config->writeBoolValue(CONFIG_AUTOUSER, autoUser->isChecked());

 config->writeBoolValue(CONFIG_INVISIBLE, invisible->isChecked());
 config->writeBoolValue(CONFIG_WALLOPS,	 wallops->isChecked());

 config->writeBoolValue(CONFIG_VERMOTD, verMotd->isChecked());
 config->writeBoolValue(CONFIG_LISTENCONECCION, listEnConn->isChecked());
 config->writeBoolValue(CONFIG_RECONNECT, reconnect->isChecked());

 config->writeBoolValue(CONFIG_VERCTCP, verCtcp->isChecked());
 config->writeBoolValue(CONFIG_RESPONDERCTCPVERSION, responderCtcpVersion->isChecked());
 config->writeStrValue(CONFIG_CTCPUSERINFO, ctcpUserInfo->text());
 config->writeStrValue(CONFIG_CTCPFINGER, ctcpFinger->text());
 config->writeIntValue(CONFIG_CTCPINTERVAL, ctcpInterval->value());

 config->writeStrValue(CONFIG_PARTMESSAGE, partMessage->text());
 config->writeStrValue(CONFIG_QUITMESSAGE, quitMessage->text());
 config->writeBoolValue(CONFIG_BORRARMIRC, borrarMirc->isChecked());
 config->writeIntValue(CONFIG_MAXLINEASREPETIDAS, lineasRepetidas->value());

 config->writeStrValue(CONFIG_FONT1, font->text());
 config->writeIntValue(CONFIG_FONTSIZE1, fontSize->value());

 config->writeBoolValue(CONFIG_VACIARIGNORELIST, borrarIgnore->isChecked());
 config->writeBoolValue(CONFIG_ELEVARPRIVADOS, elevarPrivados->isChecked());

 config->writeBoolValue(CONFIG_LOGCHANNELS, logChannels->isChecked());
 config->writeBoolValue(CONFIG_LOGPRIVATES, logPrivates->isChecked());
 config->writeBoolValue(CONFIG_BORRARMIRCLOGS, borrarMircLogs->isChecked());

 config->writeIntValue(CONFIG_DCCCONNECTTIMEOUT, dccConnectTimeout->value());
 config->writeIntValue(CONFIG_DCCTRANSFERTIMEOUT, dccTransferTimeout->value());
 config->writeIntValue(CONFIG_DCCMAXWINDOWSCOUNT, dccMaxWindowsCount->value());
 config->writeIntValue(CONFIG_DCCBLOCKSIZE, dccBlockSize->value());
}

bool cuadroConfiguracion::datosValidos()
{
 if (((nick1->text() == NULL) || (strlen(nick1->text()) == strspn(nick1->text(), " "))) ||
     ((nick2->text() == NULL) || (strlen(nick2->text()) == strspn(nick2->text(), " "))))
 {
  QMessageBox::warning(NULL, "Missing data", "You have to specify at least two nicknames", "Retry");
  return false;
 }

 if ((userInfo->text() == NULL) || (strlen(userInfo->text()) == strspn(userInfo->text(), " ")))
 {
  userInfo->setText("(empty)");
 }

 if ((user->text() == NULL) || (strlen(user->text()) == strspn(user->text(), " ")))
 {
  user->setText("User");
 }

 return true;
}

void cuadroConfiguracion::clickAceptar()
{
 if (datosValidos() == true)
 {
  guardarConfiguracion();
  emit borrarVentanaConf();
 }
}

void cuadroConfiguracion::clickRestaurar()
{
 cargarConfiguracion();
}

void cuadroConfiguracion::clickCancelar()
{
 emit borrarVentanaConf();
}

