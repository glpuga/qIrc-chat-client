/***************************************************************************
                          listanicks.h  -  description
                             -------------------
    begin                : Mon Nov 6 2000
    copyright            : (C) 2000 by Gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LISTANICKS_H
#define LISTANICKS_H

#include <qtableview.h>
#include <qpixmap.h>
#include <string.h>
#include <qpainter.h>
#include <qkeycode.h>

#include "resource.h"

// la siguiente definición incrementa la velocidad de las operaciones de repintado
// es solo experimental
#define MAX_PERFORMANCE

struct nodoListaNicks
{
 char *nick;
 bool op;

 bool seleccionado;

 nodoListaNicks *enlace;
};

class ListaNicks : public QTableView
{
 Q_OBJECT

private:
 QFontMetrics *metrica;

 nodoListaNicks *lista;
 unsigned int cantidadNicks;

 int anchoIconos, anchoNicks, altoFilas;

public:
 ListaNicks(QWidget *parent, const char *name = 0, WFlags f = 0);
 ~ListaNicks();

 void addNick(const char *nick, bool op = false);
 void changeNick(const char *oldNick, const char *newNick);
 void removeNick(const char *nick);

 void setOperatorStatus(const char *nick, bool op);

 const char *list(unsigned int index);

 void clear(); // no creo que lo use demasiado pero bueno.

 unsigned int count() { return cantidadNicks; }

 void setAutoUpdate(bool v) { QTableView::setAutoUpdate(v); }

private:
 int cellWidth(int c);
 int totalWidth() { return (anchoIconos + anchoNicks); }

 void setCurrentItem(int item);
 void paintCell(QPainter *p, int row, int col);
 void resizeEvent(QResizeEvent *e);

 void mousePressEvent(QMouseEvent *e);
 void mouseDoubleClickEvent(QMouseEvent *e);
 void keyPressEvent(QKeyEvent *e);
 void focusInEvent(QFocusEvent *e) { repaint(false); }
 void focusOutEvent(QFocusEvent *e) { repaint(false); }

signals:
 void rightButtonClicked(QPoint, const char *);
 void nickDoubleClicked(const char *);
};

#endif
