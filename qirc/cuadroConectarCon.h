/***************************************************************************
                          cuadroConectarCon.h  -  description
                             -------------------
    begin                : Fri Oct 6 2000
    copyright            : (C) 2000 by gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CUADROCONECTARCON_H
#define CUADROCONECTARCON_H

#include <string.h>
#include <stdlib.h>

#include <qwidget.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qlistview.h>
#include <qmessagebox.h>

#include "Config.h"
#include "resource.h"

#define ANCHO_CONECTARCON 600
#define ANCHO_LABEL_CONECTARCON 70
#define ALTO_LISTA_CONECTARCON 250
#define ALTO_CONECTARCON 2*MARCO + 5*DISTANCIA + ALTO_LISTA_CONECTARCON + 2*ALTO_BOTON  + 3*ALTO_LABEL

struct Server
{
 char *host;
 char *comentario;
 int puerto;
};

class cuadroConectarCon  : public QWidget
{
 Q_OBJECT

private:
 QLabel *labHost, *labComentario, *labPuerto;
 QLineEdit *host, *comentario, *puerto;
 QPushButton *bAgregar, *bReemplazar, *bBorrar;
 QListView *lista;
 QPushButton *bConectar, *bCancelar;

 Config *config;

 bool datosValidos();
 void cargarLista();
 void actualizarLista();
 void mostrarMensajeDatosInvalidos();

 void closeEvent(QCloseEvent *e) { e->ignore(); }; // para que no se pueda cerrar por la cruz

public:
 cuadroConectarCon(Config *config);
 ~cuadroConectarCon();

public slots:
 void doubleClicked(QListViewItem *item);
 void currentChanged(QListViewItem *item);

 void probablyNewRecord(const char *);

 void agregar();
 void reemplazar();
 void borrar();

 void conectar();
 void cancelar();

signals:
 void resultado(Server *s);
};

#endif