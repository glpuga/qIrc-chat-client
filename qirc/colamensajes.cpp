/***************************************************************************
                          colamensajes.cpp  -  description
                             -------------------
    begin                : Thu Oct 26 2000
    copyright            : (C) 2000 by gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "colamensajes.h"

colaMensajes::colaMensajes()
{
 // inicializa la cola a vacio
 inicio = fin = NULL;
 numElementos = 0;
}

colaMensajes::~colaMensajes()
{
 // si queda alg�n elemento en la cola lo elimino
 vaciar();
}

void colaMensajes::vaciar()
{
 fin = NULL;

 nodoColaMensajes *nodo;
 while (inicio != NULL)
 {
  nodo = inicio;
  inicio = inicio->enlace;

  delete [] nodo->identificador;
  delete [] nodo->mensaje;
  delete nodo;
 }
}

void colaMensajes::insertar(const char *identificador, const char *mensaje, const int color)
{
 nodoColaMensajes *nuevoNodo = new nodoColaMensajes;
 nuevoNodo->identificador = new char[strlen(identificador)+1];
 strcpy(nuevoNodo->identificador, identificador);
 nuevoNodo->mensaje  			= new char[strlen(mensaje)+1];
 strcpy(nuevoNodo->mensaje, mensaje);
 nuevoNodo->color 				= color;
 nuevoNodo->enlace 				= NULL;

 if (inicio == NULL)
 {
  // es el primer elemento en la lista
  inicio = fin = nuevoNodo;
 } else {
  fin->enlace = nuevoNodo;
  fin = nuevoNodo;
 }

 numElementos++;
 //listo!
}

void colaMensajes::longitudDatos(int &longIdentificador, int &longMensaje)
{
 // para permitirle al programa saber cuanto espacio reservar para las cadenas
 // (si as� lo necesita)
 // Los tama�os no incluyen el '\0'
 // si la cola est� vacia devuelve -1 y -1
 if (inicio == NULL) {
  longIdentificador = longMensaje = -1;
 } else {
  longIdentificador = strlen(inicio->identificador);
  longMensaje			 = strlen(inicio->mensaje);
 }
}

const char *colaMensajes::extraerIdentificador()
{
 if (inicio == NULL)
  return NULL;

 return inicio->identificador;
}

const char *colaMensajes::extraerMensaje()
{
 if (inicio == NULL)
  return NULL;

 return inicio->mensaje;
}

const int colaMensajes::extraerColor()
{
 if (inicio == NULL)
  return 0;

 return inicio->color;
}

void colaMensajes::eliminar()
{
 // elimina el primer elemento de la lista, el cual se supone ya leido por medio
 // de extraerIdentificador() y extraerMensaje()

 // me aseguro de que haya algo en la lista
 if (inicio == NULL) return; // no hago nada

 nodoColaMensajes *aux = inicio;
 if (inicio == fin) fin = NULL;
 inicio = inicio->enlace;

 delete [] aux->identificador;
 delete [] aux->mensaje;
 delete aux;

 numElementos--;
}

bool colaMensajes::estaVacia()
{
 if(inicio == NULL)
 {
  return true;
 } else {
  return false;
 }
}


