/***************************************************************************
                          listanicks.cpp  -  description
                             -------------------
    begin                : Mon Nov 6 2000
    copyright            : (C) 2000 by Gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "listanicks.h"

// el �cono para los nicks operadores
#include "operator.xpm"

ListaNicks::ListaNicks(QWidget *parent, const char *name = 0, WFlags f = 0) : QTableView(parent, name, f)
{
 // la lista est� vac�a
 lista = NULL;
 cantidadNicks = 0;

 // el ancho es originalmente 0
 anchoIconos = 15;
 anchoNicks = 0;

 // inicializo el objeto fontmetrics que uso para medir cadenas de texto en pixels
 metrica = new QFontMetrics(fontMetrics());

 // establezco el color de fondo y el comportamiento del widget
 setBackgroundColor(white);
 setTableFlags(Tbl_autoScrollBars | Tbl_smoothHScrolling);
 setFrameStyle(Panel | Sunken);
 setLineWidth(3);

 // el ancho de las columnas es variable
 setCellWidth(0);

 // el alto es el alto de las letras o 12 (la altura del �cono de op), lo que sea m�s grande
 altoFilas = (metrica->height()>12)?(metrica->height()):(12);
 setCellHeight(altoFilas);

 // establezco la cantidad de columnas y filas
 setNumRows(0); // para empezar, est� bien
 setNumCols(2); // una sola

 // para poder manejar presiones de teclado...
 setFocusPolicy(QWidget::ClickFocus);
}

ListaNicks::~ListaNicks()
{
 // elimino el objeto m�trica
 delete metrica;

 // vacio la lista
 nodoListaNicks *nodo = lista, *aux;
 while(nodo != NULL)
 {
  aux = nodo->enlace;
  delete [] nodo->nick;
  delete nodo;
  nodo = aux;
 }
 lista = NULL;
}

void ListaNicks::addNick(const char *nick, bool op = false)
{
 // creo el nuevo nodo
 nodoListaNicks *nuevo = new nodoListaNicks;
 nuevo->nick = new char[strlen(nick)+1];
 strcpy(nuevo->nick, nick);
 nuevo->op = op;
 nuevo->seleccionado = false;
 nuevo->enlace = NULL;

 int indice = 0; // esta variable la uso a la hora de actualizar
 // ahora busc� el lugar donde insertarlo para que quede una lista ordenada
 nodoListaNicks *nodo = lista, *anterior = NULL;
 while ((nodo != NULL) && (nuevo->op?(strcasecmp(nick, nodo->nick) > 0)&&(nodo->op):(strcasecmp(nick, nodo->nick) > 0)||(nodo->op)))
 {
  anterior = nodo;
  nodo = nodo->enlace;
  indice++;
 }

 if (anterior == NULL)
 {
  // es el primer nodo de la lista
  lista = nuevo;
 } else {
  // va en medio o al final
  anterior->enlace = nuevo;
 }
 nuevo->enlace = nodo;

 if (anchoNicks < metrica->width(nick))
 {
  anchoNicks = metrica->width(nick);
  updateTableSize();
 }

 // actualizo los cambios
 cantidadNicks++;
 setNumRows(cantidadNicks);
 #ifndef MAX_PERFORMANCE
  // minimo desempe�o, actualiza la lista entera
  update(); // porque setNumRows() unicamente repinta el �ltimo elemento (supone que se agreg� al final)
 #else
  // maximo desempe�o, actualiza solo lo necesario
  // actualizo todos las celdas a partir de la insertada (incluida)
  // para evitar repintar celdas que no lo necesitan (experimental)
  // notese que puedo hacer un repaint (false) porque paintCell pinta un
  // rectangulo de color en el fondo de las celdas, lo que me permite ahorrar
  // el borrado previo que har�a repaint(true) (reduzco el tiempo de pintado a la mitad!)
  repaint(false);
 #endif

 // listo
}

void ListaNicks::changeNick(const char *oldNick, const char *newNick)
{
 // tengo que eliminar un elemento y crear otro, pero para hacerlo m�s r�pido
 // no puedo usar las funciones addNick y removeNick sino que para ahorrar repintados
 // lo hago reescribiendo todo

 // primero busco el nodo a eliminar
 nodoListaNicks *viejo = lista, *anterior = NULL;
 while ((viejo != NULL) && (strcasecmp(viejo->nick, oldNick) != 0))
 {
  anterior = viejo;
  viejo = viejo->enlace;
 }
 if (viejo == NULL) return; // no lo encontr�?

 // si anterior es null, entonces es el primero el que tengo que borrar
 if (anterior == NULL)
 {
  lista = viejo->enlace;
 } else {
  anterior->enlace = viejo->enlace;
 }

 nodoListaNicks *nuevo = new nodoListaNicks;
 nuevo->nick = new char[strlen(newNick)+1];
 strcpy(nuevo->nick, newNick);
 nuevo->op = viejo->op;
 nuevo->seleccionado = viejo->seleccionado;
 nuevo->enlace = NULL;

 delete [] viejo->nick;
 delete viejo;

 // ahora busc� el lugar donde insertarlo para que quede una lista ordenada
 nodoListaNicks *nodo = lista;
 anterior = NULL; // definida antes de hacer la b�squeda del nodo a borrar
 while ((nodo != NULL) && (nuevo->op?((strcasecmp(newNick, nodo->nick) > 0)&&(nodo->op)):((strcasecmp(newNick, nodo->nick) > 0)||(nodo->op))))
 {
  anterior = nodo;
  nodo = nodo->enlace;
 }
 if (anterior == NULL)
 {
  // es el primer nodo de la lista
  lista = nuevo;
 } else {
  // va en medio o al final
  anterior->enlace = nuevo;
 }
 nuevo->enlace = nodo;

 if (anchoNicks < metrica->width(newNick))
 {
  anchoNicks = metrica->width(newNick);
  updateTableSize();
 }

 // la cantidad de nicks en la lista no cambio
 // hago un update para mostrar los cambios
 #ifndef MAX_PERFORMANCE
  // minimo desempe�o, actualiza la lista entera
  update(); // porque setNumRows() unicamente repinta el �ltimo elemento (supone que se agreg� la celda al final de la tabla)
 #else
  // maximo desempe�o [experimetal]
  // notese que puedo hacer un repaint (false) porque paintCell pinta un
  // rectangulo de color en el fondo de las celdas, lo que me permite ahorrar
  // el borrado previo que har�a repaint(true) (reduzco el tiempo de pintado a la mitad!)
  repaint(false);
 #endif
}

void ListaNicks::removeNick(const char *nick)
{
 // actualizo algunos valores
 cantidadNicks--;
 setNumRows(cantidadNicks); // esto se encarga de hacer el repintado

 // busco el nodo a eliminar
 nodoListaNicks *nodo = lista, *anterior = NULL;
 while ((nodo != NULL) && (strcasecmp(nodo->nick, nick) != 0))
 {
  anterior = nodo;
  nodo = nodo->enlace;
 }
 if (nodo == NULL) return; // no lo encontr�?

 // si anterior es null, entonces es el primero el que tengo que borrar
 if (anterior == NULL)
 {
  lista = nodo->enlace;
 } else {
  anterior->enlace = nodo->enlace;
 }
 delete [] nodo->nick;
 delete nodo;

 #ifndef MAX_PERFORMANCE
  // minimo desempe�o, actualiza la lista entera
  update(); // porque setNumRows() unicamente repinta el �ltimo elemento (supone que se agreg� la celda al final de la tabla)
 #else
  // maximo desempe�o [experimental]
  // notese que puedo hacer un repaint (false) porque paintCell pinta un
  // rectangulo de color en el fondo de las celdas, lo que me permite ahorrar
  // el borrado previo que har�a repaint(true) (reduzco el tiempo de pintado a la mitad!)
  repaint(false);
 #endif
}

void ListaNicks::setOperatorStatus(const char *nick, bool op)
{
 // busco el nick y lo saco de esa posici�n en la lista
 // despu�s busco la nueva posici�n que le corresponde y lo coloco ah�
 nodoListaNicks *nodoBuscado = lista, *anterior = NULL;
 while ((nodoBuscado != NULL) && (strcasecmp(nodoBuscado->nick, nick) != 0))
 {
  anterior = nodoBuscado;
  nodoBuscado = nodoBuscado->enlace;
 }
 // solo por si las dudas
 if(nodoBuscado == NULL) return;
 // cierro la lista
 if (anterior == NULL)
 {
  lista = nodoBuscado->enlace;
 } else {
  anterior->enlace = nodoBuscado->enlace;
 }

 // ahora lo marco como op (o le quito el op, whatever)
 nodoBuscado->op = op;

 // busco su nueva posici�n en la lista
 nodoListaNicks *nodo = lista;
 anterior = NULL; // declarada m�s arriba
 while ((nodo != NULL) && (nodoBuscado->op?((strcasecmp(nick, nodo->nick) > 0)&&(nodo->op)):((strcasecmp(nick, nodo->nick) > 0)||(nodo->op))))
 {
  anterior = nodo;
  nodo = nodo->enlace;
 }
 // y lo inserto
 if (anterior == NULL)
 {
  lista = nodoBuscado;
 } else {
  anterior->enlace = nodoBuscado;
 }
 nodoBuscado->enlace = nodo;

 // ahora repinto todo (todo porque cambi� el orden de los nicks)
 #ifndef MAX_PERFORMANCE
  // minimo desempe�o, actualiza la lista entera
  update(); // porque setNumRows() unicamente repinta el �ltimo elemento (supone que se agreg� la celda al final de la tabla)
 #else
  // maximo desempe�o [experimental]
  // notese que puedo hacer un repaint (false) porque paintCell pinta un
  // rectangulo de color en el fondo de las celdas, lo que me permite ahorrar
  // el borrado previo que har�a repaint(true) (reduzco el tiempo de pintado a la mitad!)
  repaint(false);
 #endif
}

const char *ListaNicks::list(unsigned int index)
{
 // busca el i-esimo elemento de la lista
 unsigned int i = 0;
 nodoListaNicks *nodo = lista;
 while((nodo != NULL) && (i != index))
 {
  nodo = nodo->enlace;
  i++;
 }
 if (nodo == NULL) return NULL;

 return nodo->nick;
}

void ListaNicks::clear()
{
 // simplemente vacio la lista
 setNumRows(0);
 cantidadNicks = 0;

 nodoListaNicks *nodo = lista, *aux;
 while(nodo != NULL)
 {
  aux = nodo->enlace;
  delete [] nodo->nick;
  delete nodo;
  nodo = aux;
 }
 lista = NULL;
}

int ListaNicks::cellWidth(int c)
{
 switch (c)
 {
  case 0 : return anchoIconos; break;
  case 1 : return ((anchoNicks>width()-anchoIconos)?(anchoNicks):(width()-anchoIconos)); break;
 }
 return 0;
}

void ListaNicks::paintCell(QPainter *p, int row, int col)
{
 // busco el nodo que tengo que dibujar
 nodoListaNicks *nodo = lista;
 int i = 0;
 while((nodo != NULL) && (i != row))
 {
  nodo = nodo->enlace;
  i++;
 }
 if (nodo == NULL) return; // no encontr� el nodo!

 int ancho = (col==0)?anchoIconos:cellWidth(1);
 p->setBackgroundMode(TransparentMode);
 if (nodo->seleccionado == true)
 {
  p->setBrush(blue);
  p->setPen(blue); // para el borde del rectangulo
  p->drawRect(0, 0, ancho, altoFilas);
  p->setPen(white);
 } else {
  p->setBrush(white);
  p->setPen(white); // para el borde del rectangulo
  p->drawRect(0, 0, ancho, altoFilas);
  p->setPen(black);
 }

 switch(col) {
  case 0 	: // dibujo el �cono
            if (nodo->op == true) p->drawPixmap(0, 0, QPixmap(operator_icon));
            break;
  case 1	: // escribo el texto
            p->drawText(0, metrica->ascent(), nodo->nick);
            break;
 }
}

void ListaNicks::mousePressEvent(QMouseEvent *e)
{
 int seleccionado = findRow(e->y());
 setCurrentItem(seleccionado); // notese que puede darse que no se seleccione nada
 if (seleccionado < 0) return; // no se hizo click sobre ning�n nick
 if (e->button() == RightButton)
 {
  emit rightButtonClicked(e->globalPos(), list(seleccionado));
 }
}

void ListaNicks::mouseDoubleClickEvent(QMouseEvent *e)
{
 int seleccionado = findRow(e->y());
 setCurrentItem(seleccionado); // notese que puede darse que no se seleccione nada
 if (seleccionado < 0) return; // el double click fue en un area vac�a
 emit nickDoubleClicked(list(seleccionado));
}

void ListaNicks::keyPressEvent(QKeyEvent *e)
{
 // busco el nodo
 nodoListaNicks *nodo = lista, *anterior = NULL, *siguiente = (lista == NULL)?(NULL):(lista->enlace);
 int index = 0;
 while ((nodo != NULL) && (nodo->seleccionado == false))
 {
  anterior = nodo;
  nodo = nodo->enlace;
  siguiente = nodo->enlace;
  index++;
 }
 if (nodo == NULL) { e->ignore(); return; }

 int visible = 0;
 bool mover = false;
 switch(e->key())
 {
  case Key_Left :
  case Key_Up   :// va pa arriba
                 if (anterior != NULL)
                 {
                  mover = true;
                  visible = index-1;
                  anterior->seleccionado = true;
                  nodo->seleccionado     = false;
                  updateCell(index, 0);
                  updateCell(index, 1);
                  updateCell(visible, 0);
                  updateCell(visible, 1);
                 } else { e->ignore(); }
                 break;
  case Key_Right:
  case Key_Down :// va pa abajo
                 if (siguiente != NULL)
                 {
                  mover = true;
                  visible = index+1;
                  siguiente->seleccionado = true;
                  nodo->seleccionado      = false;
                  updateCell(index, 0);
                  updateCell(index, 1);
                  updateCell(visible, 0);
                  updateCell(visible, 1);
                 } else { e->ignore(); }
                 break;
  default       :// no se que hacer con la tecla
                 e->ignore(); break;
 }
 // me aseguro de que sea visible
 if (((visible < topCell()) || (visible > lastRowVisible())) && (mover == true))
 {
  setTopCell(visible);
 }
 // listo, chau
}

void ListaNicks::resizeEvent(QResizeEvent *e)
{
 // solo para fijarme de avisar el cambio de ancho de la segunda columna
 updateTableSize();
}
void ListaNicks::setCurrentItem(int item)
{
 // encuentra el item y lo marca como seleccionado
 // marca todos los dem�s como no seleccionados
 bool repintar;
 nodoListaNicks *nodo = lista;
 int indice = 0;
 while (nodo != NULL)
 {
  repintar = ((nodo->seleccionado) || (item == indice))?true:false;
  nodo->seleccionado = (item == indice)?true:false; // ahorr� varias lineas, no? :)
  // evito el parpadeo lo m�s posible
  if (repintar) { updateCell(indice,0); updateCell(indice,1); }

  indice++;
  nodo = nodo->enlace;
 }
}


