/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : mi� sep 13 10:25:05 ART 2000
    copyright            : (C) 2000 by gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <qapplication.h>
#include "qirc.h"

int main(int argc, char *argv[])
{
 QApplication *a = new QApplication(argc, argv);
 QIrc *t = new QIrc;

 a->setMainWidget(t);

#ifndef ENABLE_QT2
 a->setStyle(WindowsStyle);
#endif

 t->show();

 a->exec();

 return 0;
}
