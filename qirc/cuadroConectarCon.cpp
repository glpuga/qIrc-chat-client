/***************************************************************************
                          cuadroConectarCon.cpp  -  description
                             -------------------
    begin                : Fri Oct 6 2000
    copyright            : (C) 2000 by gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cuadroConectarCon.h"

cuadroConectarCon::cuadroConectarCon(Config *config) : QWidget(NULL, NULL, WStyle_Customize | WStyle_DialogBorder)
{
 setFixedSize(ANCHO_CONECTARCON, ALTO_CONECTARCON);
 setCaption("Connect to...");

 int x1 = MARCO;
 int x2 =  x1 + ANCHO_LABEL_CONECTARCON + DISTANCIA;
 int y = MARCO;

 labComentario = new QLabel("Comment: ", this);
 labComentario->setAlignment(AlignRight | AlignVCenter);
 labComentario->setGeometry(x1, y, ANCHO_LABEL_CONECTARCON, ALTO_LABEL);
 comentario = new QLineEdit(this);
 comentario->setGeometry(x2, y, width() - 2*MARCO -ANCHO_LABEL_CONECTARCON, ALTO_ENTRADA);
 y += ALTO_LABEL + DISTANCIA;

 labHost = new QLabel("Hostname: ", this);
 labHost->setAlignment(AlignRight | AlignVCenter);
 labHost->setGeometry(x1, y, ANCHO_LABEL_CONECTARCON, ALTO_LABEL);
 host = new QLineEdit(this);
 host->setMaxLength(63);
 host->setGeometry(x2, y, width() - 2*MARCO -ANCHO_LABEL_CONECTARCON , ALTO_ENTRADA);
 y += ALTO_LABEL + DISTANCIA;

 labPuerto = new QLabel("Port: ", this);
 labPuerto->setAlignment(AlignRight | AlignVCenter);
 labPuerto->setGeometry(x1, y, ANCHO_LABEL_CONECTARCON, ALTO_LABEL);
 puerto = new QLineEdit(this);
 puerto->setGeometry(x2, y, width() - 2*MARCO -ANCHO_LABEL_CONECTARCON, ALTO_ENTRADA);
 y += ALTO_LABEL + DISTANCIA;

 int distancia = int((width() - 3*ANCHO_BOTON) / 4);
 x1 = distancia * 2 + ANCHO_BOTON;
 x2 = distancia * 3 + 2 * ANCHO_BOTON;
 bAgregar = new QPushButton("Add", this);
 bAgregar->setGeometry(distancia, y, ANCHO_BOTON, ALTO_BOTON);
 bReemplazar = new QPushButton("Replace", this);
 bReemplazar->setGeometry(x1, y, ANCHO_BOTON, ALTO_BOTON);
 bBorrar = new QPushButton("Delete", this);
 bBorrar->setGeometry(x2, y, ANCHO_BOTON, ALTO_BOTON);
 y += ALTO_BOTON + DISTANCIA;

 lista = new QListView(this);
 x1 = MARCO;
 x2 = ANCHO_CONECTARCON - 2*MARCO;
 lista->setGeometry(x1, y, x2, ALTO_LISTA_CONECTARCON);
 lista->setAllColumnsShowFocus(true);
 lista->addColumn("Comment", (int)lista->width() / 2);
 lista->addColumn("Hostname", (int)lista->width() / 2 - 80);
 lista->addColumn("Port", 80);
 lista->setMultiSelection(false);

 y += ALTO_LISTA_CONECTARCON + DISTANCIA;

 distancia = int((ANCHO_CONECTARCON - 2*ANCHO_BOTON) / 3);
 x1 = distancia * 2 + ANCHO_BOTON;
 bConectar = new QPushButton("Connect", this);
 bConectar->setGeometry(distancia, y, ANCHO_BOTON, ALTO_BOTON);
 bCancelar = new QPushButton("Cancel", this);
 bCancelar->setGeometry(x1, y, ANCHO_BOTON, ALTO_BOTON);

 // hiu!!!... ahora, conectar las se�ales
 connect(bAgregar, SIGNAL(clicked()), SLOT(agregar()));
 connect(bReemplazar, SIGNAL(clicked()), SLOT(reemplazar()));
 connect(bBorrar, SIGNAL(clicked()), SLOT(borrar()));

 connect(bConectar, SIGNAL(clicked()), SLOT(conectar()));
 connect(bCancelar, SIGNAL(clicked()), SLOT(cancelar()));

 connect(lista, SIGNAL(doubleClicked(QListViewItem *)), SLOT(doubleClicked(QListViewItem *)));
 connect(lista, SIGNAL(currentChanged(QListViewItem *)), SLOT(currentChanged(QListViewItem *)));

 connect(comentario, SIGNAL(textChanged(const char *)), SLOT(probablyNewRecord(const char *)));
 connect(host, SIGNAL(textChanged(const char *)), SLOT(probablyNewRecord(const char *)));

 // se guarda el puntero al objeto Config de la aplicaci�n
 cuadroConectarCon::config = config;

 // cargo la lista de servidores
 cargarLista();

 // por �ltimo lo muestro
 show();
}

cuadroConectarCon::~cuadroConectarCon()
{
 // escondo la ventana para que no hayan parpadeos
 hide();

 // ahora borro todo
 delete labHost;
 delete host;
 delete labComentario;
 delete comentario;
 delete labPuerto;
 delete puerto;
 delete bAgregar;
 delete bReemplazar;
 delete bBorrar;
 delete lista;
 delete bConectar;
 delete bCancelar;
}

void cuadroConectarCon::doubleClicked(QListViewItem *item)
{
 Server server;
 server.comentario = new char[strlen(item->text(0)) + 1];
 strcpy(server.comentario, item->text(0));
 server.host = new char[strlen(item->text(1)) + 1];
 strcpy(server.host, item->text(1));
 server.puerto = atoi(item->text(2));

 actualizarLista();

 emit resultado(&server);
}

void cuadroConectarCon::currentChanged(QListViewItem *item)
{
 comentario->setText(item->text(0));
 host->setText(item->text(1));
 puerto->setText(item->text(2));
}

void cuadroConectarCon::probablyNewRecord(const char *)
{
 // si cambiaron algo y el campo puerto est� vacio, asumo que est� ingresando
 // un nuevo servidor as� que pongo el valor de puerto por defecto
 if (strlen(puerto->text()) == strspn(puerto->text(), " "))
 {
  puerto->setText("6667");
 }
}

void cuadroConectarCon::agregar()
{
 // primero me aseguro de que los datos est�n bien
 if(datosValidos() != true)
 {
   mostrarMensajeDatosInvalidos();
  return;
 }

 QListViewItem *item;
 item = new QListViewItem(lista,  comentario->text(), host->text(), puerto->text());
}

void cuadroConectarCon::reemplazar()
{
 // valido los datos
 if(datosValidos() != true)
 {
   mostrarMensajeDatosInvalidos();
  return;
 }

 // si hay algun item selecciondo de la lista, lo borro
 if(lista->currentItem() != 0)
 {
  // cuidado PORQUE INFORMATION ES MODAL!!!!!
  if(QMessageBox::information(NULL, "Warning", "Are you sure you want to replace the selected server?", "Yes, I am", "No, I'm not") == 0)
  {
  QListViewItem *item = lista->currentItem();
  item->setText(0, comentario->text());
  item->setText(1, host->text());
  item->setText(2, puerto->text());
  }
 }
}

void cuadroConectarCon::borrar()
{
 // si hay algun item selecciondo de la lista, lo borro
 if(lista->currentItem() != 0)
 {
  // cuidado PORQUE INFORMATION ES MODAL!!!!!
  if(QMessageBox::information(NULL, "Warning", "Are you sure you want to delete the selected item?", "Accept", "Cancel") == 0)
  {  delete lista->currentItem(); }
 }
}

void cuadroConectarCon::conectar()
{
 // si los datos no son v�lidos no acepto+
 if(datosValidos() != true)
 {
   mostrarMensajeDatosInvalidos();
  return;
 }

 // borrar esto
 actualizarLista();

 Server server;
 server.host = new char[strlen(host->text()) + 1];
 strcpy(server.host, host->text());
 server.comentario = new char[strlen(comentario->text()) + 1];
 strcpy(server.comentario, comentario->text());
 server.puerto = atoi(puerto->text());

 // oculto la ventana porque si el proceso de resoluci�n de nombres es lento, la ventan
 // va a estar abierta mientras dure la primera fase de la conexi�n dando un a muy
 // mala impresi�n. (notese que para el cancelar no es necesario)
 hide();

 actualizarLista();

 emit resultado(&server);
}

void cuadroConectarCon::cancelar()
{
 actualizarLista();

 emit resultado(NULL);
}

bool cuadroConectarCon::datosValidos()
{
 // por ahora solo me interesa que ninguno de los tres text est�n vac�os y que el tercero solo tenga n�meros
 if ((strlen(host->text()) == 0) || (strlen(puerto->text()) == 0))
 {
  return false;
 }

 if(strspn(puerto->text(), "0123456789") != strlen(puerto->text()))
 {
  return false;
 }

 return true;
}

void cuadroConectarCon::actualizarLista()
{
 // actualizo en la lista del objeto clase Config

 QListViewItem *item;
 int cantidad = 0;
 item = lista->firstChild();
 while (item != NULL)
 {
  char *linea = new char[strlen(CONFIG_SERVIDOR_H) + 11];  // para darle espacio
  sprintf(linea, "%s%i", CONFIG_SERVIDOR_C, cantidad);
  config->writeStrValue(linea, item->text(0));
  sprintf(linea, "%s%i", CONFIG_SERVIDOR_H, cantidad);
  config->writeStrValue(linea, item->text(1));
  sprintf(linea, "%s%i", CONFIG_SERVIDOR_P, cantidad);
  config->writeStrValue(linea, item->text(2));
  delete [] linea;

  cantidad++;
  item = item->itemBelow();
 }
 config->writeIntValue(CONFIG_CANTSERVIDORES, cantidad);
}

void cuadroConectarCon::cargarLista()
{
 // actualizo en la lista del objeto clase Config

 QListViewItem *item;
 int cantidad = config->readIntValue(CONFIG_CANTSERVIDORES, 0);
 char *a = new char[strlen(CONFIG_SERVIDOR_C) + 11];  // para darle espacio
 char *b = new char[strlen(CONFIG_SERVIDOR_H) + 11];  // para darle espacio
 char *c = new char[strlen(CONFIG_SERVIDOR_P) + 11];  // para darle espacio

 for (int o = 0; o < cantidad; o++)
 {
  sprintf(a, "%s%i", CONFIG_SERVIDOR_C, o);
  sprintf(b, "%s%i", CONFIG_SERVIDOR_H, o);
  sprintf(c, "%s%i", CONFIG_SERVIDOR_P, o);

  // notese que en todo este proceso todo el tiempo trato los datos (incluso puerto) COMO CADENAS
  item = new QListViewItem(lista, config->readStrValue(a, "this text shouldn't be here"), config->readStrValue(b, " "), config->readStrValue(c, " "));
 }
 delete [] a;
 delete [] b;
 delete [] c;
}

void cuadroConectarCon::mostrarMensajeDatosInvalidos()
{
 // solo muestra un mensaje de error; unicamente utilizado para unificar c�digo
 QMessageBox::information(NULL, "Warning", "You have to fill the comment, hostname and port fields", "Continuar");
}
