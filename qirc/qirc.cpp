/***************************************************************************
                          qirc.cpp  -  description
                             -------------------
    begin                : mi� sep 13 10:25:05 ART 2000
    copyright            : (C) 2000 by gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "qirc.h"

// objeto de configuraci�n; es necesario declararlo ac� porque es una variable
// externa
Config *config;

// los pixmaps usados en este archivo
// - el �cono principal
#include "qirc64x64.xpm"


#ifdef ENABLE_TOOLBAR
// - los �conos de la barra de herramientas
#include "connect.xpm"
#include "connectto.xpm"
#include "disconnect.xpm"
#include "join.xpm"
#include "part.xpm"
#include "list.xpm"
#include "dccsend.xpm"
#include "help.xpm"
#endif

// en esta variable almaceno la direcci�n para usarla en los dccsend
struct in_addr localIpAddress;

QIrc::QIrc(QWidget *parent = 0, char *nombre = 0) : QMainWindow(parent, nombre)
{
 // cargo el �cono de la aplicaci�n
 setIcon(QPixmap(qirc64x64));

 // creo la carpeta que almacenar� los logs del programa
 crearCarpetaLogs();

 // antes que nada, cargo la configuraci�n del archivo
 config = new Config(ARCHIVO_CONFIGURACION);

 // inicializo las variables
 inicializarVariables();

 // inicializo la direcci�n ip local para que apunte a 127.0.0.1
 localIpAddress.s_addr = inet_addr("127.0.0.1");

 // le doy a la ventana el tama�o que ten�a al cerrarse
 resize(config->readIntValue(CONFIG_MAINWIDTH, DEF_MAINWIDTH), config->readIntValue(CONFIG_MAINHEIGHT, DEF_MAINHEIGHT));

 #ifdef ENABLE_TOOLBAR
  // creo la barra de herramientas
  crearToolBar();
 #endif

 // dejo que se cree la barra de menu
 crearMenus();

 // dibujo el splitter y el ventanaChat en la ventana principal
 crearPantallaPrincipal();

 // conectar se�ales del IRCClient
 conectarSenialesIRC();

 // inicializo el timer
 inicializarTemporizador();

 // muestro el los avisos de GPL
 mostrarGpl();
}

QIrc::~QIrc()
{
 // borro s�lo los que no tiene un widget padre o que no son widgets
 delete ventanas;
 delete popupCanales;
 delete popupScreen;

 delete config;
}

void QIrc::mostrarGpl()
{
  screen->escribirLinea("", "QIrc for Linux", 4);
  screen->escribirLinea("", "--------------", COLOR_PROGRAMMESSAGE);
  screen->escribirLinea("Version:", VERSION, COLOR_PROGRAMMESSAGE);
  screen->escribirLinea("Tested under:", "Linux 2.2.14, Qt 1.45", COLOR_PROGRAMMESSAGE);
  screen->escribirLinea("Author:", "Gerardo Puga [gere@mailroom.com]", COLOR_PROGRAMMESSAGE);
  screen->escribirLinea("", "This program is Free Software and may be redistributed freely under the terms of the GNU General Public License.", COLOR_PROGRAMMESSAGE);
  screen->escribirLinea("", "QIrc comes with NO WARRANTIES OF ANY KIND.", COLOR_PROGRAMMESSAGE);

/*
  screen->escribirLinea("", "This program is free software and is protected under the terms of the GNU General", COLOR_PROGRAMMESSAGE);
  screen->escribirLinea("", "Public License version 2 of the license, or (at your option) any later version. ", COLOR_PROGRAMMESSAGE);
*/

  // I don't put the GPL notices because I don't know if this program
  // can be protected under this license because of having been coded
  // with the Qt LIbrary verion 1.45
  // I hope someday I will be able to say "this is GPL protected" :(

  // no coloco los avisos obligados de la gpl porque no estoy seguro de que
  // este programa pueda ser protegido por dicha licencia por haber sido
  // codificado utilizando la versi�n 1.45 de la librer�a Qt
  // espero alg�n d�a poder decir "protegido por la licencia GPL"
/*
  screen->escribirLinea("*", "  Este programa es Software Libre, y como tal puede ser distribuido y/o modificado", COLOR_PROGRAMMESSAGE);
  screen->escribirLinea("*", "bajo los t�rminos impuestos por la GNU General Public License en cualquiera de sus", COLOR_PROGRAMMESSAGE);
  screen->escribirLinea("*", "versiones (a tu elecci�n).", COLOR_PROGRAMMESSAGE);
  screen->escribirLinea("*", "  Este programa es distribuido con la esperanza de ser �til pero sin garant�as de", COLOR_PROGRAMMESSAGE);
  screen->escribirLinea("*", "ning�n tipo, incluida las de idoneidad para cumplir con un prop�sito determinado. Para", COLOR_PROGRAMMESSAGE);
  screen->escribirLinea("*", "mayor detalle sobre este apartado leer la GNU General Public License.", COLOR_PROGRAMMESSAGE);
  screen->escribirLinea("*", "  Una copia de la GNU General Public License deber�a haber acompa�ado este programa", COLOR_PROGRAMMESSAGE);
  screen->escribirLinea("*", "pero en caso contrario puedes conseguir una copia escribiendo a:", COLOR_PROGRAMMESSAGE);
  screen->escribirLinea("*", "  Free Software Foundation, Inc.", COLOR_PROGRAMMESSAGE);
  screen->escribirLinea("*", "  675 Mass Ave, Cambridge, MA 02139, USA", COLOR_PROGRAMMESSAGE);
  screen->escribirLinea("*", "O bien en Internet visitando www.gnu.org", COLOR_PROGRAMMESSAGE);
*/
  screen->escribirLinea("", " ");
}

void QIrc::crearToolBar()
{
#ifdef ENABLE_TOOLBAR
 tBar = new QToolBar(this);

 new QToolButton(QPixmap(connect_icon), "Connect to the last server", 0, this, SLOT(menuConectar()), tBar);
 new QToolButton(QPixmap(connectto_icon), "Connect to...", 0, this, SLOT(menuConectarCon()), tBar);
 new QToolButton(QPixmap(disconnect_icon), "Disconnect", 0, this, SLOT(menuDesconectar()), tBar);
 tBar->addSeparator();
 new QToolButton(QPixmap(join_icon), "Join channel", 0, this, SLOT(menuUnirseCanal()), tBar);
 new QToolButton(QPixmap(part_icon), "Part channel", 0, this, SLOT(menuSalirCanal()), tBar);
 new QToolButton(QPixmap(list_icon), "List channels", 0, this, SLOT(menuListarCanales()), tBar);
 tBar->addSeparator();
 new QToolButton(QPixmap(dccsend_icon), "DCC Send", 0, this, SLOT(menuDccSend()), tBar);
 tBar->addSeparator();
 new QToolButton(QPixmap(help_icon), "Help", 0, this, SLOT(menuInstrucciones()), tBar);

 // hago que todos los pixmaps de la barra sean grandes
 setUsesBigPixmaps(true);
#endif
}

void QIrc::crearMenus()
{
 // creo la barra de menues
 menu = menuBar();

 // creo los menues
 archivo	= new QPopupMenu;
 comando	= new QPopupMenu;
 dcc			= new QPopupMenu;
 setup		= new QPopupMenu;
 ayuda 		= new QPopupMenu;

 // los lleno
 archivo->insertItem("Connect", this, SLOT(menuConectar()), CTRL+Key_C);
 archivo->insertItem("Connect to...", this, SLOT(menuConectarCon()));
 archivo->insertItem("Disconnect", this, SLOT(menuDesconectar()), CTRL+Key_D);
 archivo->insertSeparator();
 archivo->insertItem("Quit", this, SLOT(menuSalir()), CTRL+Key_Q);

 comando->insertItem("Change Nick...", this, SLOT(menuCambiarNick()));
 comando->insertItem("Join channel...", this, SLOT(menuUnirseCanal()));
 comando->insertItem("Part channel...", this, SLOT(menuSalirCanal()));
 comando->insertItem("Set Away", this, SLOT(menuAway()), CTRL+Key_A, ID_AWAY); // es el �nico que necesita de away
 comando->insertSeparator();
 comando->insertItem("Get the channel list", this, SLOT(menuListarCanales()), CTRL+Key_L);
 comando->insertItem("Show server MOTD", this, SLOT(menuMotd()));
 comando->insertSeparator();
 comando->insertItem("Whois...", this, SLOT(menuQuienEs()));
 comando->setCheckable(true); // para que de entrada muestre que away est� desactivado

 dcc->insertItem("DCC Send", this, SLOT(menuDccSend()), CTRL+Key_S);

 setup->insertItem("General Setup", this, SLOT(menuConfigurar()));
 setup->insertSeparator();
 setup->insertItem("Ignore List", this, SLOT(menuIgnorar()));
 setup->insertItem("Notify List", this, SLOT(menuNotificar()));

 ayuda->insertItem("Commands", this, SLOT(menuInstrucciones()));
 ayuda->insertSeparator();
 ayuda->insertItem("Thanks!", this,  SLOT(menuGracias()));
 ayuda->insertSeparator();
 ayuda->insertItem("About QIrc", this,  SLOT(menuAbout()));
 ayuda->insertItem("About Qt", this, SLOT(menuAboutQt()));

 // los agrego a la barra
 menu->insertItem("File", archivo);
 menu->insertItem("IRC Commands", comando);
 menu->insertItem("DCC", dcc);
 menu->insertItem("Setup", setup);
 menu->insertItem("Help!", ayuda);
}


void QIrc::crearPantallaPrincipal()
{
 mainWidget = new MainTab(this);

 // le anexo el manejador de ventanas
 ventanas = new ManejadorVentanas(mainWidget);

 // la lista de canales
 listaCanales = new channelList(mainWidget);
 // conecto lal lista de canales
 connect(listaCanales, SIGNAL(doubleClicked(const QPoint)), SLOT(doubleClickEnCanal(const QPoint)));
 connect(listaCanales, SIGNAL(rightButtonPressed(const QPoint)), SLOT(abrirPopupCanales(const QPoint)));
 // creo el popup de los canales
 popupCanales = new QPopupMenu();
 popupCanales->insertItem("Join channel");
 popupCanales->insertItem("Show nicks");
 popupCanales->insertSeparator();
 popupCanales->insertItem("Update list");

 // creo la ventana de texto
 screen = new VentanaChat(mainWidget);
 connect(screen, SIGNAL(rightButtonPressed(const QPoint)), SLOT(abrirPopupScreen(const QPoint)));
 connect(screen, SIGNAL(ventanaActiva(VentanaChat *)), SLOT(ventanaPrincipalActiva(VentanaChat *)));
 // creo el popup de la ventan de texto
 popupScreen = new QPopupMenu();
 popupScreen->insertItem("Connect");
 popupScreen->insertItem("Connect to...");
 popupScreen->insertItem("Disconnect");
 popupScreen->insertSeparator();
 popupScreen->insertItem("Clean screen");

 visorNotifies = new notifyShowcase(mainWidget);

 mainWidget->addPage("Main", screen);
 mainWidget->addPage("Channel List", listaCanales);
 mainWidget->addPage("Notify List", visorNotifies);
 mainWidget->raisePage(screen);

 // establezco el widget central
 setCentralWidget(mainWidget);
}

void QIrc::conectarSenialesIRC()
{
 // la ventana de status
 connect(screen, SIGNAL(lineaDisponible(const char *, const char *)), SLOT(nuevoMensajeVentana(const char *, const char *)));
 // todas las dem�s ventanas
 connect(ventanas, SIGNAL(lineaDisponible(const char *, const char *)), SLOT(nuevoMensajeVentana(const char *, const char *)));

 connect(&cliente, SIGNAL(conexionCompleta()), SLOT(conexionCompleta()));
 connect(&cliente, SIGNAL(estatusConexion(const char *)), SLOT(estatusConexion(const char *)));
 connect(&cliente, SIGNAL(loginCompleto(const char *)), SLOT(loginCompleto(const char *)));
 connect(&cliente, SIGNAL(desconexion()), SLOT(desconexion()));
 connect(&cliente, SIGNAL(replyCode(IRCEvent *)), SLOT(replyCode(IRCEvent *)));

 connect(&cliente, SIGNAL(noReconocido(IRCEvent *)), SLOT(noReconocido(IRCEvent *)));

 connect(&cliente, SIGNAL(nick(const char *, const char *)), SLOT(nick(const char *, const char *)));
 connect(&cliente, SIGNAL(quit(const char *, const char *)), SLOT(quit(const char *, const char *)));
 connect(&cliente, SIGNAL(join(const char *, const char *)), SLOT(join(const char *, const char *)));
 connect(&cliente, SIGNAL(mode(IRCEvent *)), SLOT(mode(IRCEvent *)));
 connect(&cliente, SIGNAL(part(const char *, const char *, const char *)), SLOT(part(const char *, const char *, const char *)));
 connect(&cliente, SIGNAL(topic(const char*, const char *, const char*)), SLOT(topic(const char *, const char *, const char *)));
 connect(&cliente, SIGNAL(invite(const char *, const char *)), SLOT(invite(const char *, const char *)));
 connect(&cliente, SIGNAL(kick(const char *, const char *, const char *, const char *)), SLOT(kick(const char *, const char*, const char *, const char*)));
 connect(&cliente, SIGNAL(privmsg(const char *, const char *, const char *)), SLOT(privmsg(const char *, const char *, const char *)));
 connect(&cliente, SIGNAL(notice(const char *, const char *, const char *)), SLOT(notice(const char *, const char *, const char *)));
 connect(&cliente, SIGNAL(ping(const char *)), SLOT(ping(const char *)));
 connect(&cliente, SIGNAL(error(const char *)), SLOT(error(const char *)));
}

void QIrc::inicializarVariables()
{
 // le doy a mi nick por ahora una cadena vacia ( NO NULA! )
 registeredNick = "";
 // no hay un cuadro de entrada creado
 cuadroEntrada = NULL;
 // tampoco hay uno de cuadro
 conectarCon = NULL;
 // ni la ventana de ignores
 cuadroIgnores = NULL;
 // ni la ventana de notifies
 cuadroNotificar = NULL;
 // ni una ventana de configuraci�n
 configuracion = NULL;
 // no estoy conectado
 logged = false;
 conectado = false;
 // para que cuando reciba un RPL_LIST borre la lista de canales
 recibiendoListaCanales = false;
 // lleva cuenta de cuantas ventanas dcc estan abiertas en un momento dado
 cantidadVentanasDccAbiertas = 0;
 // proximo ison
 proximoIson = 0;
}

void QIrc::inicializarTemporizador()
{
 // creo e inicializo el timer para que salte cada un segundo
 startTimer(1000);
}

void QIrc::closeEvent(QCloseEvent *e)
{
 // ignoro el evento y llamo la funcion de salida
 e->ignore();

 menuSalir();
}

void QIrc::extraerCtcpQuery(const char *nick, const char *destino, char *linea)
{
 // extrae los ctcp query (los que llegan por privmsg)

 // para �l que como yo haya leido el texto de "estandarizaci�n" del CTCP y no haya
 // entendido un pito, en resumen significa que se
 //  - un ctcp se reconoce porque viene delimitado por dos caracteres de
 //    valor hex 0x01
 //  - puede venir s�lo o mezclado con un privmsg, y puede estar dirigido a
 //    el usuario o a un canal
 //  - se responde utilizando notice y la respuesta tambi�n va delimitada por 0x01

 // en esta funci�n busco los ctcp de la cadena, los trato y los borro, that's all

 bool esCtcp = false;
 int indice = 0, borrador = 0;
 char aux[strlen(linea)+1]; //es m�s de lo que puede medir el ctcp

 while(linea[indice] != 0)
 {
  if (linea[indice] == 1)
  {
   esCtcp = !esCtcp; // lo invierto
   if (esCtcp == true)
   { strcpy(aux, ""); // la vacio
   } else {
    bool ctcpVisible = true; // para hacer que no todos los ctcp sean visibles (por ejemplo, ocultar los action)
    bool ignorado = true;

    // el intervalo antiflood no es v�lido para el dcc, que tiene sus propias
    // formas de evitar molestias
    int maximoVentanas = config->readIntValue(CONFIG_DCCMAXWINDOWSCOUNT, DEF_DCCMAXWINDOWSCOUNT);
    if ((maximoVentanas == 0) || (cantidadVentanasDccAbiertas < maximoVentanas))
    {
     if (strncmp(aux, "DCC SEND", 8) == 0)
     {
      // no se muestra en pantalla
      ctcpVisible = false;
      // parseo los par�metros
      QString fileName, strAddress, strPort, strFileSize;
      fileName = strAddress = strPort = strFileSize = "";
      int indice = 8;
      indice += strspn(aux + indice, " "); // salteo los espacios
      for(; (aux[indice] != ' ') && (aux[indice] != '\x00'); indice++)
      {
       fileName = fileName + aux[indice];
      }
      indice += strspn(aux + indice, " "); // salteo los espacios
      for(; (aux[indice] != ' ') && (aux[indice] != '\x00'); indice++)
      {
       strAddress = strAddress + aux[indice];
      }
      indice += strspn(aux + indice, " "); // salteo los espacios
      for(; (aux[indice] != ' ') && (aux[indice] != '\x00'); indice++)
      {
       strPort = strPort + aux[indice];
      }
      indice += strspn(aux + indice, " "); // salteo los espacios
      for(; (aux[indice] != ' ') && (aux[indice] != '\x00'); indice++)
      {
       strFileSize = strFileSize + aux[indice];
      }
      char linea[strlen("From:") + 1 + strlen(nick) + 3 + strlen("File:") + 1 + strlen((const char *)fileName)
                 + 3 + strlen("Size:") + 1 + strlen((const char *)strFileSize) + 1 + strlen("bytes") + 1];
      sprintf(linea, "From: %s - File: %s - Size: %s bytes", nick, (const char *)fileName, (const char *)strFileSize);
      screen->escribirLinea(PREFIJO_DCCSEND, linea, COLOR_DCC);

      // transformo los n�meros parseados
      unsigned long fileSize = strtoul((const char *)strFileSize, (char **)NULL, 10);
      unsigned int port = atol((const char *)strPort); // deber�a ser short int
      in_addr ip;
      ip.s_addr = htonl(strtoul((const char *)strAddress, (char **)NULL, 10));

      // y creo la ventana de dcc correspondiente
      DccGet *dcc;
      dcc = new DccGet(nick, (const char *)fileName, ip, port, fileSize);
      connect(dcc, SIGNAL(cerrarDccGet(DccGet *)), SLOT(entradaDccGet(DccGet *)));
      // incremento el contador de ventnas
      cantidadVentanasDccAbiertas++;
     }
    } else {
      // aviso en la pantalla principal que hubo alg�n dcc ignorado
      ctcpVisible = false;
      char linea[strlen("Ignored DCC send from") + 1 + strlen(nick) + 1];
      sprintf(linea, "Ignored DCC Send from %s", nick);
      screen->escribirLinea(PREFIJO_DCCSEND, linea, COLOR_DCC);
    }
    // el action no puede est� limitado ni por tiempo ni por cantidad de mensajes
	  if (strncasecmp(aux, "ACTION", 6) == 0)
    {
     // no muestro nada, sino que muestro en el canal que corresponda el mensaje
     // que acompa�a el action
     // me aseguro de que este no se vea en la ventana principal
     ctcpVisible = false;
     // si soy el destinatario, lo envio a la ventana del nick que lo envio
     VentanaChat *v;
     if (strcasecmp(destino, (const char *)registeredNick) == 0)
     {
      // es para m�, si la ventana no existe la creo
      v = ventanas->punteroVentana(nick);
      if (v == NULL) v = ventanas->crearVentana(nick, false);
      // si as� est� configurado, elevo las ventanas de privados al recibir mensajes (solo en las ventnas)
      if (config->readBoolValue(CONFIG_ELEVARPRIVADOS, DEF_ELEVARPRIVADOS) == true)
      {
       mainWidget->raisePage(v);
      }
     } else {
       // no es para mi, as� que tiene que ser para un canal
       // como no habr�a recibido el mensaje si no estuviera en el canal
       // (y que por ende la ventana exista) no la creo
       v = ventanas->punteroVentana(destino);
     }
     char linea[strlen(nick) + 1 + strlen(aux) + 1];
     sprintf(linea, "%s %s", nick, aux+7); // +7 para quitar la parte de "ACTION "
     if (v != NULL) v->escribirLinea(PREFIJO_ACTION, linea, COLOR_ACTION);
    }
    // a partir de ac� los ctcp que estan afectados por el retraso antiflood
    if (ctcpDelay == 0)
    {
     ignorado = false; // para poder indicar cuando ignor� un mensaje ctcp

     ctcpDelay = config->readIntValue(CONFIG_CTCPINTERVAL, DEF_CTCPINTERVAL);
     // ac� es donde hago el tratamiento de los ctcp
     if (strncasecmp(aux, "PING", 4) == 0)
     {
      sendCtcpReply(nick, "PONG");
     }
     if (strcasecmp(aux, "FINGER") == 0)
     {
      sendCtcpReply(nick, "FINGER", config->readStrValue(CONFIG_CTCPFINGER, DEF_CTCPFINGER));
     }
     if (strcasecmp(aux, "USERINFO") == 0)
     {
      sendCtcpReply(nick, "USERINFO", config->readStrValue(CONFIG_CTCPUSERINFO, DEF_CTCPUSERINFO));
     }
     if ((strcasecmp(aux, "VERSION") == 0) && (config->readBoolValue(CONFIG_RESPONDERCTCPVERSION, DEF_RESPONDERCTCPVERSION) == true))
     {
      sendCtcpReply(nick, "VERSION", CTCP_VERSION_REPLY);
     }
     // si el usuario lo quiere o si es una respuesta (contiene por lo menos un espacio) lo muestr
    } // ac� temrmina el if (ctcpDelay == 0)
    if((config->readBoolValue(CONFIG_VERCTCP, DEF_VERCTCP) == true) && (ctcpVisible == true))
    {
     char nuevaLinea[600+1]; // para darle suficiente espacio
     if (ignorado == false) {
      snprintf(nuevaLinea, 600, "[CTCP Query] <Source: %s> <Target: %s> <Command: %s>", nick, destino, aux);
     } else {
      snprintf(nuevaLinea, 600, "[CTCP Query] <Source: %s> <Target: %s> <Command: %s> %c04[IGNORED!]", nick, destino, aux, 03);
     }
     screen->escribirLinea(PREFIJO_CTCP, nuevaLinea, COLOR_CTCPQUERY);
    }
   }
  } else {
   if(esCtcp == true)
   {
    strncat(aux, &linea[indice], 1);
   } else {
    // si los �ndices son diferentes, copio el caracter de indice en el de borrardor
    // as� sobreescribo el ctcp con el resto del mensaje
    if (indice != borrador) linea[borrador] = linea[indice];

    // no est� dentro del ctcp ni es delimitador, as� que incremento el caracter
    borrador++;
   }
  }
  // este se incrementa siempre, no importa donde est�
  indice++;
 }
 linea[borrador] = 0;
}

void QIrc::extraerCtcpReply(const char *nick, char *linea)
{
 // extrae los ctcp reply (los que llegan por notice)

 // para �l que como yo haya leido el texto de "estandarizaci�n" del CTCP y no haya
 // entendido un pito, en resumen significa que se
 //  - un ctcp se reconoce porque viene delimitado por dos caracteres de
 //    valor hex 0x01
 //  - puede venir s�lo o mezclado con un privmsg, y puede estar dirigido a
 //    el usuario o a un canal
 //  - se responde utilizando notice y la respuesta tambi�n va delimitada por 0x01

 // en esta funci�n busco los ctcp de la cadena, los trato y los borro, that's all

 bool esCtcp = false;
 int indice = 0, borrador = 0;
 char aux[strlen(linea)+1]; //es m�s de lo que puede medir el ctcp

 while(linea[indice] != 0)
 {
  if (linea[indice] == 1)
  {
   esCtcp = !esCtcp; // lo invierto
   if (esCtcp == true)
   { strcpy(aux, ""); // la vacio
   } else {
    // si el usuario lo quiere o si es una respuesta (contiene por lo menos un espacio) lo muestro
    char nuevaLinea[strlen("CTCP Reply from") + 1 + strlen(nick) + 2 + strlen(aux) +1];
    sprintf(nuevaLinea, "CTCP Reply from %s: %s", nick, aux);
    screen->escribirLinea(PREFIJO_CTCP, nuevaLinea, COLOR_CTCPREPLY);
   }
  } else {
   if(esCtcp == true)
   {
    strncat(aux, &linea[indice], 1);
   } else {
    // si los �ndices son diferentes, copio el caracter de indice en el de borrardor
    // as� sobreescribo el ctcp con el resto del mensaje
    if (indice != borrador) linea[borrador] = linea[indice];

    // no est� dentro del ctcp ni es delimitador, as� que incremento el caracter
    borrador++;
   }
  }
  // este se incrementa siempre, no importa donde est�
  indice++;
 }
 linea[borrador] = 0;
}


void QIrc::sendCtcpQuery (const char *nick, const char *ctcpCommand, const char *datos = NULL)
{
 // los ctcp query van por privmsg y los reply por notice (estandar, rengl�n 162)
 int size = strlen(COMMAND_PRIVMSG) + 1 + strlen(nick) + 3 + strlen(ctcpCommand) + 2;
 if(datos != NULL) size += strlen(datos) + 2;

 char mensaje[size];
 sprintf(mensaje, "%s %s :\x01%s", COMMAND_PRIVMSG, nick, ctcpCommand);
 if (datos != NULL){
  strcat(mensaje, " ");
  strcat(mensaje, datos);
 }
 strcat(mensaje, "\x01");
 cliente.enviarLinea(mensaje);
}

void QIrc::sendCtcpReply (const char *nick, const char *ctcpCommand, const char *datos = NULL)
{
 // los ctcp query van por privmsg y los reply por notice (estandar, rengl�n 162)
 int size = strlen(COMMAND_NOTICE) + 1 + strlen(nick) + 3 + strlen(ctcpCommand) + 2;
 if(datos != NULL) size += strlen(datos) + 2;

 char mensaje[size];
 sprintf(mensaje, "%s %s :\x01%s", COMMAND_NOTICE, nick, ctcpCommand);
 if (datos != NULL){
  strcat(mensaje, " ");
  strcat(mensaje, datos);
 }
 strcat(mensaje, "\x01");

 cliente.enviarLinea(mensaje);
}

bool QIrc::ignorado(const char *nick)
{
 // revisa en la lista de ignorado para ver si el nick
 // fue marcado como ignorado
 bool resultado = false;
 char key[strlen(CONFIG_IGNOREDNICK) + 21];
 int cantidad = config->readIntValue(CONFIG_IGNORELISTCOUNT, 0);
 for (int i = 0; i < cantidad; i++)
 {
  sprintf(key, "%s%i", CONFIG_IGNOREDNICK, i);
  if (strcasecmp(nick, config->readStrValue(key, "THIS IS A BUG! SEND ME AN EMAIL!!!")) == 0)
  {
    resultado = true;
    break;
  }
 }
 return resultado;
}

void QIrc::doubleClickEnCanal(const QPoint globalCoordinates)
{
 cliente.enviarComando(1, COMMAND_JOIN, listaCanales->selectedChannel());
}

void QIrc::abrirPopupCanales(const QPoint globalCoordinates)
{
 int resultado = popupCanales->exec(globalCoordinates);
 if (resultado < 0) return;

 switch(resultado) {
  case 0 :  // unirse al canal
  					cliente.enviarComando(1, COMMAND_JOIN, listaCanales->selectedChannel());
     				break;
  case 1 : 	// listar usuarios
  					cliente.enviarComando(1, COMMAND_NAMES, listaCanales->selectedChannel());
						break;
  case 3 : menuListarCanales(); break;
 }
}

void QIrc::abrirPopupScreen(const QPoint coordinates)
{
 int resultado = popupScreen->exec(coordinates);
 if (resultado < 0) return;

 switch(resultado) {
  case 0 : menuConectar(); break;
  case 1 : menuConectarCon(); break;
  case 2 : menuDesconectar(); break;
  case 4 : screen->clear(); break;
 }
}

void QIrc::timerEvent(QTimerEvent *e)
{
 // salta una vez cada segundo

 // lleva la cuenta de los retardos de ctcp
 if(ctcpDelay > 0) ctcpDelay--;

 // ac� los proceso que se temporizan en minutos
 if (logged == true)
 {
  if (proximoIson <= 0)
  {
   int cantidad = config->readIntValue(CONFIG_NOTIFIESLISTCOUNT, 0);
   char linea[511];
   strcpy(linea, "ISON : ");
   for (int i = 0; i < cantidad; i++)
   {
    char keyName[100];
    sprintf(keyName, "%s%i", CONFIG_NOTIFYNICK, i);
    const char *nick = config->readStrValue(keyName, "nothing");
    if ((strlen(linea) + 1 + strlen(nick) >= 510))
    {
     cliente.enviarLinea(linea);
     strcpy(linea, "ISON : ");
    }
    strncat(linea, nick, 510 - strlen(linea) - 1); // para evitar ciclos infinitos si el primer nick de una linea es demasiado largo
    strcat(linea, " "); // el separador de nicks
   }
   cliente.enviarLinea(linea); // envio lo que quede en la linea, en el peor de los casos
                              // (habiendo alg�n nick) hay por lo menos un nick
                              // (no habiendo ninguno en la lista) mando el ison pelado
   // reinicio este temporizador
   proximoIson = config->readIntValue(CONFIG_PAUSANOTIFY, DEF_PAUSANOTIFY);
  }
  proximoIson--;
 }
}

void QIrc::nuevoMensajeVentana(const char *ventana, const char * mensaje)
{
 // toma una linea para mandar y
 // - si empieza por "/" la manda tal cual sin la barra
 // - si no empieza por "/", supone que es un mensaje privado, a menos que sea de una ventana sin nombre
 //   que solo puede ser la de status
 if (mensaje == NULL) return;

 char cadenaNick[strlen((const char *)QIrc::registeredNick) + 5];

 VentanaChat *origen;
 if (ventana == NULL) {
  origen = screen;
 } else {
  origen = ventanas->punteroVentana(ventana);
 }

 // atenci�n, porque es la �nica vez en todo el programa que uso esta clase
 // especialemente dise�ada :)

 if (mensaje[0] == '/') // es un comando
 {
  ProgramCommand e(mensaje + 1); // el +1 para borrar la barra

  // para que no joda lo mando al a pantalla principal
  screen->escribirLinea(PREFIJO_COMANDO, mensaje, COLOR_COMANDO);

  // ahora me tengo que fijar
  // a) si es un mensaje propio del programa que no se env�a al server
  // b) si es un comando del server que tengo que tener en cuenta
  // c) si es cualquiera de los otros

  // comandos del programa
  if (strcasecmp(e.getCommand(), "EXIT") == 0)
  {
   // EXIT
   // sale del programa
   menuSalir();
   return;
  }
  if (strcasecmp(e.getCommand(), "IGNORE") == 0)
  {
   // IGNORE <nick>
   // agrega un nick a la lista de ignoredNicks
   // si la ventana de ignored nicks est� abierta, no permite la modificaci�n
   // para evitar inconsistencias. Tambi�n me asuguro de que haya un par�metro
   if (cuadroIgnores == NULL)
   {
    if (strspn(e.getParam(0), " ") != strlen(e.getParam(0)))
    { // es un nombre v�lido
     char key[strlen(CONFIG_IGNOREDNICK) + 21];
     sprintf(key, "%s%i", CONFIG_IGNOREDNICK, config->readIntValue(CONFIG_IGNORELISTCOUNT, 0));
     config->writeStrValue(key, e.getParam(0)); // el primer parametro es el nick a ignorar
     config->writeIntValue(CONFIG_IGNORELISTCOUNT, config->readIntValue(CONFIG_IGNORELISTCOUNT, 0)+1);
    }
   } else {
    origen->escribirLinea(PREFIJO_AVISOPROGRAMA, "You can't add a new ignored nick while the ignored nicks list is open", COLOR_ERROR);
   }
   // salgo, porque no quiero que llegue al server
   return;
  }
  if (strcasecmp(e.getCommand(), "CLEAR") == 0)
  {
   origen->clear();
   // salgo, porque no quiero que llegue al server
   return;
  }
  if (strcasecmp(e.getCommand(), "CONNECT") == 0)
  {
   // CONNECT [<server> [<puerto>]]
   // se vuelve a conectar al �ltimo servidor (si est� sin par�metros) o se conecta con el
   // server <server> y si est� dado al puerto <puerto> (si no est� se asume 6667)
   if (e.paramCount() >= 1) // tengo par�metros
   {
    if (e.paramCount() >= 2)
    {
     // se me pas� el parametro de puerto
     if (strspn(e.getParam(1), "0123456789") != strlen(e.getParam(1)))
     {
      // el n�mero de puerto no es un n�mero v�lido
      origen->escribirLinea(PREFIJO_ERROR, "The port number is not valid", COLOR_ERROR);
      return;
     }

     config->writeIntValue(CONFIG_PUERTOPORDEFECTO, atoi(e.getParam(1)));
    } else {
     // no se especific� el puerto, se asume 6667
     config->writeIntValue(CONFIG_PUERTOPORDEFECTO, 6667);
    }
    config->writeStrValue(CONFIG_HOSTPORDEFECTO, e.getParam(0));
   }
   menuConectar();
   return;
	}
  if (strcasecmp(e.getCommand(), "CTCP") == 0)
  {
   if (e.paramCount() < 2)
   {
    screen->escribirLinea(PREFIJO_AYUDA, "Usage: /CTCP <destination> <command> [:<ctcp data>]", COLOR_ERROR);
    return;
   }
   if (e.paramCount() >= 3) {
    // tiene un parametro de datos
    sendCtcpQuery(e.getParam(0), e.getParam(1), e.getFinalParam(2));
   } else {
    // no tiene parametro de datos
    sendCtcpQuery(e.getParam(0), e.getParam(1));
   }
   return;
  }
  if (strcasecmp(e.getCommand(), "RCTCP") == 0)
  {
   if (e.paramCount() < 2)
   {
    screen->escribirLinea(PREFIJO_AYUDA, "Usage: /RCTCP <destination> <command> [:<ctcp data>]", COLOR_ERROR);
    return;
   }
   if (e.paramCount() >= 3) {
    // tiene un parametro de datos
    sendCtcpReply(e.getParam(0), e.getParam(1), e.getFinalParam(2));
   } else {
    // no tiene parametro de datos
    sendCtcpReply(e.getParam(0), e.getParam(1));
   }
   return;
  }
  if (strcasecmp(e.getCommand(), "DCCSEND") == 0)
  {
   if (e.paramCount() < 1) {
    screen->escribirLinea(PREFIJO_AYUDA, "Usage: /DCCSEND <nick> [<file>]", COLOR_ERROR);
    return;
   }
   // creo la ventana
   DccSend *dccWindow;
   dccWindow = new DccSend(e.getParam(0), e.getParam(1));
   // la conecto para saber cuando se cierra
   connect(dccWindow, SIGNAL(dccSendCerrar(DccSend *)), SLOT(entradaDccSend(DccSend *)));
   connect(dccWindow, SIGNAL(lineaComando(const char *, const char *)), SLOT(nuevoMensajeVentana(const char *, const char *)));
   return;
  }
  if (strcasecmp(e.getCommand(), "FINGER") == 0)
  {
   if (e.paramCount() != 1)
   {
    screen->escribirLinea(PREFIJO_AYUDA, "Usage: /FINGER <nick>", COLOR_ERROR);
   } else {
    // no tiene parametro de datos
    sendCtcpQuery(e.getParam(0), "FINGER");
   }
   return;
  }
  if (strcasecmp(e.getCommand(), "VERSION") == 0)
  {
   if (e.paramCount() != 1)
   {
    screen->escribirLinea(PREFIJO_AYUDA, "Usage: /VERSION <nick>", COLOR_ERROR);
   } else {
    // no tiene parametro de datos
    sendCtcpQuery(e.getParam(0), "VERSION");
   }
   return;
  }
  if (strcasecmp(e.getCommand(), "QUOTE") == 0)
  {
   if (e.paramCount() < 1)
   {
    screen->escribirLinea(PREFIJO_AYUDA, "Usage: /QUOTE :<irc command>", COLOR_ERROR);
   } else {
    cliente.enviarLinea(e.getFinalParam(0)); // paso tal cual el comando
   }
   return;
  }
  if (strcasecmp(e.getCommand(), "ACTION") == 0)
  {
   // para este en particular se dan dos casos: que estes en la ventana principal o que no
   if(ventana == NULL)
   {
    if (e.paramCount() < 2)
    {
     screen->escribirLinea(PREFIJO_AYUDA, "Usage: /ACTION <destination> :<message>", COLOR_ERROR);
    } else {
     // envio el mensaje
     sendCtcpQuery(e.getParam(0), "ACTION", e.getFinalParam(1));
     VentanaChat *v = ventanas->punteroVentana(e.getParam(0));
     char linea[strlen(registeredNick) + 3 + strlen(e.getFinalParam(1)) + 1];
     sprintf(linea, "%s %s", (const char *)registeredNick, e.getFinalParam(1));
     if (v != NULL) v->escribirLinea(PREFIJO_ACTION, linea, COLOR_ACTION);
    }
    return;
   } else {
    if (e.paramCount() < 1)
    {
     screen->escribirLinea(PREFIJO_AYUDA, "Usage: /ACTION :<message>", COLOR_ERROR);
    } else {
     sendCtcpQuery(ventana, "ACTION", e.getFinalParam(0));
     // lo muestro en mi pantalla
     char linea[strlen(registeredNick) + 3 + strlen(e.getFinalParam(0)) + 1];
     sprintf(linea, "%s %s", (const char *)registeredNick, e.getFinalParam(0));
     if (origen != NULL) origen->escribirLinea(PREFIJO_ACTION, linea, COLOR_ACTION);
    }
    return;
   }
  }
  if (strcasecmp(e.getCommand(), "USERINFO") == 0)
  {
   if (e.paramCount() != 1)
   {
    screen->escribirLinea(PREFIJO_AYUDA, "Usage: /USERINFO <nick>", COLOR_ERROR);
   } else {
    // no tiene parametro de datos
    sendCtcpQuery(e.getParam(0), "USERINFO");
   }
   return;
  }
  if (strcasecmp(e.getCommand(), "HELP") == 0)
  {
   screen->escribirLinea(PREFIJO_AYUDA, "QIrc commands", COLOR_PROGRAMMESSAGE);
   screen->escribirLinea(PREFIJO_AYUDA, "--------------------------------", COLOR_PROGRAMMESSAGE);
   screen->escribirLinea(PREFIJO_AYUDA, "/ACTION [<destination>] <message>");
   screen->escribirLinea(PREFIJO_AYUDA, "  Sends a ctcp message to a channel or a nick.", COLOR_PROGRAMMESSAGE);
   screen->escribirLinea(PREFIJO_AYUDA, "  You only have to specify the destination", COLOR_PROGRAMMESSAGE);
   screen->escribirLinea(PREFIJO_AYUDA, "  if you are in the main window.", COLOR_PROGRAMMESSAGE);
   screen->escribirLinea(PREFIJO_AYUDA, "/CLEAR");
   screen->escribirLinea(PREFIJO_AYUDA, "  Cleans the screen", COLOR_PROGRAMMESSAGE);
   screen->escribirLinea(PREFIJO_AYUDA, "/CONNECT [<server> [<port>]]");
   screen->escribirLinea(PREFIJO_AYUDA, "  Connects to the server. Without parameters it", COLOR_PROGRAMMESSAGE);
   screen->escribirLinea(PREFIJO_AYUDA, "  reconnects to the last server. If you don't", COLOR_PROGRAMMESSAGE);
   screen->escribirLinea(PREFIJO_AYUDA, "  specify the port, the default port number", COLOR_PROGRAMMESSAGE);
   screen->escribirLinea(PREFIJO_AYUDA, "  is 6667", COLOR_PROGRAMMESSAGE);
   screen->escribirLinea(PREFIJO_AYUDA, "/CTCP <destination> <command> [<ctcp data>]");
   screen->escribirLinea(PREFIJO_AYUDA, "  Sends a CTCP command to a nick or a channel", COLOR_PROGRAMMESSAGE);
   screen->escribirLinea(PREFIJO_AYUDA, "/DISCONNECT, /QUIT");
   screen->escribirLinea(PREFIJO_AYUDA, "  Disconnect from the server.", COLOR_PROGRAMMESSAGE);
   screen->escribirLinea(PREFIJO_AYUDA, "/DCCSEND <nick> [<file>]");
   screen->escribirLinea(PREFIJO_AYUDA, "  Opens a new DCC Send window.", COLOR_PROGRAMMESSAGE);
   screen->escribirLinea(PREFIJO_AYUDA, "/EXIT");
   screen->escribirLinea(PREFIJO_AYUDA, "  Quits the program", COLOR_PROGRAMMESSAGE);
   screen->escribirLinea(PREFIJO_AYUDA, "/FINGER <nick>");
   screen->escribirLinea(PREFIJO_AYUDA, "  Sends a CTCP FINGER to somebody", COLOR_PROGRAMMESSAGE);
   screen->escribirLinea(PREFIJO_AYUDA, "/HELP");
   screen->escribirLinea(PREFIJO_AYUDA, "  Shows this help.", COLOR_PROGRAMMESSAGE);
   screen->escribirLinea(PREFIJO_AYUDA, "/IGNORE <nick>");
   screen->escribirLinea(PREFIJO_AYUDA, "  Adds a new nick to the ignored nick list", COLOR_PROGRAMMESSAGE);
   screen->escribirLinea(PREFIJO_AYUDA, "/QUOTE <irc command>");
   screen->escribirLinea(PREFIJO_AYUDA, "  Sends the irc command directly to the server.", COLOR_PROGRAMMESSAGE);
   screen->escribirLinea(PREFIJO_AYUDA, "/RCTCP <destination> <command> [<ctcp data>]");
   screen->escribirLinea(PREFIJO_AYUDA, "  Sends a CTCP Reply to a nick or channel.", COLOR_PROGRAMMESSAGE);
   screen->escribirLinea(PREFIJO_AYUDA, "/SHELP");
   screen->escribirLinea(PREFIJO_AYUDA, "  Shows the server command list.", COLOR_PROGRAMMESSAGE);
   screen->escribirLinea(PREFIJO_AYUDA, "/VERSION <nick>");
   screen->escribirLinea(PREFIJO_AYUDA, "  Sends a CTCP VERSION to the specified nick.", COLOR_PROGRAMMESSAGE);
   screen->escribirLinea(PREFIJO_AYUDA, "/USERINFO <nick>");
   screen->escribirLinea(PREFIJO_AYUDA, "  Sends a CTCP USERINFO to the specified nick.", COLOR_PROGRAMMESSAGE);
   screen->escribirLinea(PREFIJO_AYUDA, " ");
   screen->escribirLinea(PREFIJO_AYUDA, "You can also send any IRC command (/JOIN, /PART,", COLOR_PROGRAMMESSAGE);
   screen->escribirLinea(PREFIJO_AYUDA, "/NAMES) in the format of the IRC Protocol", COLOR_PROGRAMMESSAGE);
   screen->escribirLinea(PREFIJO_AYUDA, "RFC 2812.", COLOR_PROGRAMMESSAGE);
   screen->escribirLinea(PREFIJO_AYUDA, " ");
   return;
  }
  if (strcasecmp(e.getCommand(), "SHELP") == 0)
  {
   // env�a un comando HELP al server para que liste los comandos que acepta
   cliente.enviarLinea(mensaje+2); // as� elimino el "/S"
   return; // el comando ya est� enviado
  }
  // comandos que tengo que tener en cuenta
  if (strcasecmp(e.getCommand(), COMMAND_PRIVMSG) == 0)
  {
   // adorno un poco el nick
   sprintf(cadenaNick, "[%s]", (const char *)QIrc::registeredNick);
   VentanaChat *v = ventanas->punteroVentana(e.getParam(0));
   if (v != NULL) // nunca deber�a ser null, pero por si las dudas
   {
    v->escribirLinea(cadenaNick, e.getFinalParam(1));
   }
   cliente.enviarComando(2, COMMAND_PRIVMSG, e.getParam(0), e.getFinalParam(1));
   return;
  }
  if (strcasecmp(e.getCommand(), COMMAND_NICK) == 0)
  {
   // este s�lo lo tengo que tener en cuenta si el programa no est� conectado a un server
   // para que se pueda cambar el nick estando offline
   if (logged == false)
   {
    if (e.paramCount() != 1)
    { screen->escribirLinea(PREFIJO_AYUDA, "Usage: /NICK <new nickname>", COLOR_ERROR);
    } else {
     // me aseguro de que sea un nickname v�lido (ver secci�n 2.3.1 del rfc 2812
     if (validNickname(e.getParam(0)) == true)
     {
      // la primera letra est� entre los caracteres v�lidos
      // hay que remarcar que si el caracter [0] del la cadena fuera un cero (cadena vac�a)
      // entonces strchr devolver�a != NULL (!!!!!) pero como ya me hab�a asegurado de que
      // haya por lo menos un parametro entonces eso no es posible
      config->writeStrValue(CONFIG_NICK1, e.getParam(0));
     } else {
      // el nick no es v�lido y el server se va a quejar cuando trate de conectarme
      screen->escribirLinea(PREFIJO_AYUDA, "Invalid Nickname!", COLOR_ERROR);
     }
    }
   }
  }
  if (strcasecmp(e.getCommand(), COMMAND_NOTICE) == 0)
  {
   sprintf(cadenaNick, "++%s++", (const char *)QIrc::registeredNick);
   VentanaChat *v = ventanas->punteroVentana(e.getParam(0));
   if (v != NULL) // nunca deber�a ser null, pero por si las dudas
   {
    v->escribirLinea(cadenaNick, e.getFinalParam(1));
   }
   cliente.enviarComando(2, COMMAND_NOTICE, e.getParam(0), e.getFinalParam(1));
   return;
  }
  if ((strcasecmp(e.getCommand(), COMMAND_QUIT) == 0) || (strcasecmp(e.getCommand(), "DISCONNECT") == 0))
  {
   // lo intercepto y lo hago por medio de menuDesconectar
   menuDesconectar();
   return;
  }
  cliente.enviarLinea(mensaje+1);
 } else {
  // si vino desde screen no continuo m�s
  if (ventana == NULL) return;

  // gracias a servidores como channels.undernet.org no puedo hacer esto
  // cliente.enviarComando(2, COMMAND_PRIVMSG, ventana, mensaje);
  // sino esto
  char linea[strlen(COMMAND_PRIVMSG)+1+strlen(ventana)+2+strlen(mensaje)+1];
  sprintf(linea, "%s %s :%s", COMMAND_PRIVMSG, ventana, mensaje);
  cliente.enviarLinea(linea);

  VentanaChat *v = ventanas->punteroVentana(ventana);
  if (v != NULL) // nunca deber�a ser null, pero por si las dudas
  {
   sprintf(cadenaNick, "[%s]", (const char *)QIrc::registeredNick);
   v->escribirLinea(cadenaNick, mensaje);
  }
 }
}

void QIrc::error(const char *mensaje)
{
 screen->escribirLinea(PREFIJO_ERROR, mensaje, COLOR_ERROR);
}

// *********************************** SLOTS DE MANEJO DE MENUS
void QIrc::menuConectar()
{
 // me aseguro de que las reconecciones no den sorpresas
 debeEstarConectado = false;

 // ahora me fijo si estoy Conectado en el server
 if (conectado == true)
 { menuDesconectar(); } // me aseguro de desconectarme antes de empezar la nueva conexi�n
 // notese que us� conectado y no logged

 char host[strlen(config->readStrValue(CONFIG_HOSTPORDEFECTO, DEF_HOSTPORDEFECTO)) +1];
 char nick1[strlen(config->readStrValue(CONFIG_NICK1, DEF_NICK1))+1];
 char nick2[strlen(config->readStrValue(CONFIG_NICK2, DEF_NICK2))+1];
 char userInfo[strlen(config->readStrValue(CONFIG_USERINFO, DEF_USERINFO))+1];
 strcpy(host, config->readStrValue(CONFIG_HOSTPORDEFECTO, DEF_HOSTPORDEFECTO));
 strcpy(nick1, config->readStrValue(CONFIG_NICK1, DEF_NICK1));
 strcpy(nick2, config->readStrValue(CONFIG_NICK2, DEF_NICK2));
 strcpy(userInfo, config->readStrValue(CONFIG_USERINFO, DEF_USERINFO));
 int puerto	= config->readIntValue(CONFIG_PUERTOPORDEFECTO, DEF_PUERTOPORDEFECTO);
 char *pass = NULL, *user = NULL;

 if(config->readBoolValue(CONFIG_USARPASS, DEF_USARPASS) == true)
 {
  pass = new char[strlen(config->readStrValue(CONFIG_PASS, DEF_PASS)) + 1];
  strcpy(pass, config->readStrValue(CONFIG_PASS, DEF_PASS));
 } else {
  pass = NULL; // cuando pass value null, ircclient asume que no tiene que usar el pass
 }

 if(config->readBoolValue(CONFIG_AUTOUSER, DEF_AUTOUSER) == true)
 {
  user = new char[strlen(obtenerUser()) + 1];
  strcpy(user, obtenerUser());
 } else {
  user = new char[strlen(config->readStrValue(CONFIG_USER, DEF_USER)) + 1];
  strcpy(user, config->readStrValue(CONFIG_USER, DEF_USER));
 }

 char linea[strlen("Connecting to:") + 1 + strlen(host) + strlen(":") + 100]; // m�s de 100 no puede tener, no?
 sprintf(linea, "Connecting to: %s:%i",  host, puerto);
 screen->escribirLinea(PREFIJO_AVISOPROGRAMA, linea, COLOR_PROGRAMMESSAGE);

 // me aseguro de que haya un pass, porque IRCClient usa el valor de pass NULL para no usar pass
 if ((pass == NULL) || ((pass != NULL) && (strspn(pass, " ") == strlen(pass))))
 {
  cliente.conectar(host, puerto,  nick1, nick2, user, userInfo, NULL, 0);
 } else {
  cliente.conectar(host, puerto,  nick1, nick2, user, userInfo, pass, 0);
 }

 // borro los dos punteros que acabo de crear
 delete [] user;
 if (pass != NULL) delete [] pass;

 // me aseguro de que el men� away est� desmarcado
 comando->setItemChecked(ID_AWAY, false);

 // me fijo si muestro o no el primer motd
 noMotd = !config->readBoolValue(CONFIG_VERMOTD, DEF_VERMOTD);

 // para que no haya pausa antes de recibir el primer ctcp
 ctcpDelay = 0;

 // para que haga un ison apenas me loggee
 proximoIson = 0;

 // todav�a no se con que nick me voy a poder loggear
 QIrc::registeredNick = "";

 // no estoy recibiendo la lista de canales (esto es porque si se desconecta sin recibir el
 // final de la lista de canales al bajar la lista la primera vez no se limpia la lista anterior
 // y se mezclan las listas)
 recibiendoListaCanales = false;

 // reseteo la lista de notifies, para que que as� el programa los ponga todos offline
 visorNotifies->reset();
}

void QIrc::menuConectarCon()
{
 // si todav�a hay una ventana abierta
 if (conectarCon != NULL)
 {
  conectarCon->raise();
  return;
 }

 conectarCon  = new cuadroConectarCon(config);
 connect(conectarCon, SIGNAL(resultado(Server *)), SLOT(entradaConectarCon(Server *)));
}

void QIrc::entradaConectarCon(Server *s)
{
 // no borro el cuadro porque la se�al tiene que volver ah�
 // igualmente la pr�xima vez que sea necesario el widget va a ser borrado de memoria

 // si el bot�n fue cancelar (s == NULL)
 if (s != NULL)
 {
  // coloco el nuevo server elegido como server por defecto (para que lo use conectar)
  // comentario no me interesa
  config->writeStrValue(CONFIG_HOSTPORDEFECTO, s->host);
  config->writeIntValue(CONFIG_PUERTOPORDEFECTO, s->puerto);

  // por puro mani�tico que soy nom�s...
  delete [] s->host;
  delete [] s->comentario;

  // me conecto
  menuConectar();
 }
 delete conectarCon; // elimino la ventana
 conectarCon = NULL;
}

void QIrc::menuConfigurar()
{
 if (configuracion != NULL)
 {
  configuracion->raise();
  return;
 }

 configuracion = new cuadroConfiguracion();
 connect(configuracion, SIGNAL(borrarVentanaConf()), SLOT(borrarVentanaConfiguracion()));
}

void QIrc::borrarVentanaConfiguracion()
{
 if (configuracion != NULL)
 {
  delete configuracion;
  configuracion = NULL;
 }
}

void QIrc::menuSalir()
{
 // desconecto el server
 menuDesconectar();

 // escondo la ventana, para que no se vea como borro los widgets
 hide();

 // me aseguro de quitar todas las ventanas que est�n en memoria
 ventanas->vaciar();

 // si es necesario vac�o la lista de ignored nicks
 if (config->readBoolValue(CONFIG_VACIARIGNORELIST, DEF_VACIARIGNORELIST) == true)
  config->writeIntValue(CONFIG_IGNORELISTCOUNT, 0);

 // guardo el tama�o actual de la ventana
 config->writeIntValue(CONFIG_MAINWIDTH, width());
 config->writeIntValue(CONFIG_MAINHEIGHT, height());

 delete config;

  // salgo nom�s
 qApp->quit();
}

void QIrc::menuCambiarNick()
{
 if (cuadroEntrada != NULL)
 {
  delete cuadroEntrada;
 }

 cuadroEntrada = new inputBox("Write your new nickname:");
 connect(cuadroEntrada, SIGNAL(resultado(const char *)), SLOT(entradaCambiarNick(const char *)));
}

void QIrc::entradaCambiarNick(const char *entrada)
{
 // slot correspondiente al menu Unirse al Canal
 if (entrada != NULL)
 {
  cliente.enviarComando(1, COMMAND_NICK, entrada);

  delete cuadroEntrada; // elimino la ventana
  cuadroEntrada = NULL;
 }
}

void QIrc::menuUnirseCanal()
{
 if (cuadroEntrada != NULL)
 {
  delete cuadroEntrada;
 }

 cuadroEntrada = new inputBox("Which channel do you want to join?");
 connect(cuadroEntrada, SIGNAL(resultado(const char *)), SLOT(entradaUnirseCanal(const char *)));
}

void QIrc::entradaUnirseCanal(const char *entrada)
{
 // slot correspondiente al menu Unirse al Canal
 if (entrada != NULL)
 {
  cliente.enviarComando(1, COMMAND_JOIN, entrada);
 }

 // borro el cuadro (no antes, no podr�a usar la linea de entrada)
 delete cuadroEntrada;
 cuadroEntrada = NULL;
}

void QIrc::menuSalirCanal()
{
 if (cuadroEntrada != NULL)
 {
  delete cuadroEntrada;
 }

 cuadroEntrada = new inputBox("Which channel do you want to part?");
 connect(cuadroEntrada, SIGNAL(resultado(const char *)), SLOT(entradaSalirCanal(const char *)));
}

void QIrc::entradaSalirCanal(const char *entrada)
{
 // slot correspondiente al menu Salir del Canal
 if (entrada != NULL)
 {
  cliente.enviarComando(1, COMMAND_PART, entrada);
 }

 // borro el cuadro (no antes, no podr�a usar la linea de entrada)
 delete cuadroEntrada;
 cuadroEntrada = NULL;
}

void QIrc::menuQuienEs()
{
 if (cuadroEntrada != NULL)
 {
  delete cuadroEntrada;
 }

 cuadroEntrada = new inputBox("Write a nick");
 connect(cuadroEntrada, SIGNAL(resultado(const char *)), SLOT(entradaQuienEs(const char *)));
}

void QIrc::entradaQuienEs(const char *entrada)
{
 // slot correspondiente al menu Qui�n Es
 if (entrada != NULL)
 {
  cliente.enviarComando(1, COMMAND_WHOIS, entrada);
 }

 // borro el cuadro (no antes, no podr�a usar la linea de entrada)
 delete cuadroEntrada;
 cuadroEntrada = NULL;
}

void QIrc::menuAway()
{
 // si el men� away no est� marcado, lo marco
 if(comando->isItemChecked(ID_AWAY) == false)
 {
  cliente.enviarComando(1, COMMAND_AWAY, config->readStrValue(CONFIG_AWAYMESSAGE, DEF_AWAYMESSAGE));
  comando->setItemChecked(ID_AWAY, true);
 } else {
  cliente.enviarComando(0, COMMAND_AWAY);
  comando->setItemChecked(ID_AWAY, false);
 }
}

void QIrc::menuMotd()
{
 cliente.enviarComando(0, COMMAND_MOTD);
}

void QIrc::menuListarCanales()
{
 cliente.enviarComando(0, COMMAND_LIST);
}

void QIrc::menuDesconectar()
{
 // para saber que no tengo que reconectarme
 debeEstarConectado = false;

 // solo para ahorrar un poco de codigo
 const char *aux = config->readStrValue(CONFIG_QUITMESSAGE, DEF_QUITMESSAGE);
 if ((aux == NULL) || (strcmp(aux, "") == 0)) // nunca deber�a ser null, pero no pierdo nada en la comprobaci�n
 {
  cliente.desconectar();
 } else {
  cliente.desconectar(config->readStrValue(CONFIG_QUITMESSAGE, DEF_QUITMESSAGE));
 }
}

void QIrc::menuIgnorar()
{
 // primero me fijo que no haya ya una ventan creada de eso
 if(cuadroIgnores != NULL)
 {
  cuadroIgnores->raise();
  return;
 }

 cuadroIgnores = new cuadroListaNicks("Ignored Nicks", "Ignore those nicks", CONFIG_IGNORELISTCOUNT, CONFIG_IGNOREDNICK);
 connect(cuadroIgnores, SIGNAL(terminado()), SLOT(entradaIgnorar()));
}

void QIrc::menuNotificar()
{
 // primero me fijo que no haya ya una ventan creada de eso
 if(cuadroNotificar != NULL)
 {
  cuadroNotificar->raise();
  return;
 }

 cuadroNotificar = new cuadroListaNicks("Notify List", "Notify me when those nicks are in the server", CONFIG_NOTIFIESLISTCOUNT, CONFIG_NOTIFYNICK);
 connect(cuadroNotificar, SIGNAL(terminado()), SLOT(entradaNotificar()));
}

void QIrc::entradaIgnorar()
{
 // elimino la ventana
 delete cuadroIgnores;
 cuadroIgnores = NULL;
}

void QIrc::entradaNotificar()
{
 // elimino la ventana
 delete cuadroNotificar;
 cuadroNotificar = NULL;
}



void QIrc::menuDccSend()
{
 // creo una nueva ventana de dccsend
 DccSend *dccWindow;
 dccWindow = new DccSend();

 // la conecto para saber cuando se cierra y para que pueda mandar comandos
 connect(dccWindow, SIGNAL(dccSendCerrar(DccSend *)), SLOT(entradaDccSend(DccSend *)));
 connect(dccWindow, SIGNAL(lineaComando(const char *, const char *)), SLOT(nuevoMensajeVentana(const char *, const char *)));
}

void QIrc::entradaDccSend(DccSend *v)
{
 // solo cierro la ventana
 delete v;
}

void QIrc::entradaDccGet(DccGet *v)
{
 // borro la ventana, eso es todo
 delete v;

 // disminuyo el contador de ventanas abiertas
 if (cantidadVentanasDccAbiertas > 0) cantidadVentanasDccAbiertas--;
}

void QIrc::menuInstrucciones()
{
 // fuerzo un comando help
 nuevoMensajeVentana(NULL, "/help");
}

void QIrc::menuGracias()
{
 // muestra el about del programa
 QMessageBox::about(this, NULL, "I'm very thankful to those who have helped me.\n"
                                "Right now this isn't a big list, but I hope it'll\n"
                                "grow soon. Thanks again to you all!\n\n"
                                " - Keith Birdwell\n"
                                " - <insert funny name here>\n");
}


void QIrc::menuAbout()
{
 // muestra el about del programa
 QMessageBox::about(this, NULL, "QIrc for linux\n\n"
																"Author: Gerardo Puga [gere@mailroom.com]\n"
																"Started: September 13th, 2000\n"
															 	"Version: " VERSION "\n\n"
																"This program comes with absolutely NO WARRANTY OF ANY KIND\n\n"
																"Este programa NO TIENE GARANT�AS DE NING�N TIPO.\n\n"
																"Have Fun!!! ; )");
}

void QIrc::menuAboutQt()
{
 // muestra el about del qt
 QMessageBox::aboutQt(this);
}


// *********************************** SLOTS DE MANEJO DE SERVIDOR

void QIrc::conexionCompleta()
{
 screen->escribirLinea(PREFIJO_AVISOPROGRAMA, "connection complete!", COLOR_PROGRAMMESSAGE);
 screen->escribirLinea(PREFIJO_AVISOPROGRAMA, "Logging in...", COLOR_PROGRAMMESSAGE);

 // seteo la variable conectado a true para que despu�s el programa sepa si tiene que conectar
 conectado = true;

 // obtengo la direcci�n ip mediante la cual me conect� al server para usarla en los dccsend
 struct sockaddr_in localConnection;
 unsigned int len = sizeof(sockaddr_in);
 getsockname(cliente.getSocket(), (sockaddr*)&localConnection, &len);
 localIpAddress = localConnection.sin_addr;
}

void QIrc::estatusConexion(const char *mensaje)
{
 screen->escribirLinea(PREFIJO_MENSAJE_SERVIDOR, mensaje, COLOR_SERVERMESSAGE);
}

void QIrc::loginCompleto(const char *nick)
{
 // guardo el nick
 QIrc::registeredNick = nick;

 char linea[strlen("Login complete! Nick: ")+ 5 + strlen(nick) + 1]; // el +5 es para el codigo de color del mirc
 sprintf(linea, "Login complete! Nick: %c4%s", 3, nick);
 screen->escribirLinea(PREFIJO_MENSAJE_SERVIDOR, linea, COLOR_PROGRAMMESSAGE);

 // ac� van lo automatismos
 if (config->readBoolValue(CONFIG_LISTENCONECCION, DEF_LISTENCONECCION) == true)
 {
  cliente.enviarComando(0, COMMAND_LIST);
 }

 if((config->readBoolValue(CONFIG_INVISIBLE, DEF_INVISIBLE) || config->readBoolValue(CONFIG_WALLOPS, DEF_WALLOPS)) == true)
 {
  char modos[10];
  strcpy(modos, "+");
  if (config->readBoolValue(CONFIG_INVISIBLE, DEF_INVISIBLE) == true)
  {
   strcat(modos, "i");
  }
  if (config->readBoolValue(CONFIG_WALLOPS, DEF_WALLOPS) == true)
  {
   strcat(modos, "w");
  }
  cliente.enviarComando(2, COMMAND_MODE, (const char *)registeredNick, modos);
 }

 // la variable que uso para saber cuando la desconexi�n fue arbitraria
 debeEstarConectado = true;

 // para que el programa sepa que est� conectado
 logged = true;
}

void QIrc::desconexion()
{
 screen->escribirLinea(PREFIJO_AVISOPROGRAMA, "Disconnecting from the server", COLOR_PROGRAMMESSAGE);

 // deshabilito todas las ventanasde canales
 VentanaChat *v;
 int cantidad = ventanas->cantidadVentanas();
 for (int i = 0; i < cantidad; i++)
 {
  v = ventanas->punteroVentana(i);
  if (v->esTipoCanal() == true)
  {
   // te aviso que se desconect� el server
   v->escribirLinea(PREFIJO_AVISOPROGRAMA, "Disconnected!", COLOR_PROGRAMMESSAGE);
   v->deshabilitarVentana();
  }
 }

 // ya no estoy conectado (lo hago antes de intentar reconectar)
 conectado = false;
 logged = false;

 // me fijo si la desconexi�n fue incorrecta, si lo fue y a la reconexi�n est�
 // configurada como autom�tica reconecto
 if ((debeEstarConectado == true) && (config->readBoolValue(CONFIG_RECONNECT, DEF_RECONNECT) == true))
 {
  // reconecto de inmediato, simple, no?
  menuConectar();
 }
}

void QIrc::noReconocido(IRCEvent *e)
{
 // comando no reconocido!!!
 screen->escribirLinea(PREFIJO_ERROR, "Unknown command!");

 char linea[1000];
 snprintf(linea, 1000, "\tnick {%s} - user {%s} - host {%s}", (const char *)e->getNick(), (const char *)e->getUser(), (const char *)e->getHost());
 screen->escribirLinea(PREFIJO_ERROR, linea, COLOR_ERROR);

 sprintf(linea, "\t{%s} ", (const char *)e->getCommand());
 for (int i = 0; i < e->getParamCount(); i++)
 {
  strcat(linea, "{");
  strcat(linea, (const char *)e->getParam(i));
  strcat(linea, "} ");
 }
 screen->escribirLinea(PREFIJO_ERROR, linea, COLOR_ERROR);
}

void QIrc::replyCode(IRCEvent *evento)
{
 // distribuyo las respuestas del server seg�n a d�nde tengan que ir, y si no correponden a ning�n
 // espec�fico, las muestro por la ventan principal

 // esto lo voy a usar varias veces
 VentanaChat *v;
 char *linea;

 switch (evento->getNumeric())
 {
  case RPL_ISON:
            // irc.bariloche.com.ar y su implementaci�n del irc hace que esto sea necesario
            linea = new char[513];
            strcpy(linea, "");
            for (int i = 1; i < evento->getParamCount(); i++)
            { strcat(linea, " "); strcat(linea, (const char *)evento->getParam(i)); }
            visorNotifies->isonReply(linea);
            // si hubo alg�n cambio en la lista, la pongo en primer plano
            if (visorNotifies->notifiesChanged() == true)
            {
             mainWidget->raisePage(visorNotifies);
            }
            delete [] linea;
            break;
  case RPL_NAMREPLY: // linea de nicks para determinado canal
           v =  ventanas->punteroVentana((const char *)evento->getParam(2));
            // SOLO LE HAGO CASO SI la ventana existe, o de otra forma abrir�a una ventana por canal en
            // el server cuando alguien hiciera NAMES sin par�metro
           if ((v != NULL) && (v->listaNicksIngresada() == false)) {
             v->agregarListaNicks((const char *) evento->getParam(3));
           } else {
            linea = new char[strlen((const char *)evento->getParam(2)) + 3 + strlen((const char *)evento->getParam(3))+1];
            sprintf(linea, "%s - %s", (const char *)evento->getParam(2), (const char *)evento->getParam(3));
            screen->escribirLinea(PREFIJO_AVISOPROGRAMA, linea);
            delete [] linea;
           }
           break;
  case RPL_ENDOFNAMES:
           // notese que cuando se hace un comando names solo se devuelve un final de lista
           // comparando el valor con NULL se si la ventana existe, si no existe no es un canal v�lido
           v =  ventanas->punteroVentana((const char *)evento->getParam(1));
           if (v != NULL)  v->finListaNicks();
          break;
  case RPL_LIST:
          // compruebo si es la primera linea de la lista, si lo es vac�o la lista en pantalla
          if (recibiendoListaCanales == false)  {
            listaCanales->clear();
            recibiendoListaCanales = true;
           }
          listaCanales->insertChannel((const char *)evento->getParam(1), atoi((const char *) evento->getParam(2)), (const char *)evento->getParam(3));
          break;
  case RPL_LISTEND:
          // doy por terminada la lista de canales
          recibiendoListaCanales = false;
          // muestro la hoja de la lista de canales
          mainWidget->raisePage(listaCanales);
          break;
  case  ERR_CANNOTSENDTOCHAN:
	  	    // se trat� de mandar un mensaje a un canal en el que no se est� o no se tiene permiso de charla
		      v =  ventanas->punteroVentana((const char *)evento->getParam(1));
          if (v != NULL)  v->escribirLinea(PREFIJO_MENSAJE_SERVIDOR, "You can't talk in this channel!", COLOR_ERROR);
          break;
  case RPL_NOTOPIC:
  case RPL_TOPIC:
          // recibido al obtener el topic (o la inexistencia de topic) de un canal
          v =  ventanas->punteroVentana((const char *)evento->getParam(1));
          if (v != NULL)  v->escribirLinea(PREFIJO_TOPIC, (const char *)evento->getParam(2), COLOR_NICKSyMODESyTOPIC);
          break;
  case RPL_AWAY:
          // recibido al obtener el topic (o la inexistencia de topic) de un canal
          v = ventanas->punteroVentana((const char *)evento->getParam(1));
          if (v == NULL) v = ventanas->crearVentana((const char *)evento->getParam(1), false);
          v->escribirLinea(PREFIJO_AWAY, (const char *)evento->getParam(2), COLOR_AWAY);
          break;
  case RPL_MOTDSTART:
  case RPL_MOTD:
          if (noMotd == true) break;
          screen->escribirLinea(PREFIJO_MOTD, (const char *)evento->getParam(1));
          break;
  case ERR_NOMOTD:
  case RPL_ENDOFMOTD:	
          if (noMotd == false) screen->escribirLinea(PREFIJO_MOTD, (const char *)evento->getParam(1));
          noMotd = false; // as� si el usuario pide el motd lo puede ver
          break;
  default: char linea[513];
          strcpy(linea, ""); // para vaciar la linea
          for (int i = 1; i < evento->getParamCount(); i++)
          {
           strcat(linea, " ");
           strcat(linea, (const char *)evento->getParam(i));
          }
          screen->escribirLinea(PREFIJO_MENSAJE_SERVIDOR, linea);
          break;
 }
}

void QIrc::nick(const char *viejoNick, const char *nuevoNick)
{
 // cambia este nick por el nuevo en todas las ventanas
 // si el nick cambiado es el m�o, cambia el contenido de la variable nick en QIrc

 VentanaChat *v;

 for (int i = 0; i < ventanas->cantidadVentanas(); i++)
 {
  v = ventanas->punteroVentana(i);

  char linea[strlen(viejoNick) + 1 + strlen("is now known as") + 1 + strlen(nuevoNick) + 1];
  sprintf(linea, "%s is now known as %s", viejoNick, nuevoNick);

  if (v->existeNick(viejoNick) == true)
  {
   v->cambiarNick(viejoNick, nuevoNick);
   v->escribirLinea(PREFIJO_NICK, linea, COLOR_NICKSyMODESyTOPIC);
  }
 }

 if (strcasecmp((const char *)QIrc::registeredNick, viejoNick) == 0)
 {
  // es mi nick el que cambi�
  char linea[strlen("Your nick is now") + 1 + strlen(nuevoNick) +1];
  sprintf(linea, "Your nick is now %s", nuevoNick);
  screen->escribirLinea(PREFIJO_NICK, linea);

  // lo guardo para referencia futura
  QIrc::registeredNick = nuevoNick;
  // y tambi�n lo pongo en la configuracion
  config->writeStrValue(CONFIG_NICK1, nuevoNick);
 }
}

void QIrc::quit(const char *nick, const char *razon)
{
 // elimino el nick de todas las ventanas en las que est�
 VentanaChat *v;

 for (int i = 0; i < ventanas->cantidadVentanas(); i++)
 {
  v = ventanas->punteroVentana(i);
  if (v->existeNick(nick) == true)
  {
   char linea[strlen(nick) + 1 + strlen("has left the server:") + 1 + strlen(razon) +1];
   v->quitarNick(nick);

   sprintf(linea, "%s has left the server: %s", nick, razon);

   // borro los c�digos del mirc que puedan haber en el mensaje de despedida
   if (config->readBoolValue(CONFIG_BORRARMIRC, DEF_BORRARMIRC) == true)
   {
    filtrarMirc(linea);
   }

   v->escribirLinea(PREFIJO_QUIT, linea, COLOR_ENTRADASySALIDAS);
  }
 }
}

void QIrc::join(const char * nick, const char * canal)
{
 VentanaChat *v;
 char linea[strlen(nick) + 1 + strlen("has joined the channel") + 1];

 sprintf(linea, "%s has joined the channel", nick);

 if((v = ventanas->punteroVentana(canal)) != NULL)
 {
  // primero me fijo si est� deshabilitada
  // si lo est� la reseteo y me voy (ahora deber�a venir el codigo RPL_NAMES o como se llame)
  if (v->ventanaHabilitada() == false)
  {
   v->resetearVentana();
   v->escribirLinea(PREFIJO_ENTRAR_CANAL, "Rejoining channel", COLOR_PROGRAMMESSAGE);
  } else {
   v->agregarNick(nick);
   v->escribirLinea(PREFIJO_ENTRAR_CANAL, linea, COLOR_ENTRADASySALIDAS);
  }
 } else {
  // la ventana no existe, y eso s�lo se puede dar en dos casos:
  // 	- La ventana no fue creada todav�a y este join es el que vienen antes de la lista de
  //    nicks al unirse a un canal
  //  - La ventana fue cerrada y borrada, pero el server envi� mensajes que correspond�an a
  //    esa ventana antes de procesar el mensaje de PART de mi parte (problema de race)
  // El segundo caso debo ignorarlo para no mostrar comportamientos extra�os como el de
  // las 2ventanas fantasmas". Solo creo la ventana si el que se da es el primer caso.
  if (strcasecmp(nick, (const char*) QIrc::registeredNick) == 0)
  {
   // yo soy el que entr� a un nuevo canal
   ventanas->crearVentana(canal, true);
  }
 }
}

void QIrc::part(const char *nick, const char *canal, const char *mensaje)
{
 // elimina el nick del canal del que salio
 VentanaChat *v = ventanas->punteroVentana(canal);
 if (v != NULL) // puede ser que la ventana ya no exista (fue cerrada)
 {
  // si el que abandon� el canal fui yo, entonces deshabilito la ventana para que no
  // pueda seguir madando mensajes, y no muestro ning�n tipo de mensaje de part
  if (strcasecmp(nick, (const char *)QIrc::registeredNick) == 0)
  {
   // el que dej� el canal fui yo, as� que deshabilito la ventana
   v->deshabilitarVentana();
   v->escribirLinea(PREFIJO_PART, "You've left the channel", COLOR_ENTRADASySALIDAS);
   return;
  }

  int size = strlen(nick) + 1 + strlen("has left the channel") + 1;
  if (mensaje != NULL) size += 2 + strlen(mensaje); // "2" por los dos puntos y el espacio
  char linea[size];
  if (mensaje != NULL)
  {
   sprintf(linea, "%s has left the channel: %s", nick, mensaje);
  } else {
   sprintf(linea, "%s has left the channel", nick);
  }

  // borro los c�digos del mirc
  if (config->readBoolValue(CONFIG_BORRARMIRC, DEF_BORRARMIRC) == true)
  {
   filtrarMirc(linea);
  }

  v->quitarNick(nick);
  v->escribirLinea(PREFIJO_PART, linea, COLOR_ENTRADASySALIDAS);
 }
}

void QIrc::mode(IRCEvent *evento)
{
 // de todos los posibles modos el �nico que me interesa es el +o para mostrarlo en la lista
 // todos los dem�s son desacartados
 int parametros = 0; // los parametros requeridos
 VentanaChat *v = ventanas->punteroVentana((const char *)evento->getParam(0));

  if (v == NULL)
  {
   // asumo que no es un canal, por ej es un mode de server
   return;
  }

 for (int i = 1; i < evento->getParamCount(); i++)
 {
  parametros = 0;
  char cadena[strlen(evento->getParam(i)) + 1];
  strcpy(cadena, (const char *)evento->getParam(i));
  bool activar = true;

  for (unsigned int o = 0; o < strlen(cadena); o++)
  {
   switch (cadena[o]) {
     	case '+' : activar = true; break;
     	case '-' : activar = false; break;
     	case 'O' : parametros++; break;
    	case 'o' : parametros++;
              	 if (activar == true)
		    				 { v->setNickAsChanop((const char *)evento->getParam(i + parametros));
                 v->escribirLinea(PREFIJO_MODE, (const char *)QString("") + evento->getParam(i +parametros) + " has become a ChanOp", COLOR_NICKSyMODESyTOPIC); }
                 else { v->unsetNickAsChanop((const char *)evento->getParam(i + parametros));
                 v->escribirLinea(PREFIJO_MODE, (const char *)QString("") + evento->getParam(i +parametros) + " is not a ChanOp anymore", COLOR_NICKSyMODESyTOPIC);}
                 break;
  		case 'v' : parametros++;
                 if (activar == true) { v->escribirLinea(PREFIJO_MODE, (const char *)QString("") + evento->getParam(i +parametros) + " ahora puede hablar en el canal", COLOR_NICKSyMODESyTOPIC); }
                 else { v->escribirLinea(PREFIJO_MODE, (const char *)QString("") + evento->getParam(i +parametros) + " ya no puede hablar en el canal", COLOR_NICKSyMODESyTOPIC); }
						     break;
// todos sin parametro
     	case 'a' : if (activar == true) v->escribirLinea(PREFIJO_MODE, "The channel is now anonymous", COLOR_NICKSyMODESyTOPIC);
                 else v->escribirLinea(PREFIJO_MODE, "The channel is now not anonymous", COLOR_NICKSyMODESyTOPIC);
				      	 break;
  		case 'i' : if (activar == true) 	v->escribirLinea(PREFIJO_MODE, "This channel is now invite-only", COLOR_NICKSyMODESyTOPIC);
                 else v->escribirLinea(PREFIJO_MODE, "This channel is now public", COLOR_NICKSyMODESyTOPIC);
					       break;
	    case 'm' : if (activar == true)  v->escribirLinea(PREFIJO_MODE, "This channel is now moderated", COLOR_NICKSyMODESyTOPIC);
                 else v->escribirLinea(PREFIJO_MODE, "The channel is not moderated anymore", COLOR_NICKSyMODESyTOPIC);
					       break;
	   	case 'n' : break;
      case 'q' : break;
		  case 'p' : if (activar == true)  v->escribirLinea(PREFIJO_MODE, "This is now a private channel", COLOR_NICKSyMODESyTOPIC);
                 else v->escribirLinea(PREFIJO_MODE, "This channel is not private anymore", COLOR_NICKSyMODESyTOPIC);
					       break;
			case 's' : if (activar == true)  v->escribirLinea(PREFIJO_MODE, "The channel is now secret", COLOR_NICKSyMODESyTOPIC);
                 else	v->escribirLinea(PREFIJO_MODE, "This channel isn't secret anymore", COLOR_NICKSyMODESyTOPIC);
					       break;
			case 'r' : break;
			case 't' : break;
// todos con parametro
			case 'k' :
			case 'l' :
			case 'b' :
 			case 'e' :
			case 'I' : parametros++; break;
			default:
        #ifdef DEBUG_TELLME
          cout << "Se recibi� un modo desconocido del server! Modo: " << cadena[o] << endl;
        #endif
        break;
    }
   }

   // hago que no revise como modos a los parametros de los modos anteriores
   i += parametros;
  }
}

void QIrc::topic(const char *nick, const char *canal, const char *topic)
{
 VentanaChat *v;

  if ((v = ventanas->punteroVentana(canal)) != NULL)
  {
   if (strcmp(topic, "") == 0)
   {
     // alguien quit� el topic del canal
    char linea[strlen(nick) + 1 + strlen("has cleaned the channel topic")+1];

    sprintf(linea, "%s has cleaned the channel topic", nick);
    v->escribirLinea(PREFIJO_TOPIC, linea, COLOR_NICKSyMODESyTOPIC);
   } else {
    // alguien cambi� el topic del canal
    char linea[strlen(nick) + 1 + strlen("has changed the topic:") + 1 + strlen(topic) + 1];

    sprintf(linea, "%s has changed the topic: %s", nick , topic);

    v->escribirLinea(PREFIJO_TOPIC, linea, COLOR_NICKSyMODESyTOPIC);
  }
 }
}

void QIrc::invite(const char *nick, const char *canal)
{
 char linea[strlen(nick) + 1 + strlen("has invited you to join channel") + 1 + strlen(canal) + 1];

 sprintf(linea, "%s has invited you to join channel %s", nick, canal);
 screen->escribirLinea(PREFIJO_INVITE, linea, COLOR_INVITES);
}

void QIrc::kick(const char *nick, const char *canal, const char *echado, const char *razon)
{
 VentanaChat *v;

 if ((v = ventanas->punteroVentana(canal)) != NULL)
 {
  if (strcasecmp((const char *)QIrc::registeredNick, echado) == 0)
  {
    // me echaron del canal!
   char linea[strlen(nick) +1+ strlen("has kicked you from") +1+ strlen(canal) +2+ strlen(razon) + 1];
   sprintf(linea, "%s has kicked you from %s: %s", nick, canal, razon);
   v->escribirLinea(PREFIJO_KICK, linea, COLOR_ENTRADASySALIDAS);

   // deshabilito la ventana para que no pueda seguir escribiendo
   v->deshabilitarVentana();
  } else {
   // echaron a alguien m�s
   char linea[strlen(echado) +1+ strlen("has been kicked by") +1+ strlen(nick) +2+ strlen(razon) + 1];
   sprintf(linea, "%s has been kicked by %s: %s", echado, nick, razon);
   v->escribirLinea(PREFIJO_NICK, linea, COLOR_ENTRADASySALIDAS);
  }
  v->quitarNick(echado);
 }
}

void QIrc::privmsg(const char *nick, const char *destino, const char *mensaje)
{
 // si el mensaje es para m� mando el mensaje a una ventana privada que corresponde a
 // al nick de origen, sino al la ventana de canal indicada por destino

 // antes que nada me fijo si el nick no est� ignorado
 if (ignorado(nick) == true) return;

 char linea[strlen(mensaje)+1];
 strcpy(linea, mensaje);

 // idem con los c�digos del mIrc
 if (config->readBoolValue(CONFIG_BORRARMIRC, DEF_BORRARMIRC) == true)
 {
  filtrarMirc(linea);
 }

 // extraigo los c�digos ctcp (los query)
 extraerCtcpQuery(nick, destino, linea);

 // compruebo que la cadena no haya quedado vac�a, y si eso pasa no hago nada
 if (strspn(linea, " ") == strlen(linea)) return;

 // adorno un poco el nick
 char cadenaNick[strlen(nick) + 3];
 sprintf(cadenaNick, "[%s]", nick);

 if (strcasecmp((const char *)QIrc::registeredNick, destino) == 0)
 {
  // es para m�
  // si no existe la ventana la creo
  VentanaChat *v = ventanas->punteroVentana(nick);
  if (v == NULL) v = ventanas->crearVentana(nick, false);
  v->escribirLinea(cadenaNick, linea);
 } else {
  // es para un canal
  // como es para un canal, si la ventana todavia (o "ya") no existe, no la creo
  // y descarto el mensaje. Esto es para evitar el problema de las "ventanas fantasma"
  // que pasa al cerrar una ventana, si el server env�a un mensaje del canal entre
  // que yo cerr� la ventana y que recib� el part correspondiente crea una con s�lo
  // ese mensaje.
  VentanaChat *v = ventanas->punteroVentana(destino);
  if (v != NULL) v->escribirLinea(cadenaNick, linea);
 }
}

void QIrc::notice(const char *nick, const char *destino, const char *mensaje)
{
 // si el mensaje es para m� mando el mensaje a una ventana privada que corresponde a
 // al nick de origen, sino al la ventana de canal indicada por destino

 // antes que nada me fijo si el nick no est� ignorado
 if (ignorado(nick) == true) return;

 char linea[strlen(mensaje)+1];
 strcpy(linea, mensaje);

 // idem con los c�digos del mIrc
 if (config->readBoolValue(CONFIG_BORRARMIRC, DEF_BORRARMIRC) == true)
 {
  filtrarMirc(linea);
 }

 // extraigo los c�digos ctcp (los reply)
 extraerCtcpReply(nick, linea);

 // compruebo que la cadena no haya quedado vac�a, y si eso pasa no hago nada
 if (strspn(linea, " ") == strlen(linea)) return;

 // adorno un poco el nick
 char cadenaNick[strlen(nick) + 5];
 sprintf(cadenaNick, "+<%s>+", nick);

 // ELIMINADO: a partir de la versi�n 1.0.12 no se puede ignorar los notices por x segundos

 // al �ltimo canal
 if (strcasecmp((const char *)QIrc::registeredNick, destino) == 0)
 {
  // el mensaje fue dirigido a m�
  // muestro el notice en todas las ventanas (privados y canales) en los que est� el nick
  // que lo envi�. Si no hay ninguna ventana abierta con ese nick dentro, entonces
  // el mensaje no se descarta sino que se muestra en la ventana principal
  bool mostrado = false;
  VentanaChat *v;
  for (int i = 0; i < ventanas->cantidadVentanas(); i++)
  {
   v = ventanas->punteroVentana(i);
   if (v->existeNick(nick) == true)
   {
    v->escribirLinea(cadenaNick, linea, COLOR_NOTICE);
    mostrado = true;
   }
  }
  if (mostrado == false) screen->escribirLinea(cadenaNick, linea, COLOR_NOTICE);
 } else {
   // es para un canal
   // como es para un canal, si la ventana todavia (o "ya") no existe, no la creo
   // y descarto el mensaje. Esto es para evitar el problema de las "ventanas fantasma"
   // que pasa al cerrar una ventana, si el server env�a un mensaje del canal entre
   // que yo cerr� la ventana y que recib� el part correspondiente crea una con s�lo
   // ese mensaje.

   VentanaChat *v = ventanas->punteroVentana(destino);
   if (v != NULL)
   { v->escribirLinea(cadenaNick, linea, COLOR_NOTICE); }
   else { screen->escribirLinea(cadenaNick, linea, COLOR_NOTICE); }
 }
}

void QIrc::ping(const char *server)
{
 char linea[strlen("PING from server") +1 + strlen(server) + 1];

 sprintf(linea, "PING from server %s", server);
 screen->escribirLinea(PREFIJO_AVISOPROGRAMA, linea, COLOR_PROGRAMMESSAGE);
}
