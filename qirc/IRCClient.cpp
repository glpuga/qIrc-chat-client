/***************************************************************************
                          IRCClient.cpp  -  description
                             -------------------
    begin                : Sun Sep 17 2000
    copyright            : (C) 2000 by gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "IRCClient.h"

#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <errno.h>

// ********* Private section
void IRCClient::tratarLinea()
{
 #ifdef IRCCLIENT_DEBUG_MODE
  cout << "Message from the server: " << linea << endl;
 #endif

 // Creo el objeto evento con el que voy a trabajar
 IRCEvent evento(linea);

 // I emit the signal announcing that a new event has been received, so tha app will be able to control the
 // communication if it wants to.
 emit event(&evento);
 if (evento.isNumeric() == true)
 {
  if (evento.getNumeric() == RPL_WELCOME)
  {
   #ifdef IRCCLIENT_DEBUG_MODE
    cout << "Login Completo! Nick: " << (const char *) *nickInUse << endl;
   #endif

   // the logging in was succesfull
   logged = true;

    emit loginCompleto((const char *) *nickInUse); // so the calling program knows wich of the nicknames was accepted
  } else if ((evento.getNumeric() == ERR_NICKNAMEINUSE) && (logged == false)) // este reply tambi�n se utiliza al cambiar el nic estando conectado
  {
   // it is an error of some kind (it doesn't matter what  it is exactly) like nickInUse
   // I look to se if I have already tryed both the first and the second nickname
   if (testedNicks == 2)
   {
    #ifdef IRCCLIENT_DEBUG_MODE
     cout << "Los dos nicks est�n en uso, desconectando" << endl;
    #endif

    // I have tryed both nicknames, so the connection is aborted
    emit estatusConexion("Both nicknames are in use, closing connection.");
    desconectar();
   } else {
    #ifdef IRCCLIENT_DEBUG_MODE
     cout << "Probando el segundo nick" << endl;
    #endif
    emit estatusConexion("First nickname already in use, trying the second one...");
    // I have to try the second nick
    nickInUse = &nick2;

    enviarComando(1, COMMAND_NICK, (const char *)*nickInUse);
    testedNicks = 2;
   }
  } else if (evento.getNumeric() == ERR_PASSWDMISMATCH)
  {
   #ifdef IRCCLIENT_DEBUG_MODE
    cout << "the sent passwd is not valid" << endl;
   #endif

   // I have tryed both nicknames, so the connection is aborted
   emit estatusConexion("Password mismatch!!!");
   // no hay que forzar la desconexi�n, el mensaje ERROR es enviado por el server
  } else // cualquier otra cosa
  {
   emit replyCode(&evento);
  }
  return; // para que salga
 } // hasta ac� llega la comprobaci�n de si es num�rico

 // no es num�rico
 // remember that all commands are uppercased when received
 QString command = evento.getCommand();

 if (command == COMMAND_NICK)
 {
  emit nick((const char *)evento.getNick(), (const char *)evento.getParam(0));
  return;
 }
 if (command == COMMAND_QUIT)
 {
  emit quit((const char *)evento.getNick(), (const char *)evento.getParam(0));
  return;
 }
 if (command == COMMAND_JOIN)
 {
  emit join((const char *)evento.getNick(), (const char *)evento.getParam(0));
  return;
 }
 if (command == COMMAND_PART)
 {
  emit part((const char *)evento.getNick(), (const char *)evento.getParam(0), (const char *)evento.getParam(1));
  return;
 }
 if (command == COMMAND_MODE)
 {
  emit mode(&evento); // porque no se cuantos parametro pueda llegar a necesitar
  return;
 }
 if (command == COMMAND_TOPIC)
 {
  emit topic((const char *)evento.getNick(), (const char *)evento.getParam(0), (const char *)evento.getParam(1));
  return;
 }
 if (command == COMMAND_KICK)
 {
  emit kick((const char *)evento.getNick(), (const char *)evento.getParam(0), (const char *)evento.getParam(1), (const char *)evento.getParam(2));
  return;
 }
 if (command == COMMAND_INVITE)
 {
  emit invite((const char *)evento.getNick(), (const char *)evento.getParam(1));
  return;
 }
 if (command == COMMAND_PRIVMSG)
 {
  emit privmsg((const char *)evento.getNick(), (const char *)evento.getParam(0), (const char *)evento.getParam(1));
  return;
 }
 if (command == COMMAND_NOTICE)
 {
  emit notice((const char *)evento.getNick(), (const char *)evento.getParam(0), (const char *)evento.getParam(1));
  return;
 }
 if (command == COMMAND_PING)
 {
  // if autopong is set, then this object responds automaticaly
  if (automaticPong == true)
  {
   enviarComando(1, COMMAND_PONG, (const char *) *nickInUse);
  }
  emit ping((const char *)evento.getParam(0));
  return;
 }

 if (command == COMMAND_ERROR)
 {
  emit error((const char *)evento.getParam(0)); // Muestro el mensaje de error
  terminarConexion(); // desconecto el socket abruptamente
  return;
 }

 // si no sali� antes es porque es un comando no reconocido
 emit noReconocido(&evento);
}

// ********* Public Section
IRCClient::IRCClient(bool autoPong = true)
{
 #ifdef IRCCLIENT_DEBUG_MODE
  cout << "IRCClient::IRCClient(bool)" << endl;
 #endif

 // creo el objeto socket
 s = new IClientSocket(); // sin parametros, no hay conexi�n establecida

 // no estoy conectado
 conectado = false;
 logged = false;

 // linea est� vac�a
 linea[0] = 0;
 longitud = 0;

 // lectura y escritura son null ( as� puedo averiguar cuando tienen una direcci�n que tengo
 // que borrar antes de empezar una nueva conexi�n
 lectura = escritura = NULL;

 // pongo la respuesta autom�tica de pings
 automaticPong = autoPong;
}

IRCClient::~IRCClient()
{
 #ifdef IRCCLIENT_DEBUG_MODE
  cout << "IRCClient::~IRCClient()" << endl;
 #endif

 // desconecto el cliente
 desconectar();

 // borro el socket
 delete s;

 // no elimino los notificadores porque no se si no fueron eliminados por desconectar()
 // igualemente, QObject se encarga de eso
}

int IRCClient::getSocket()
{
 // solo me sirve para saber cual es el socket que estoy usando para conectarme al server
 // Lo uso para que los programas clientes de la clase puedan obtener informaci�n
 // de la conexi�n
 return s->getSocket();
}

bool IRCClient::setAutoPong(bool aP)
{
 #ifdef IRCCLIENT_DEBUG_MODE
  cout << "IRCClient::setAutoPong(bool)" << endl;
 #endif

 return (automaticPong = aP);
}

void IRCClient::socketActivoLectura(int socketHandler)
{
 int error = 0;
 bool moreThanOne = false;

 #ifdef IRCCLIENT_DEBUG_MODE
  cout << "Hay datos para lectura en el socket!" << endl;
 #endif

 // hubo actividad de lectura en el socket
 // leo de a un caracter hasta que no haya nada m�s que leer
 char c;

 while((conectado == true) && ((error = s->read(&c, 1)) > 0))
 {
  if ((longitud < 510) && (c != 10) && (c != 13))
  {
   linea[longitud] = c;
   linea[longitud+1] = 0;
   longitud++;
  }

  // me fijo si lleg� una linea completa (el buffer termina en crlf)
  if (((c == 10) || (c == 13)) && (longitud > 0))
  {
   #ifdef IRCCLIENT_DEBUG_MODE
    cout << "Se recibi� una linea completa, elev�ndola para an�lisis..." << endl;
   #endif

   // hago que se trate lo que haya llegado
   tratarLinea();

   // empiezo de cero
   linea[0] = 0;
   longitud = 0;
  }

  // para que sepa que hubo datos para recibir
  moreThanOne = true;
 }

 if ((error == 0) && (moreThanOne == false)) // si en esta llamda no hab�a datos para leer
 {
  #ifdef IRCCLIENT_DEBUG_MODE
   cout << "El socket se cerr� de forma imprevista! Terminando conexi�n" << endl;
  #endif

  // el socket se cerr�!!! cierro la conexi�n para no tener cuelgues por escribir en
  // socket cerrados
  terminarConexion();
 }

 if ((error < 0) && (conectado == true)) // solo pueden producirse errores v�lidos mientras est� conectado
 {
  #ifdef IRCCLIENT_DEBUG_MODE
   cout << "Error en la lectura del socket!!!!: " << s->strError() << endl;
  #endif

  // termino la conexi�n
  terminarConexion();
 }
}

void IRCClient::socketActivoEscritura(int socketHandler)
{
 #ifdef IRCCLIENT_DEBUG_MODE
  cout << "El socket est� listo para ser escrito..." << endl;
 #endif

 // este slot se activa solo una vez por conexi�n y es cuando la misma se completa
 // elimino el notificador
 escritura->setEnabled(false); // este notificador ya cumpli� su funci�n

 // me fijo en el resultado de la operaci�n
 int resultado = 0;
 socklen_t l = sizeof(resultado);
 getsockopt(s->getSocket(), SOL_SOCKET, SO_ERROR, &resultado, &l);
 // si la conexi�n se realiz� deber�a haber devuelto un cero, sino un codigo de error
 if (resultado != 0)
 {
  // hubo error
  #ifdef IRCCLIENT_DEBUG_MODE
   cout << "No se pudo completar la conexi�n no bloqueante!! Abortando" << endl;
  #endif

  emit estatusConexion("Connection failed...");

  terminarConexion();
 } else {
  #ifdef IRCCLIENT_DEBUG_MODE
   cout << "La conexi�n se llev� a cabo satisfactoriamente! Comenzando login..." << endl;
   cout << "\tNick del primer intento: " << (const char *)nick1 << endl;
  #endif
  emit estatusConexion("Successful connection!");
  // no hubo error y estamos conectados!
  conectado = true;

  // habilito la se�al de lectura
  lectura->setEnabled(true);

  emit conexionCompleta(); // emito una se�al para que el programa lo sepa

  // ahora mando los datos de pass, user y del nick (el primero de los dos)
  // posteriormente en tratarLinea() se fija si el primero no estaba en uso, y si se puede usar el segundo
  nickInUse = &nick1;

  // no estoy loggeado
  logged = false;

  emit estatusConexion("Now logging in...");
  if (pass.isNull() == false) enviarComando(1, COMMAND_PASS, (const char *)pass);
  enviarComando(4, COMMAND_USER, (const char*)user, "0", "0", (const char *)realName);
  enviarComando(1, COMMAND_NICK, (const char*)*nickInUse);

  testedNicks = 1; // esta variable lleva cuenta de cuantos nicks prob�
 }
}

void IRCClient::conectar(const char *server, int puerto, const char *nick1, const char *nick2, const char *user, const char *realName, const char *pass, int flags)
{
 #ifdef IRCCLIENT_DEBUG_MODE
  cout << "IRCClient::conectar(const char *, int, cont char *, const char *, const char *, const char * const char * int)" << endl;
  if (server == NULL) cout << " El par�metro server vale NULL!" << endl;
  if (nick1 == NULL) cout << " El par�metro nick1 vale NULL!" << endl;
  if (nick2 == NULL) cout << " El par�metro nick2 vale NULL!" << endl;
  if (user == NULL) cout << " El par�metro user vale NULL!" << endl;
  if (realName == NULL) cout << " El par�metro realaname vale NULL!" << endl;
 #endif

 // si todav�a estoy conectado, desconecto
 // para saber si estoy conectado (si empez� el proceso de conexi�n es m�s apropiado)
 // me fijo si (lectura == NULL). GUESS WHY? RIGHT!
 if (lectura != NULL) // los dos (lectura y escritura) van a la par y se usan para saber cuando estoy realmente conectado
 {
  if (conectado == true)
  {  desconectar();
  } else {
     terminarConexion();
  }
 }

 // inicializo variables
 conectado = false;

 IRCClient::nick1 = nick1;
 IRCClient::nick2 = nick2;
 IRCClient::user = user;
 IRCClient::pass = pass;
 IRCClient::realName = realName;

 // le digo al socket que comienze la conexi�n
 #ifdef IRCCLIENT_DEBUG_MODE
  cout << "Creando el socket y conectando: " << endl;
  cout << "\tServer: " << server << endl;
  cout << "\tPuerto: " << puerto << endl;
 #endif

 if (s->connect(server, puerto, false) < 0) // conexi�n con el server NO BLOQUEANTE, reviso que no haya habido error
 {
  emit estatusConexion("Can't resolve the server name. Connection aborted.");
  return;
 }

 // como es no bloqueante tengo que esperar hasta que la conexi�n se haya completado
 // para eso espero a que se pueda escribir en el socket
 // creo el QSocketNotifier de escritura
 escritura = new QSocketNotifier(s->getSocket(), QSocketNotifier::Write, this);
 escritura->setEnabled(true); // solo para asegurarme
 // conecto el evento activated()... (el connect es el de qObject)
 connect(escritura, SIGNAL(activated(int)), this, SLOT(socketActivoEscritura(int)));

 // hago lo mismo con el de lectura, pero ese lo dejo deshabilitado
 lectura = new QSocketNotifier(s->getSocket(), QSocketNotifier::Read, this);
 lectura->setEnabled(false);
 // conecto el evento activated()... (el connect es el de qObject)
 connect(lectura, SIGNAL(activated(int)), this, SLOT(socketActivoLectura(int)));

 // y me siento a esperar... (ver socketActivoEscritura(int))
 emit estatusConexion("Waiting to complete connection");
}

void IRCClient::desconectar(const char *razon = 0)
{
 #ifdef IRCCLIENT_DEBUG_MODE
  cout << "Desconectando del server" << endl;
 #endif

 // vacio la linea de cualquier comando pendiente
 linea[0] = 0;
 longitud = 0;

 // solo puedo desconectar si en alg�n punto se inici� la conexi�n
 // para saber eso me fijo si lectura (o escritura, es lo mismo)
 // es diferente de NULL
 if (lectura == NULL)
  return; // no estoy conectado

 // primero que nada envio por el socket un quit (�nicamente si se alcanz� a establecer la conexi�n)
 if (conectado == true)
 {
  if (razon == 0)
  {
   // no se dio una raz�n
   enviarComando(0, COMMAND_QUIT);
  }else{
   // se dio una raz�n
   enviarComando(1, COMMAND_QUIT, razon);
  }
 }

 // *** ***
 // es necesario que cierre el socket sin esperar la respuesta de ERROR a causa de
 // servers como chat.clarin.com.ar que no cumplen los RFC al cerrar el socket despu�s
 // de un quit SIN ENVIAR EL ERROR

 // cierra el socket, y elimina los dos notificadores de eventos en el socket
 delete lectura;
 delete escritura;
 lectura = escritura = NULL;
 // notese que al eliminar un objeto se eliminan todas las conexiones de sus se�ales con cualquier
 // slot y viceversa

 s->close();
 conectado = logged = false;

 // por �ltimo emito la se�al indicando la desconexi�n
 emit desconexion();
}

void IRCClient::terminarConexion()
{
 // versi�n abrupta de desconectar(). Se usa cuando el server desconecta su lado del socket

 #ifdef IRCCLIENT_DEBUG_MODE
  cout << "Desconectando abruptamente del server" << endl;
 #endif

 // solo puedo desconectar si en alg�n punto se inici� la conexi�n
 // para saber eso me fijo si lectura (o escritura, es lo mismo)
 // es diferente de NULL
 if (lectura == NULL)
  return; // no estoy conectado

 // cierra el socket, y elimina los dos notificadores de enventos en el socket
 delete lectura;
 delete escritura;
 lectura = escritura = NULL;
 // notese que al eliminar un objeto se eliminan todas las conexiones de sus se�ales con cualquier
 // slot y viceversa

 s->close();
 conectado = logged = false;

 // por �ltimo emito la se�al indicando la desconexi�n
 emit desconexion();
}


void IRCClient::enviarLinea(const char *linea)
{
 #ifdef IRCCLIENT_DEBUG_MODE
  cout << "Enviando mensaje al server: " << linea << endl;
 #endif

 // first, I send the string
 s->write(linea, strlen(linea));
 // and now I send the CRLF pair
 s->write(CRLF, strlen(CRLF));
}

void IRCClient::enviarComando(int cantidad, const char *comando, ...)
{
 char linea[513];
 char *parametro;
 va_list lp;

 // inicializo la linea
 strncpy(linea, comando, 512);
 linea[512] = 0; // para que si el comando es muy largo quede una cadena terminada en cero

 // inicizalizo la tabla de parametros
 va_start(lp, comando);
 // comienzo
 for (int i = 0; i < cantidad; i++)
 {
  // me fijo de no mandar m�s de 15 par�metros
  if (i == 15) break;
  parametro = va_arg(lp, char *);
  // es este el parametro final? si es as� lo hago trailing
  if (i == cantidad - 1)
  {
   // me fijo de no superar el tama�o de la cadena
   if (strlen(linea) + strlen(parametro) + 2 > 512) break;
   // agrego la cadena
   // esto es un if que tengo que hacer gracias a servers como channels.undernet.org
   if (strchr(parametro, ' ') != NULL)
   { strcat(linea, " :");
   } else {
     strcat(linea, " "); // no son necesarios
   }
   strcat(linea, parametro);
  } else {
   // me fijo de no superar el tama�o de la cadena
   if (strlen(linea) + strlen(parametro) + 1 > 512) break;
   // agrego la cadena
   strcat(linea, " ");
   strcat(linea, parametro);
  }
 }
 va_end(lp);

 // finalmente envio la linea
 enviarLinea(linea);
}


// ************************** IRCEvent **********************

IRCEvent::IRCEvent(const char *serverMessage)
{
 char prefix[MAX_LINE_LEN];
 int index = 0;

 // inicializo los datos
 nick = "";
 user = "";
 host = "";
 for (int i = 0; i < 15; i++)
 {
  param[i] = "";
 }

 // empiezo por sacar el prefijo (si existe)
 if (serverMessage[0] == CHR_COLON)
 {
  // tiene prefijo
  index++; // to skip the colon
  strcpy(prefix,(const char *)getStringToken(serverMessage, index));

  // Ahora todav�a tengo que desmenuzar el prefijo en sus partes
  // prefix = servername / (nickname [[ "!" user ] "@" host ]
  // para eso lo puedo dividir en etapas
  // 0 - nickname
  // 1 - user
  // 2 - host
  // notese que no diferencio entre el servername y el nick, pero tambi�n n�tese
  // que eso no es importante

  int etapa = 0;
  for (unsigned int i = 0; i < strlen(prefix); i++)
  {
   switch (prefix[i]) {
    case '!' : etapa = 1; break;
    case '@' : etapa = 2; break;
    default:
     switch (etapa) {
      case 0  : nick += prefix[i]; break;
      case 1  : user += prefix[i]; break;
      default : host += prefix[i]; break;
     }
   }
  }
 }

 // ahora saco el comando o codigo de tres d�gitos
 command = getStringToken(serverMessage, index);
 command = command.upper(); // lo paso a may�sculas

 // y empiezo a sacar los parametros
 // para eso antes de sacar cada uno por separado me fijo si no empiezan por ":"
 // o si son el 15avo parametro. En estos caso considero todo lo que reste del
 // mensaje como un solo parametro

 // pongo a cero la cuenta
 paramCount = 0;
 while (serverMessage[index] != 0)
 {
  if ((serverMessage[index] == CHR_COLON) || (paramCount == 14))
  {
   // si es el comienzo de un trailing or if this is the 15th parameter

   if (serverMessage[index] == CHR_COLON) // I have to increase the index count by one to skip the colon
   {
    index++;
   }

   param[paramCount] = (const char *)(serverMessage+index);
   index += strlen(serverMessage+index); // para que apunte al cero
  } else {
   // it isnt a trailing, so I only have to read one token
   param[paramCount] = getStringToken(serverMessage, index);
  }

  // in both cases, I have to increase the paramCount count by one
  paramCount++;
 }

 // now I have to set the values for the numeric and codeNumber variables
 if (strlen((const char *)command) == strspn((const char *)command, DIGITS))
 { // I know that it isn't a number
  numeric = true;
  codeNumber = atoi((const char *)command);
 } else {
  numeric = false;
 }

 // i'm done, I guess :)
}

int IRCEvent::getNumeric()
{
 if (!numeric) // si no es numerica
 {
  return -1;
 } else {
  return codeNumber;
 }
}

QString IRCEvent::getParam(int index)
{
 if ((index < 0) || (index > 14)) // fuera de rango
 {
  // no es un �ndice v�lido
  return QString();
 } else {
  return param[index];
 }
}

// *** protected methods

int IRCEvent::skipSpaces(const char *linea, int &index)
{
 // no modifica la cadena, pero incrementa el indice
 while (linea[index] == CHR_SPACE)
 {
  index++;
 }
 return index;
}

QString IRCEvent::getStringToken(const char *linea, int &index)
{
 QString token(""); // inicializo el token a cadena vac�a

 // empiezo por saltear todos los posibles espacios que puedieran haber (NO deber�a haber niguno)
 skipSpaces(linea, index);

 //ahora copio todos los caracteres hasta el pr�ximo espacio o cero terminador de cadena
 while ((linea[index] != CHR_SPACE) && (linea[index] != CHR_ZERO))
 {
  token += linea[index];
  index++;
 }

 // ahora salteo los espacios que est�n al final
 skipSpaces(linea, index);

 // regreso el resultado
 return token;
}

QString IRCEvent::getStringToken(QString linea, int &index)
{
 return getStringToken((const char*)linea, index);
}