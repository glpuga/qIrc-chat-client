/***************************************************************************
                          dccget.cpp  -  description
                             -------------------
    begin                : Fri Nov 17 2000
    copyright            : (C) 2000 by Gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "dccget.h"

// esto est� definido en qirc.cpp
extern Config *config;

// la ip local definida en qirc solo la uso para mostrarla en pantalla
extern struct in_addr localIpAddress;

DccGet::DccGet(const char *nick, const char *fileName, in_addr ip, unsigned int port, unsigned long fileSize)
{
 timeout 				= 0;
 CONNECTTIMEOUT = config->readIntValue(CONFIG_DCCCONNECTTIMEOUT, DEF_DCCTRANSFERTIMEOUT);
 TRANSFERTIMEOUT= config->readIntValue(CONFIG_DCCTRANSFERTIMEOUT, DEF_DCCTRANSFERTIMEOUT);
 fileHandler    = -1;
 recibido 			= 0;
 socket					= NULL;
 notificaConexion = notificaDatos = NULL;
 blockSize 			= config->readIntValue(CONFIG_DCCBLOCKSIZE, DEF_DCCBLOCKSIZE);
 buffer 				= new char[blockSize];

 setFixedSize(DCCGET_ANCHO, DCCGET_ALTO);
 setCaption(DCCGET_WINDOW_TITLE);

 lNick = new QLabel("From:", this);
 lNick->setAlignment(AlignRight | AlignVCenter);
 lNick->setGeometry(MARCO, MARCO, DCCGET_ANCHO_LABEL, ALTO_LABEL);
 eNick = new QLabel(this);
 if (nick != 0) eNick->setText(nick);
 eNick->setGeometry(MARCO + DCCGET_ANCHO_LABEL + DISTANCIA, MARCO, width() - 2*MARCO - DCCGET_ANCHO_LABEL - DISTANCIA, ALTO_LABEL);

 lArchivo = new QLabel("File:", this);
 lArchivo->setAlignment(AlignRight | AlignVCenter);
 lArchivo->setGeometry(MARCO, MARCO + ALTO_LABEL + DISTANCIA, DCCGET_ANCHO_LABEL, ALTO_LABEL);
 eArchivo = new QLineEdit(this);
 if (fileName != 0)
 {
  char nuevoNombre[strlen(nick) + 1 + strlen(fileName) + 1];
  strcpy(nuevoNombre, nick);
  strcat(nuevoNombre, ".");
  if (strrchr(fileName, '/') != NULL)
  {
   strcat(nuevoNombre, strrchr(fileName, '/')+1);
  } else {
   strcat(nuevoNombre, fileName);
  }
  eArchivo->setText(nuevoNombre);
 }
 eArchivo->setGeometry(MARCO + DCCGET_ANCHO_LABEL + DISTANCIA, MARCO + ALTO_LABEL + DISTANCIA, width() - 2*MARCO - DCCGET_ANCHO_LABEL - DISTANCIA, ALTO_LABEL);

 lIpRemota = new QLabel("Remote Ip:", this);
 lIpRemota->setAlignment(AlignRight | AlignVCenter);
 lIpRemota->setGeometry(MARCO, MARCO + ALTO_LABEL*2 + DISTANCIA * 3, DCCGET_ANCHO_LABEL, ALTO_LABEL);
 ipRemota = new QLabel("--", this);
 ipRemota->setAlignment(AlignVCenter);
 ipRemota->setGeometry(MARCO + DCCGET_ANCHO_LABEL + DISTANCIA, MARCO + ALTO_LABEL*2 + DISTANCIA*3, DCCGET_ANCHO_LAB_IP, ALTO_LABEL);

 lIpLocal = new QLabel("Local Ip:", this);
 lIpLocal->setAlignment(AlignRight | AlignVCenter);
 lIpLocal->setGeometry(width() - MARCO - DCCGET_ANCHO_LABEL - DCCGET_ANCHO_LAB_IP - DISTANCIA, MARCO + ALTO_LABEL*2 + DISTANCIA*3, DCCGET_ANCHO_LABEL, ALTO_LABEL);
 ipLocal = new QLabel("--", this);
 ipLocal->setAlignment(AlignVCenter);
 ipLocal->setGeometry(width() - MARCO - DCCGET_ANCHO_LAB_IP, MARCO + ALTO_LABEL*2 + DISTANCIA*3, DCCGET_ANCHO_LAB_IP, ALTO_LABEL);

 lStatus = new QLabel(this);
 lStatus->setAlignment(AlignCenter);
 lStatus->setGeometry(MARCO, MARCO + ALTO_LABEL*3 + DISTANCIA*4, DCCGET_ANCHO - MARCO, ALTO_LABEL);

 barra = new QProgressBar(100, this); // una escala de 0 a 100
 barra->setGeometry(MARCO, DCCGET_ALTO - MARCO - ALTO_BARRA_PROGRESO - DISTANCIA - ALTO_BOTON, width() - 2*MARCO,  ALTO_BARRA_PROGRESO);

 aceptar = new QPushButton("Accept", this);
 aceptar->setGeometry(MARCO, DCCGET_ALTO - MARCO - ALTO_BOTON, (int)((DCCGET_ANCHO-MARCO) / 2), ALTO_BOTON);
 cancelar  = new QPushButton("Cancel", this);
 cancelar->setGeometry((int)((DCCGET_ANCHO - MARCO) / 2), DCCGET_ALTO - MARCO - ALTO_BOTON, (int)((DCCGET_ANCHO-MARCO) / 2), ALTO_BOTON);
 connect(aceptar, SIGNAL(clicked()), SLOT(clickAceptar()));
 connect(cancelar, SIGNAL(clicked()), SLOT(clickCancelar()));

 // guardo los valores de direcci�n
 DccGet::ipOrigen = ip;
 DccGet::puerto   = port;
 DccGet::fileSize = fileSize;

 // muestro la direcci�n de origen y la direcci�n local
 ipRemota->setText(inet_ntoa(ip));
 ipLocal->setText(inet_ntoa(localIpAddress)); // variable externa declarada en qirc

 // muestro el tama�o del archivo
 QString tamanio;
 tamanio.setNum(fileSize);
 tamanio = tamanio + " bytes file";
 lStatus->setText((const char *)tamanio);

 // arranco el timer para verificar los timeouts
 startTimer(1000); // una vez por segundo

 show();
}

void DccGet::timerEvent(QTimerEvent *e)
{
 if ((notificaConexion != NULL) && (timeout == 0))
 {
  // est� esperando a que termine la conexi�n pero se venci� el plazo
  // cierro el socket y termino el notificador
  socket->close();
  delete socket; socket = NULL;
  delete notificaConexion; notificaConexion = NULL;
  ::close(fileHandler); fileHandler = -1;
  #ifdef DEBUG_DCCGET
   cout << "Se venci� el tiempo de espera en la conexi�n" << endl;
  #endif
  QMessageBox::warning(NULL, "Connection error", "Connection timed out, transfer aborted", "Exit");
  lStatus->setText("Connection timed out, transfer aborted");
 }
 if ((notificaDatos != NULL) && (timeout == 0))
 {
  // est� esperando a que termine la conexi�n pero se venci� el plazo
  // cierro el socket y termino el notificador
  socket->close();
  delete socket; socket = NULL;
  delete notificaDatos; notificaDatos = NULL;
  ::close(fileHandler); fileHandler = -1;
  #ifdef DEBUG_DCCGET
   cout << "Se venci� el tiempo de espera en la recepci�n de datos" << endl;
  #endif
  QMessageBox::warning(NULL, "Connection error", "Transfer timed out, aborting", "Exit");
  lStatus->setText("Transfer timed out, aborted");
 }
 if (timeout > 0) timeout--;
}

DccGet::~DccGet()
{
 delete [] buffer;
}

void DccGet::clickAceptar()
{
 // valido los datos
 if ((eArchivo->text() == NULL) || (strlen(eArchivo->text()) == 0))
 {
  QMessageBox::warning(NULL, "Invalid Data", "You have to specify a file name", "Retry");
  return;
 }
 if (eArchivo->text()[0] == ' ') // no puede tener espacios al comienzo
 {
  QMessageBox::warning(NULL, "Invalid Data", "The file name can't have spaces at the beggining", "Retry");
  return;
 }
 if (eArchivo->text()[strlen(eArchivo->text())-1] == ' ') // no puede tener espacios al final
 {
  QMessageBox::warning(NULL, "Invalid Data", "The file name can't have spaces at the end", "Retry");
  return;
 }
 // me aseguro de poder crear el archivo y de que no exista ya otro con el mismo nombre
 // si el nombre del archivo empieza por / lo uso tal como est�. Sino le agrego el $HOME
 // del usuario al principio
 char pathCompleto[strlen(eArchivo->text()) + 2 + strlen(obtenerHomePath()) + 1]; // el largo que tiene en el peor de los casos
 if (eArchivo->text()[0] == '/')
 {
  strcpy(pathCompleto, eArchivo->text());
 } else {
  strcpy(pathCompleto, obtenerHomePath());
  // solo por si las dudas, si el path termina en / no se la agrego de vuelta
  if(pathCompleto[strlen(pathCompleto)-1] != '/') strcat(pathCompleto, "/");
  strcat(pathCompleto, eArchivo->text());
 }
 if ((fileHandler = open(pathCompleto, O_WRONLY | O_CREAT | O_EXCL, S_IRUSR | S_IWUSR)) < 0)
 {
  // hubo un error al abrir el archivo
  // si fue que el archivo ya existe entonces le pregunto al user si quiere sobreescribirlo
  if (errno == EEXIST)
  {
   int res = -1;
   if ((res = QMessageBox::warning(NULL, "File Error", "Selected file already exists in your hard disck, overwrite?", "Yes, overwrite", "No, I want to change the filename", NULL, 0, 1)) == 1)
   {
    // no quiere sobreescribir el archivo
    return;
   } else {
    // lo quiere sobreescribir, lo abro de vuelta con banderas diferentes
    fileHandler = open(pathCompleto, O_WRONLY | O_TRUNC, S_IRUSR | S_IWUSR); // si existe lo trunco a 0 bytes
   }
  }
  if (fileHandler < 0) // pregunto de vuelta porque puedo haber tenido que truncar el archivo
  {
   QString mensaje = "";
   switch(errno)
   {
    case EACCES : mensaje = "You don't have the permissions to create/open the file!";
	 							 break;
    default     : mensaje = "Unknown file error: ";
                  mensaje = mensaje + strerror(errno);
                  break;
   }
   QMessageBox::warning(NULL, "File Error", (const char *)mensaje, "Retry");
   return;
  }
 }
 // el archivo ya est� abierto, ahora trato de abrir el socket pero antes deshabilito
 // el bot�n aceptar
 aceptar->setEnabled(false);

 // ahora si el socket
 socket = new IClientSocket();
 if (socket->connect(ipOrigen, puerto, false) < 0) // no bloqueante
 {
  #ifdef DEBUG_DCCGET
   cout << "No se pudo comenzar la conexi�n" << endl;
  #endif
  delete socket; socket = NULL; // la pongo a null para que cancelar no la trate de borrar de memoria de vuelta
  ::close(fileHandler); fileHandler = -1;
  QMessageBox::warning(NULL, "Connection error", socket->strError(), "Exit");
  lStatus->setText("Error when starting connection");
  return;
 }
 // creo el notificador de conexi�n
 notificaConexion = new QSocketNotifier(socket->getSocket(), QSocketNotifier::Write, this);
 notificaConexion->setEnabled(true);
 connect(notificaConexion, SIGNAL(activated(int)), SLOT(conexionCompleta(int)));

  #ifdef DEBUG_DCCGET
   cout << "Conectando con el origen" << endl;
  #endif
  lStatus->setText("Connecting...");

 // doy inicio al temporizador del timeout
 timeout = CONNECTTIMEOUT;
}

void DccGet::clickCancelar()
{
 if (socket != NULL)
 {
  socket->close();
  delete socket; socket = NULL;
 }
 if (fileHandler >= 0)
 {
  ::close(fileHandler);
 }

 emit cerrarDccGet(this);
}

void DccGet::conexionCompleta(int)
{
 // esto lo tengo que hacer ya sea que la conexi�n se complete o no
 delete notificaConexion; notificaConexion = NULL;

 // se complet� la conexi�n en el socket, me fijo si esta fue exitosa o un total fracaso
 int resultado = 0;
 socklen_t l = sizeof(resultado);
 getsockopt(socket->getSocket(), SOL_SOCKET, SO_ERROR, &resultado, &l);

 if (resultado != 0)
 {
  QString mensaje;
  switch (resultado)
  {
   case ECONNREFUSED : mensaje = "Connection refused"; break;
   case ETIMEDOUT    : mensaje = "Connection timed out"; break;
   case ENETUNREACH  : mensaje = "Host unreachable"; break;
   default           : mensaje = "Unknown error connection error: ";
                       mensaje = mensaje + strerror(resultado);
                       break;
  }
  #ifdef DEBUG_DCCGET
   cout << "No se pudo completar la conexi�n:" <<  (const char *)mensaje << endl;
  #endif
  socket->close();
  delete socket; socket = NULL;
  ::close(fileHandler); fileHandler = -1;
  QMessageBox::warning(NULL, "Connection error", (const char *)mensaje, "Exit");
  lStatus->setText("Can't connect to the sender, connection aborted");
  return;
 }
 // la conexi�n fue exitosa
 notificaDatos = new QSocketNotifier(socket->getSocket(), QSocketNotifier::Read, this);
 notificaDatos->setEnabled(true);
 connect(notificaDatos, SIGNAL(activated(int)), SLOT(dataRecv(int)));
 lStatus->setText("Downloading file...");

 #ifdef DEBUG_DCCGET
  cout << "Conexi�n completa" << endl;
 #endif

 // inicio el temporizador de la llegada de datos
 timeout = TRANSFERTIMEOUT;
}

void DccGet::dataRecv(int)
{
 // reinicio el temporizador de la llegada de datos
 timeout = TRANSFERTIMEOUT;

 // recibo datos del socket y los almaceno en el disco
 long cBytes;
 if ((cBytes = socket->read(buffer, blockSize)) <= 0)
 {
  #ifdef DEBUG_DCCGET
   if (cBytes == 0) {
    cout << "Se perdi� la conexi�n, transferencia abortada" << endl;
   } else {
    cout << "Error en la conexi�n: " << socket->strError() << endl;
   }
  #endif
  socket->close();
  delete socket; socket = NULL;
  delete notificaDatos; notificaDatos = NULL;
  ::close(fileHandler); fileHandler = -1;
  // nunca mostrar un QMessageBox antes de haber cerrado todo (el warning es una ventana modal y sigue escuchando
  // el socket!!!!)
  QMessageBox::warning(NULL, "Connection error", "The sender has closed the connection, transfer interrupted", "Exit");
  lStatus->setText("The sender has closed the connection, transfer aborted");
  return;
 }
 recibido += cBytes;

 int ret;
 if ((ret = write(fileHandler, buffer, cBytes)) < 0)
 {
  // se produjo un error al escribir en el archivo
  delete socket; socket = NULL;
  delete notificaDatos; notificaDatos = NULL;
  ::close(fileHandler); fileHandler = -1;

  QString mensaje;
  switch (errno)
  {
   case EINTR  : mensaje = "Disk operation signaled!!! this is a big bug!"; break;
   case ENOSPC : mensaje = "Not enough disk space"; break;
   default     : mensaje = "Unknown error while saving the file: ";
                 mensaje = mensaje + strerror(errno);
                 break;
  }
  QMessageBox::warning(NULL, "File error", (const char *)mensaje, "Exit");
  lStatus->setText((const char *)mensaje);
  socket->close();
  return;
 }

 // envio la confirmaci�n de lectura
 U_4BYTES_INT conf = (U_4BYTES_INT)htonl(recibido);
 socket->write((const char *)&conf, 4);

 #ifdef DEBUG_DCCGET
  cout << "confirmando recepci�n de " << recibido << " bytes" <<  endl;
 #endif

 //actualizo la barra
 barra->setProgress((int)(recibido * 100 / fileSize));

 // si ya baj� todo el archivo cierro el socket
 if (recibido == fileSize)
 {
  socket->close();
  delete socket; socket = NULL;
  delete notificaDatos; notificaDatos = NULL;
  ::close(fileHandler); fileHandler = -1;

  #ifdef DEBUG_DCCGET
   cout << "Recepci�n finalizada" << endl;
  #endif

  // emito la se�al de destrucci�n de la ventana
  emit cerrarDccGet(this);
  return;
 }
}

