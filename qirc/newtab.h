/***************************************************************************
                          newtab.h  -  description
                             -------------------
    begin                : Sat Dec 16 2000
    copyright            : (C) 2000 by Gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef NEWTAB_H
#define NEWTAB_H

#include <qwidget.h>
#include <qpainter.h>
#include <qpixmap.h>
#include <qfont.h>
#include <qcolor.h>
#include <string.h>

#include "resource.h"

struct nodoListaTabs
{
 char *label;
 QPixmap *icon;

 short state;
 int  id;
 int  width;

 nodoListaTabs *izquierda, *derecha;
};

class newTab : public QWidget
{
 Q_OBJECT
private:
 nodoListaTabs *lista, *finalLista;
 int tabsZoneMaxWidth;

 int base;

 int selId;

 int lastBaseIndex;
 int cantidadTabs;

public:
 newTab(QWidget *parent, const char *name = 0, WFlags f = 0);
 ~newTab();

 int addTab(const char *label, int id);
 void removeTab(int id);
 void selectTab(int id) { mostrarTab(id); repaint(false); }
 int currentTab() { return selId; }

 void setNewInfo(int id);

 int height() { return 25; }

private:
 void recalcularMaximoIndice();

 void mostrarTab(int id);

 void paintEvent(QPaintEvent *e);
 void resizeEvent(QResizeEvent *e);
 void mousePressEvent(QMouseEvent *e);

signals:
 void tabChange(int id);
 void tabClose(int id);

};

#endif
