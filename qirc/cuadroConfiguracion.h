/***************************************************************************
                          cuadroConfiguracion.h  -  description
                             -------------------
    begin                : Tue Oct 10 2000
    copyright            : (C) 2000 by gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                          *
 **************************************************************************/

#ifndef CUADROCONFIGURACION_H
#define CUADROCONFIGURACION_H

#include <stdlib.h>
#include <qwidget.h>
#include <qlistview.h>
#include <qlabel.h>
#include <qcheckbox.h>
#include <qcolor.h>
#include <qspinbox.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qmessagebox.h>
#include <string.h>

#include "resource.h"
#include "Config.h"

#define ALTO_CONFIGURACION 				300
#define ANCHO_CONFIGURACION 			600
#define ANCHO_PANEL_CONFIGURACION 150
#define ANCHO_LABEL_CONFIGURACION	110

class cuadroConfiguracion : public QWidget
{
 Q_OBJECT

private:
 QListView *panel;
 QWidget *identidad, *modos, *conexion, *ctcp, *varios, *canales, *display, *logging, *dcc;
 QWidget *widgetActual;

 QLabel *titulo;

 QListViewItem *itemIdent, *itemModos, *itemDisplay, *itemConn, *itemCtcp, *itemVarios, *itemCanales, *itemLogging, *itemDcc;
 QLineEdit *nick1, *nick2, *userInfo, *user, *pass;
 QCheckBox *usarPass, *autoUser;

 QCheckBox *invisible, *wallops;

 QCheckBox *verMotd, *reconnect;
 QCheckBox *listEnConn;

 QCheckBox *verCtcp, *responderCtcpVersion;
 QLineEdit *ctcpUserInfo, *ctcpFinger;
 QSpinBox  *ctcpInterval;

 QLineEdit *partMessage, *quitMessage;
 QCheckBox *borrarMirc;
 QSpinBox *lineasRepetidas;

 QLineEdit *font;
 QSpinBox *fontSize;
 QPushButton *verFont;
 QLabel *muestra;

 QCheckBox *borrarIgnore, *elevarPrivados;

 QCheckBox *logChannels, *logPrivates, *borrarMircLogs;

 QSpinBox *dccConnectTimeout, *dccTransferTimeout, *dccBlockSize, *dccMaxWindowsCount;

 QPushButton *aceptar, *restaurar, *cancelar;

private:
 void crearPanel();
 void crearHojas();
 void crearBotonera();

 QLineEdit *putEntry(QWidget *father, int &y, const char *label, int anchoLabel, int anchoText = -1);
 QSpinBox *putSpin(QWidget *father, int &y, const char *label, int anchoLabel, int max, int min, int anchoSpin, int step = 1);
 QCheckBox *putCheck(QWidget *father, int &y, const char *label, int anchoLabel = -1, int cantidad = 1, int indice = 1);

 bool datosValidos();
public:
 cuadroConfiguracion();
 ~cuadroConfiguracion();

protected slots:
 void otraHoja(QListViewItem *item);
 void cargarConfiguracion();
 void guardarConfiguracion();
 void mostrarFont();

 void habilitarPassYUser(bool b) { pass->setEnabled(usarPass->isChecked()); user->setEnabled(!autoUser->isChecked()); }

 void closeEvent(QCloseEvent *e) { e->ignore(); }

 void clickAceptar();
 void clickRestaurar();
 void clickCancelar();

signals:
 void borrarVentanaConf();
};

#endif