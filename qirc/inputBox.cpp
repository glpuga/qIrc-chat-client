/***************************************************************************
                          inputBox.cpp  -  description
                             -------------------
    begin                : Thu Oct 5 2000
    copyright            : (C) 2000 by gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "inputBox.h"

inputBox::inputBox(const char *textoLabel, const char *textoPorDef = NULL, int maxLen = -1) : QWidget(NULL, NULL, WStyle_Customize | WStyle_DialogBorder)
{
 setFixedSize(ANCHO_INPUTBOX, ALTO_INPUTBOX);
 setCaption("Input box");

 if (textoLabel != NULL) {
  texto = new QLabel(textoLabel, this);
 } else {
  texto = new QLabel(this);
 }
texto->resize(ANCHO_INPUTBOX - 2 * MARCO, ALTO_LABEL);
texto->move(MARCO, MARCO);

 entrada = new QLineEdit(this);
 if (textoPorDef != NULL) entrada->setText(textoPorDef);
 if(maxLen > 0) entrada->setMaxLength(maxLen);
 entrada->resize(ANCHO_INPUTBOX - 2 * MARCO, ALTO_ENTRADA);
 entrada->move(MARCO, MARCO + texto->height() + DISTANCIA);

 // hago que el foco pertenezca a entrada
 entrada->setFocus();

 aceptar = new QPushButton("Accept", this);
 cancelar = new QPushButton("Cancel", this);
 aceptar->resize(ANCHO_BOTON, ALTO_BOTON);
 cancelar->resize(ANCHO_BOTON, ALTO_BOTON);
 int y = MARCO + texto->height() + 2 * DISTANCIA + entrada->height();
 aceptar->move((int)((ANCHO_INPUTBOX - 2 * ANCHO_BOTON) / 3), y);
 cancelar->move((int)(((ANCHO_INPUTBOX - 2 * ANCHO_BOTON) / 3) * 2  + ANCHO_BOTON),  y);

 connect(aceptar, SIGNAL(clicked()), SLOT(clickAceptar()));
 connect(cancelar, SIGNAL(clicked()), SLOT(clickCancelar()));
 connect(entrada, SIGNAL(returnPressed()), SLOT(clickAceptar()));

 show();
}

inputBox::~inputBox()
{
 delete texto;
 delete entrada;
 delete aceptar;
 delete cancelar;
}

void inputBox::clickAceptar()
{
 hide();

 emit resultado(entrada->text());
}

void inputBox::clickCancelar()
{
 hide();

 emit resultado(NULL);
}

