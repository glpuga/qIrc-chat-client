/***************************************************************************
                          ChatView.cpp  -  description
                             -------------------
    begin                : vie oct 20 13:41:53 ARST 2000
    copyright            : (C) 2000 by gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "ChatView.h"

ChatView::ChatView(QWidget *parent = 0, const char *name = 0) : QTableView(parent, name)
{
 // no hay ning�n nodo en la lista
 cantidad = 0;

 // el tabla mide 0 pixels
 tableHeight = 0;

 // inicializo el objeto fontMetrics
 metrica = new QFontMetrics(font());

 // inicializo la lista
 lista = NULL;

 // inicializo los anchos de las columnas
 anchoTotal = width() - verticalScrollBar()->width();

 // establezo el ancho de la primera columna
 setPrefixWidth(ANCHO_MINIMO);

 // establezco el n�mero de columnas a 2 y la cantidad de filas a 0 (no hay nada todav�a)
 setNumCols(2); // las dos principales, m�s dos de marco a los costados y una al medio para
                // separar las principales
 setNumRows(0);

 // ajustes finos
 setTableFlags(Tbl_vScrollBar);
 clearTableFlags(Tbl_smoothScrolling);
 setFrameStyle(Panel | Sunken);
 setLineWidth(3);
 setMidLineWidth(2);


 // conecto la se�al valueChanged del scrollvertical para poder controlar la variable stickyBottom
 connect(verticalScrollBar(), SIGNAL(valueChanged(int)), SLOT(scrollValueChanged(int)));

 // por defecto el color por defecto es el negro
 idColor = colorMirc(DEFAULT_FOREGROUND);

 // pongo el color de fondo
 setBackgroundColor(colorMirc(DEFAULT_BACKGROUND));

 // inicialmente centrado
 setPrefixAlignment(AlRight, AlTop);

 // creo la cola
 cola = new colaMensajes;

 // arranco el temporizador que lleva que redibuja todos los mensajes encolados cada
 // 200 ms
 timerId = startTimer(TIMER_INTERVAL);

 /// inicializo la variable stickybottom
 stickyBottom = true;
 currentY = 0;
}

ChatView::~ChatView()
{
 // elimino el temporizador
 killTimer(timerId);

 // vac�a la lista
 clear();

 // elimino la cola
 delete cola;

 // borro de memoria ltos objetos din�micos
 delete metrica;
}

void ChatView::clear()
{
 #ifdef DEBUG_CHATVIEW
  cout << "Limpiando la pantalla" << endl;
 #endif

 // por una extra�a cadena de acontecimientos, el programa funciona bien sin esto,
 // pero para hacerlo m�s claro reseteo los valores de stickyBottom y cuerrentY
 stickyBottom = true;
 currentY = 0;

 // antes que nada modifico la cantidad de filas
 cantidad = 0;
 setNumRows(cantidad);

 // establezo el ancho de la primera columna a su valor inicial
 setPrefixWidth(ANCHO_MINIMO);

 // actualizo el tama�o de la tabla
 updateTableSize();

 // vac�a la lista
 nodoLista *siguiente, *nodo = lista;
 while (nodo != NULL)
 {
  siguiente = nodo->enlace;

  delete [] nodo->identificador;
  delete [] nodo->mensajeOriginal;

  // vacio la lista de segmentos de texto
  nodoListaSegmentos *segmentoBorrado, *segmentoSiguiente = nodo->listaSegmentos;
  while (segmentoSiguiente != NULL)
  {
   segmentoBorrado = segmentoSiguiente;
   segmentoSiguiente = segmentoSiguiente->enlace;
   delete [] segmentoBorrado->texto;
   delete segmentoBorrado;
  }
  nodo->listaSegmentos = NULL;	

  delete nodo;
  nodo = siguiente;
 }
 lista = NULL;

 // la altura de la tabla es ahora 0
 tableHeight = 0;

 // vac�o la cola de mensajes
 cola->vaciar();
}

void ChatView::write(const char *identificador, const char *mensaje, int color = DEFAULT_FOREGROUND)
{
 #ifdef DEBUG_CHATVIEW
  cout << "Agregando elemento a la cola: " << identificador << "-" << mensaje << endl;
 #endif

 // simplemente encola los pedidos
 cola->insertar(identificador, mensaje, color);

 // eso es todo
}

void ChatView::setPrefixWidth(int ancho)
{
 #ifdef DEBUG_CHATVIEW
  cout << "Cambiando el ancho de la primera columna " << endl;
 #endif

 // el ancho de la primera columna no puede ser nunca y bajo ninguna circunstancia
 // mayor que 1/3 del ancho total
 // esto se revisa tambi�n cada vez que cambia el tama�o del widget
 if (ancho > (int)width() / 3) ancho = (int) width() / 3;

 if (ancho == ancho1) return;

 // cambio el ancho de la primera columna y el de la segunda para no cambiar el tama�o total
 ancho1 = ancho;
 ancho2 = anchoTotal - ancho;

 // ajusto las alturas
 ajustarTamanioCeldas();
}

void ChatView::ajustarTamanioCeldas()
{
 #ifdef DEBUG_CHATVIEW
  cout << "Ajustando las alturas de las filas" << endl;
 #endif

 // recorro toda la lista recalculando las alturas
 nodoLista *nodo = lista;
 tableHeight = 0;
 while (nodo != NULL)
 {
  // recalculo cuantas lineas requiere el elemento actual
  calcularValoresFila(nodo);

  tableHeight += nodo->alto;
  nodo = nodo->enlace;
 }
}

void ChatView::calcularValoresFila(nodoLista *nodo)
{
 // voy segmentando la lista y obtengo valores hasta que cambia el valor de color o
 // estilo, o bien hasta que alcanzo el final de una linea

 // limpio la lista de segmentos que anteriores
 nodoListaSegmentos *borrado, *siguiente = nodo->listaSegmentos;
 while (siguiente != NULL)
 {
  borrado = siguiente;
  siguiente = siguiente->enlace;
  delete [] borrado->texto;
  delete borrado;
 }
 nodo->listaSegmentos = NULL;	

 // ac� voy a ir acumulando los caracteres de cada segmento
 char linea[strlen(nodo->mensajeOriginal)+1];
 int indice = 0;
 char c;
 int foreground = DEFAULT_FOREGROUND;
 int background = DEFAULT_BACKGROUND;
 int xActual, yActual;
 bool bold, italic, underline;

 linea[0] = 0;
 xActual = 0; // parto del origen
 yActual = metrica->ascent();
 bold = italic = underline = false; // por defecto texto limpio
 // es un ciclo do, as� que al finalizar la cadena c vale el caracter de terminaci�n cero
 do {
  c = nodo->mensajeOriginal[indice];
  indice++;
  // si cambia el estilo o si llegu� al final de la linea agarro lo que tengo hasta ahora y
  // lo mando a la fila
  if ((c == MIRC_CTRL_B) || (c == MIRC_CTRL_U) || (c == MIRC_CTRL_R) ||
      (c == MIRC_CTRL_O) || (c == MIRC_CTRL_K) || (c == 0) ||
      ((xActual + metrica->width(linea) + metrica->width(c)) >= (ancho2-MARCOH)))
  {
   // se dio uno de esos casos
   // me fijo si tengo m�s de una letra (para evitar nodos sin texto)
   if (strlen(linea) > 0)
   {
    nodoListaSegmentos *nuevo = new nodoListaSegmentos;
    nuevo->texto = new char[strlen(linea)+1];
    strcpy(nuevo->texto, linea);
    nuevo->foreground = foreground;
    nuevo->background = background;
    nuevo->x 		 = xActual;
    nuevo->y     = yActual;
    nuevo->bold  = bold;
    nuevo->italic= italic;
    nuevo->underline = underline;
    // lo engancho en la lista
    nuevo->enlace = nodo->listaSegmentos;
    nodo->listaSegmentos = nuevo;
   }
   // ya cree el nodo, ahora actualizo los valores locales
   if ((c != MIRC_CTRL_B) && (c != MIRC_CTRL_U) && (c != MIRC_CTRL_R) &&
      (c != MIRC_CTRL_O) && (c != MIRC_CTRL_K) && (c != 0))
   {
    xActual = 0;
    yActual = yActual + metrica->height();
    // convierto a c en el primer caracter del siguiente segmento
    linea[0] = c;
    linea[1] = (char)0;
   } else {
    switch(c) {
     case 0           : break; // se termin� la cadena, no hago nada m�s.
     case MIRC_CTRL_B : bold = !bold; break;
     case MIRC_CTRL_U : underline = !underline; break;
     case MIRC_CTRL_R : italic = !italic; break;
     case MIRC_CTRL_O : bold = underline = italic = false; break; // vuelvo todo a cero
     case MIRC_CTRL_K : int contador = 0;
                        char strColor[3];
                        strColor[0] = 0; // es m�s r�pido que llamar a una funcion
                        while ((nodo->mensajeOriginal[indice] != 0) && (strchr("0123456789", nodo->mensajeOriginal[indice]) != NULL) && (contador < 2))
                        {
                         strncat(strColor, &nodo->mensajeOriginal[indice], 1);
                         contador++; indice++;
                        }
                        // si no hab�a ning�n n�mero es que hay que volver los colores a
                        // los valores por defecto
                        if (strlen(strColor) == 0)
                        {
                         foreground = DEFAULT_FOREGROUND;
                         background = DEFAULT_BACKGROUND;
                        } else {
                         // si hab�a n�meros
                         foreground = atoi(strColor);
                         strColor[0] = 0; // es m�s r�pido que llamar a una funcion
                         if (nodo->mensajeOriginal[indice] == ',')
  			                 {
        								  indice++;
                          contador = 0;
                          while ((nodo->mensajeOriginal[indice] != 0) && (strchr("0123456789", nodo->mensajeOriginal[indice]) != NULL) && (contador < 2))
                          {
                           strncat(strColor, &nodo->mensajeOriginal[indice], 1);
                           contador++; indice++;
                          }
                          background = atoi(strColor); // si hab�a coma tiene que haber otro n�mero
                         }
                        }
  										  break;

    } // switch()
    xActual += metrica->width(linea);
    linea[0] = 0; // limpio la cadena
   }
  } else {
   // no hay nada en particular en esta letra, as� que simplemente la acumulo
   strncat(linea, &c, 1);
  }
 } while(c != 0); // termin� cuando se haya procesado el cero

 nodo->alto = yActual + metrica->descent() + MARCOV;
 // return ((int)((metrica->width(texto) + ancho2 - MARCOH - 1) / (ancho2 - MARCOH)) * metrica->height()) + MARCOV; // creo que esta ser�a la f�rmula
}

void ChatView::scrollView()
{
 // - si el tama�o de la tabla es menor que el alto de la vista, no muevo el offset
 // - sino si stickyBottom = true muestro el �ltimo segmento de la tabla
 // - sino si stickyBottom = false muevo el offset a ese segmento de la tabla
 // si el nuevo offset es igual al offset actual, no se produce un cambio para evitar parpadeo
 int offset = 0;
 if (viewHeight() < tableHeight)
 {
  if (stickyBottom == true)
  {
   offset = tableHeight - viewHeight();
  } else {
   offset = currentY;
  }
 }

 if (offset != yOffset())
 {
  setYOffset(offset);
 }
}

void ChatView::resizeEvent(QResizeEvent *e)
{
 // el ancho de la primera columna no puede ser nunca y bajo ninguna circunstancia
 // mayor que 1/3 del ancho total
 // esto se revisa tambi�n cada vez que cambia el ancho de la columna
 if (ancho1 > (int)width() / 3) ancho1 = (int) width() / 3;

 // cambio el ancho de la segunda columna
 anchoTotal = e->size().width() - verticalScrollBar()->width();
 ancho2 = anchoTotal - ancho1;
 ajustarTamanioCeldas();
 updateTableSize();

 QTableView::resizeEvent(e);
}

void ChatView::setFont(const QFont &f)
{
 // sobrecargada para mantener actualizado el objeto fontMetrics
 // la �nica forma de cambiar la font del objeto fontMetrics es mediante su constructor
 delete metrica;
 metrica = new QFontMetrics(f);

 QTableView::setFont(f);

 // recalculo las alturas
 ajustarTamanioCeldas();
 updateTableSize();
 repaint();
}

void ChatView::paintCell ( QPainter * p, int row, int col )
{
 int x = 0, y = 0;
 nodoLista *nodo = buscarFila(row);

 switch(col) {
  case 0 :// me fijo como tiene que estar alineado
          // si el ancho de la primer columna es menor que el ancho de este id,
          // entonces se alinea siempre a izquierda
 				  p->setPen(idColor);
// 				  p->setPen(colorMirc((nodo->identificador[0]%10)+2));
          p->setBackgroundColor(colorMirc(DEFAULT_BACKGROUND));
          p->setBackgroundMode(OpaqueMode);

          switch (prefixVerAlignment) {
    				case AlBottom		: y = nodo->alto - MARCOV - metrica->descent(); break;
   					case AlCenterV	: y = (int)(nodo->alto - MARCOV + metrica->ascent()) / 2; break;
            default : /* case AlTop 	*/ y = metrica->ascent(); break;
  				}

          if (ancho1 - SEPARACION < metrica->width(nodo->identificador))
          { x = MARCOH;
            int i = 0;
            for (i = 0; metrica->width(nodo->identificador, i) < ancho1 - SEPARACION; i++)
            { /* vacio */ }
            p->drawText(x, y, nodo->identificador, i-1);
          } else {
    			 switch (prefixHorAlignment) {
    			   case AlCenterH	: x = MARCOH + (int)(ancho1 - metrica->width(nodo->identificador) - MARCOH - SEPARACION) / 2; break;
             case AlRight		: x = ancho1 - metrica->width(nodo->identificador) - SEPARACION; break;
             case AlLeft		: x = MARCOH; break;
  				 }
           p->drawText(x, y, nodo->identificador);
          }
 			 	 	break;
  case 1 :// voy dibujando uno por uno los segmentos del nodo
          nodoListaSegmentos *segmento = nodo->listaSegmentos;
          while(segmento != NULL)
          {
           QColor color;
           if (segmento->foreground == 1) // uso el color indicado
           { color = colorMirc(nodo->color); }
           else { color = colorMirc(segmento->foreground);}
           p->setPen(color);
           color = colorMirc(segmento->background);
           p->setBackgroundColor(color);
           p->setBackgroundMode(OpaqueMode);
       	   p->drawText(segmento->x, segmento->y, segmento->texto);
           if (segmento->bold == true)
           {
            // dibujo el mismo texto con un corrimiento de un pixel
            p->setBackgroundMode(TransparentMode);
       	    p->drawText(segmento->x+1, segmento->y, segmento->texto);
           }
           if (segmento->underline == true)
           {
            p->drawLine(segmento->x, segmento->y + metrica->descent(), segmento->x + metrica->width(segmento->texto), segmento->y + metrica->descent());
           }
           segmento = segmento->enlace;
  				}
          // vuelvo el color a la normalidad
          p->setBackgroundColor(colorMirc(DEFAULT_BACKGROUND));
 			    break;
 }
}

void ChatView::timerEvent( QTimerEvent *e )
{
 // pone en pantalla todas las lineas encoladas para mejorar el parpadeo y la velocidad
 // del chatView
 // es el que escribe en pantalla
 // tengo que crear un nuevo elemento en la lista y agregarle una fila a la tabla

 // si la cola est� vac�a salgo ya
 if (cola->estaVacia() == true) return;

 #ifdef DEBUG_CHATVIEW
  cout << "Desencolando mensajes. Hay " << cola->cantidad() << " elementos esperando escribirse" << endl;
 #endif

 nodoLista *nodo;
 int lPrefijo, lTexto;

 // no permito que la tabla se actualice sola
 setAutoUpdate(false);

 while(cola->estaVacia() == false)
 {
  cola->longitudDatos(lPrefijo, lTexto);

  nodo 								= new nodoLista;
  nodo->fila 					= cantidad;
  nodo->identificador	= new char[lPrefijo+1];
  strcpy(nodo->identificador, cola->extraerIdentificador());
  nodo->mensajeOriginal	= new char[lTexto+1];
  strcpy(nodo->mensajeOriginal, cola->extraerMensaje());
  nodo->color 				= cola->extraerColor();
  nodo->listaSegmentos= NULL; // importante: incializar!
  cola->eliminar();

  int anchoIdentificador = metrica->width(nodo->identificador);
  if (anchoIdentificador + SEPARACION + MARCOH > ancho1)
  {
   #ifdef CHATVIEW_DEBUG
    cout << "El identificador es demsiado ancho, reajustando..." << endl;
   #endif
   setPrefixWidth(anchoIdentificador + SEPARACION + MARCOH);
  }

  // recalculo los valores para la celda en base al ancho actual y el mensaje original
  // (lo hago despu�s de [posiblemente] haber cambiado el ancho de la primer columna)
  calcularValoresFila(nodo);

  // actualizo el alto de la tabla
  tableHeight += nodo->alto;

  // lo engancho a la lista
  nodo->enlace 	= lista;
  lista = nodo;

  // finalmente actualizo la tabla
  cantidad	= cantidad + 1;
  setNumRows(cantidad);

  // y vuelvo a empezar
 }

 #ifdef DEBUG_CHATVIEW
  cout << "-Terminado de desencolar. Redibujando" << endl;
 #endif

 // acomodo la vista de la pantalla si es necesario
 scrollView();

 // vuelvo a la normalidad la pantalla y repinto todo
 setAutoUpdate(true);
 repaint();

 #ifdef DEBUG_CHATVIEW
  cout << "-Redibujado!" << endl;
 #endif
}

int ChatView::cellWidth()
{
 // siempre regresa 0 porque las columnas tienen diferentes anchos
 return 0;
}

int ChatView::cellWidth(int col)
{
 // regresa el ancho de la primera o la segunda columna, o bien de los marcos o la separaci�n
 switch(col) {
  case 0: return ancho1; break;
  case 1: return ancho2; break;
  default:
   #ifdef DEBUG_CHATVIEW
    cout << "No existe la columna " << col << endl;
   #endif
   break;
 }
 return -1; // se asume error
}

int ChatView::cellHeight(int row)
{
 // busco la fila en la tabla y devuelvo su altura
 nodoLista *nodo = buscarFila(row);

 #ifdef DEBUG_CHATVIEW
  if (nodo == NULL)
  {
   cout << "No existe la fila pedida: " << row << endl;
   return 0;
  }
 #endif

 return nodo->alto;
}

nodoLista *ChatView::buscarFila(int row)
{
 nodoLista *nodo = lista;

 while  (nodo != NULL)
 {
  if (nodo->fila == row) return nodo;

  nodo = nodo->enlace;
 }

 // si lleg� ac� significa que no existe esa fila ( en la lista por lo menos )
 return NULL;
}

void ChatView::mousePressEvent(QMouseEvent *e)
{
 // seg�n el bot�n que sea emito la se�al correspondiente
 QPoint coordenada(e->globalX(), e->globalY());

 if ((e->button() & RightButton) != 0) emit rightButtonPressed(coordenada);
}

void ChatView::scrollValueChanged(int newValue)
{
 // si el nuevo valor es el m�s alto que puede tener el scroll
 // entonces es que el usuario lo arrastr� hasta el fondo
 if (newValue == verticalScrollBar()->maxValue())
 {
  stickyBottom = true;
 } else {
  stickyBottom = false;
 }

 // actualizo la variable currentY para saber donde tengo que mantener quieta la pantalla
 // mientras stickyBottom es false
 currentY = yOffset();
}

