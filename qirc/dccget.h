/***************************************************************************
                          dccget.h  -  description
                             -------------------
    begin                : Fri Nov 17 2000
    copyright            : (C) 2000 by Gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef DCCGET_H
#define DCCGET_H

// #define DEBUG_DCCGET

#include <qwidget.h>
#include <qmessagebox.h>
#include <qlabel.h>
#include <qsocketnotifier.h>
#include <qlineedit.h>
#include <qprogressbar.h>
#include <qpushbutton.h>
#include <qstring.h>

#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#ifdef DEBUG_DCCGET
 #include <iostream.h>
#endif

#include "Config.h"
#include "resource.h"
#include "IClientSocket.h"
#include "general.h"

#define DCCGET_WINDOW_TITLE "DCC Get Window"

#define DCCGET_ANCHO_LABEL 		60
#define DCCGET_ANCHO_LAB_IP 	120
#define DCCGET_ANCHO 					400
#define DCCGET_ALTO 					MARCO*2+ALTO_LABEL*4+ALTO_BARRA_PROGRESO+DISTANCIA*6+ALTO_BOTON

class DccGet : public QWidget
{
 Q_OBJECT

private:
 QSocketNotifier *notificaConexion, *notificaDatos;
 IClientSocket *socket;

 QLabel *lNick, *lArchivo, *eNick;
 QLabel *lIpLocal, *lIpRemota, *ipLocal, *ipRemota;
 QLabel *lStatus;
 QLineEdit *eArchivo;
 QProgressBar *barra;
 QPushButton *aceptar, *cancelar;

 int timeout, CONNECTTIMEOUT, TRANSFERTIMEOUT;

 char *buffer;

 unsigned long recibido, blockSize, fileSize;

 in_addr	ipOrigen;
 int			puerto;

 int fileHandler;

public: 
  DccGet(const char *nick, const char *filename, in_addr ip, unsigned int port, unsigned long fileSize);
	~DccGet();

private:
 void closeEvent(QCloseEvent *e) { e->ignore(); clickCancelar(); }
 void timerEvent(QTimerEvent *e);

private slots:
 void clickAceptar();
 void clickCancelar();
 void conexionCompleta(int);
 void dataRecv(int);

signals:
 void cerrarDccGet(DccGet *);
};

#endif
