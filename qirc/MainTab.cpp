/***************************************************************************
                          MainTab.cpp  -  description
                             -------------------
    begin                : Wed Dec 6 2000
    copyright            : (C) 2000 by Gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "MainTab.h"

MainTab::MainTab(QWidget *parent, const char *name = 0) : QWidget(parent, name)
{
 // creo las herramientas que voy a usar
 tab = new newTab(this);
 tab->setGeometry(0, 0, width(), tab->height());
 connect(tab, SIGNAL(tabChange(int)), SLOT(tabChange(int)));
 // la se�al de cerrado la retransmito
 connect(tab, SIGNAL(tabClose(int)), SIGNAL(tabClose(int)));

 actual   = NULL;
 lista    = NULL;

 currentId = 0;
 idEnPantalla = -1;
}

MainTab::~MainTab()
{
 // vacio la lista enlazada
 nodoMainTab *nodo, *sig;

 nodo = lista;
 while (nodo != NULL)
 {
  sig = nodo->enlace;
  delete nodo;
  nodo = sig;
 }
 lista = NULL;
}

void MainTab::addPage(const char *label, QWidget *w)
{
 // agrega una p�gina al tab, devuelve un entero que representa esa p�gina (id)
 // (no se garantiza el orden de los id)
 nodoMainTab *nodo = new nodoMainTab;

 nodo->id = currentId;
 currentId++;
 nodo->widget = w;
 nodo->enlace = lista;
 lista = nodo;

 // escondo el widget actual
 w->hide();

 // sololo puedo hacer una vez que ya agregu� el nodo a la lista
 tab->addTab(label, nodo->id);
}

void MainTab::removePage(int id)
{
 // elimino el nodo de id = id
 nodoMainTab *nodo = lista, *anterior = NULL;
 while ((nodo != NULL) && (nodo->id != id))
 {
  anterior = nodo;
  nodo = nodo->enlace;
 }
 if (nodo == NULL) return ; // no lo encontr�

 if (anterior == NULL)
 {
  // es el anterior
  lista = nodo->enlace;
 } else {
  anterior->enlace = nodo->enlace;
 }
 // no elimino el widget!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
 // de eso se tiene que encargar quien lo agreg� a la barra
 delete nodo;

 actual->hide();
 actual = NULL;

 // por �ltimo lo remuevo de la barra
 tab->removeTab(id);
}

void MainTab::raisePage(QWidget *w)
{
 // me aseguro de que no sea la misma que estoy mostrando
 if (actual == w) return;

 // busco la id del widget
 nodoMainTab *nodo = lista;
 while (nodo != NULL)
 {
  if (nodo->widget == w)
  {
   tab->selectTab(nodo->id);
   break;
  }
  nodo = nodo->enlace;
 }
}

void MainTab::raisePage(int id)
{
 tab->selectTab(id);
}

void MainTab::setNewInfo(QWidget *w)
{
 // me aseguro de que no sea la misma que estoy mostrando
 if (actual == w) return;

 // busco la id del widget
 nodoMainTab *nodo = lista;
 while (nodo != NULL)
 {
  if (nodo->widget == w)
  {
   tab->setNewInfo(nodo->id);
   break;
  }
  nodo = nodo->enlace;
 }
}

void MainTab::setNewInfo(int id)
{
 tab->setNewInfo(id);
}


void MainTab::resizeEvent(QResizeEvent *e)
{
 // ajusto el tama�o de los widgets hijos
 tab->setGeometry(0, 0, width(), tab->height());
 if (actual != NULL) actual->setGeometry(0, tab->height(), width(), height() - tab->height());
}

void MainTab::tabChange(int id)
{
 idEnPantalla = id;

 QWidget *nuevoActual = NULL;
 // busco el nuevo
 nodoMainTab *nodo = lista;
 while (nodo != NULL)
 {
  if (nodo->id == id)
  {
   nuevoActual = nodo->widget;
   break;
  }
  nodo = nodo->enlace;
 }
 if ((nuevoActual != NULL) && (nuevoActual != actual))
 {
  // muestro el nuevo y le ajusto el tama�o
  nuevoActual->setGeometry(0, tab->height(), width(), height() - tab->height());
  nuevoActual->show();
  nuevoActual->setFocus();

  if (actual != NULL)
  {
   actual->hide();
  }
  actual = nuevoActual;
 }
}