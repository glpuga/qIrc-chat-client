/***************************************************************************
                          newtab.cpp  -  description
                             -------------------
    begin                : Sat Dec 16 2000
    copyright            : (C) 2000 by Gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "newtab.h"

// todos los pixmaps que uso ac�
#include "leftArrowEnabled.xpm"
#include "leftArrowDisabled.xpm"
#include "rightArrowEnabled.xpm"
#include "rightArrowDisabled.xpm"
#include "tabRight.xpm"
#include "tabLeft.xpm"
#include "tabMiddle.xpm"
#include "tabClose.xpm"

// los estados del nombre de la ventana
// no hay novedades en el frente
#define Bored		0
// hay nueva informaci�n disponible
#define Waiting	1
// est� visible
#define Visible	2

newTab::newTab(QWidget *parent, const char *name = 0, WFlags f = 0) : QWidget(parent, name, f)
{
 // inicializo variables
 lista = finalLista = NULL;

 base = 0;
 cantidadTabs = 0; // la lista est� vac�a;
 lastBaseIndex = 0; // solo para mayor claridad, recalcularMaximoIndice la sobreescribe
 selId = -1; // no hay ninguno seleccionados

 // calculo los valores del ancho de la vista de tabs y el mayor indice que se puede mostrar
 tabsZoneMaxWidth = (width() > 75)?width()-75:0;
 recalcularMaximoIndice();
}

newTab::~newTab()
{
 // que elimine toda la tabla
 nodoListaTabs *nodo = lista, *aux;

 while (nodo != NULL)
 {
  aux = nodo->derecha;
  delete [] nodo->label;
  delete nodo;
  nodo = aux;
 }
 lista = finalLista = NULL;
}

int newTab::addTab(const char *label, int id)
{
 // creo un nuevo nodo, lo agrego a la lista y redibujo
 nodoListaTabs *nuevo = new nodoListaTabs;
 nuevo->label 	= new char[strlen(label)+1];
 strcpy(nuevo->label, label);
 nuevo->state		= Bored;
 nuevo->width 	= (((int)((fontMetrics().width(label) + 9)/10)*10)) + 8;
 nuevo->id 			= id;
 nuevo->izquierda = NULL;
 nuevo->derecha		= NULL;	

 if (lista == NULL)
 {
  // la lista est� vacia
  lista = finalLista = nuevo;
 } else {
  // coloco el nodo al final
  finalLista->derecha = nuevo;
  nuevo->izquierda = finalLista;
  finalLista = nuevo;
 }

 // aumento en uno la cantidad de tabs
 cantidadTabs++;
 // recalculo el maximo indice que puedo mostrar sin dejar espacio libre a la
 // derecha
 recalcularMaximoIndice();

 // me aseguro de que quede visible
 mostrarTab(nuevo->id); // preferir�a buscar alg�n m�todo m�s eficiente, pero bueno
 // muestro en pantalla
 repaint(false);

 return nuevo->id;
}

void newTab::mostrarTab(int id)
{
 // recorro la lista hasta encontrar el id buscado al mismo tiempo que cuento mi posici�n
 // cuando lo encuentro recorro la lista en sentido opuesto sumando los anchos de los tabs
 // para saber a partir de qu� indice contar

 nodoListaTabs *nodo = lista, *aux;
 int indice = 0;
 bool desmarcado = false;
 while ((nodo != NULL) && (nodo->id != id))
 {
  if (nodo->state == Visible)
   { nodo->state = Bored; desmarcado = true; }
  nodo = nodo->derecha;
  indice++;
 }
 if (nodo == NULL) return; // no lo encontre
 // marco este como el marcado
 nodo->state = Visible;
 selId = id;
 // desmarco cualquier otro
 aux = nodo->derecha;
 while ((aux != NULL) && (desmarcado == false))
 {
  if (aux->state == Visible)
   { aux->state = Bored; desmarcado = true; }
  aux = aux->derecha;
 }

 // el siguiente ciclo funciona por comprobaci�n emp�rica, no te�rica!!!!
 // no cambiar ni una letra! :)
 // el primer nodo (el que quiero que quede visible) lo tomo si o si, ya sea que entre
 // completo o parcialmente mostrado
 int suma = nodo->width;
 // si este elemento ya ocupa todo el ancho entonces no busco m�s
 if (suma < tabsZoneMaxWidth)
 {
  // sino sigo restando a indice hasta que encuentre un nodo que no quepa entero
  do {
   indice--;
   nodo = nodo->izquierda;
   if (nodo != NULL) suma += nodo->width;
  } while((nodo != NULL) && (suma < tabsZoneMaxWidth));
  indice++;
 }

 // en este punto indice indica que ordinal de nodo es el primer a pintar
 base = indice;

 // y aviso al exterior
 emit tabChange(id);
}

void newTab::removeTab(int id)
{
 // busco el id en la lista, lo elimino y redibujo la lista
 // si el que borr� es la tab habilitada, entonces habilito la siguente
 // (o la primera, si no hay siguiente)
 nodoListaTabs *nodo = lista, *anterior = NULL;
 while ((nodo != NULL) && (nodo->id != id))
 {
  anterior = nodo;
  nodo = nodo->derecha;
 }
 if (nodo == NULL) return; // no lo encontr�

 if (anterior == NULL)
 {
  lista = nodo->derecha;
 } else {
  anterior->derecha = nodo->derecha;
 }
 if (nodo->derecha != NULL) (nodo->derecha)->izquierda = anterior;

 if (nodo == finalLista)
 {
  finalLista = anterior; // que puede ser null o no
 }

 // reduzco el contador de nodos
 cantidadTabs--;
 // recalculo el mayor indice que puedo mostrar sin dejar espacio libre
 recalcularMaximoIndice();

 // si se borr� el tab que era visible, hago visible otro
 if (nodo->state == Visible)
 {
  if (nodo->derecha != NULL)
  {
   // lo selecciono
   mostrarTab((nodo->derecha)->id);
  } else {
   // no hab�a siguiente, as� que selecciono el primero (si existe)
   if (nodo->izquierda != NULL)
   {
    mostrarTab((nodo->izquierda)->id);
   } else {
    // llamo a mostrarTab con id -1, para indicar que no hay ninguna ventana selecionada
    mostrarTab(-1);
   }
  }
 }
 // muestro en pantalla
 repaint(false);

 // finalmente elimino el nodo
 delete [] nodo->label;
 delete nodo;
}

void newTab::setNewInfo(int id)
{
 // busco el nodo que tiene esa id, y si tiene status Bored lo marco como
 // Waiting y redibujo
 nodoListaTabs *nodo = lista;
 while (nodo != NULL)
 {
  if (nodo->id == id)
  {
   if (nodo->state == Bored)
   {
    nodo->state = Waiting;
    repaint(false);
   }
   return;
  }
  nodo = nodo->derecha;
 }
}

void newTab::resizeEvent(QResizeEvent *e)
{
 // mantengo correcto el valor de tabsZoneMaxWidth
 tabsZoneMaxWidth = (width() > 75)?width()-75:0;
 recalcularMaximoIndice();
}

void newTab::paintEvent(QPaintEvent *e)
{
 QPainter p(this);

 // empiezo por pintar las flechas
 if (base <= 0)
 {
  p.drawPixmap(3, 3, leftArrowDisabled_icon);
 } else {
  p.drawPixmap(3, 3, leftArrowEnabled_icon);
 }
 p.drawPixmap(width() - 47, 3, tabClose_icon);
 if (base >= lastBaseIndex)
 {
  p.drawPixmap(width() - 22, 3, rightArrowDisabled_icon);
 } else {
  p.drawPixmap(width() - 22, 3, rightArrowEnabled_icon);
 }

 // ahora busco en la lista el primer nodo a mostrar
 nodoListaTabs *nodo = lista;
 int indice = 0;
 while ((nodo != NULL) && (indice < base))
 {
  indice++;
  nodo = nodo->derecha;
 }
 // por si las dudas
 if (nodo == NULL) return; // no lo encontr�!

 // establezco que recorte los bordes
 p.setClipRect(25, 0, tabsZoneMaxWidth, 25);
 p.setClipping(true);

 // que el texto sea tranparente
 p.setBackgroundMode(TransparentMode);

 // limpio el margen por encima de los bordes curvados de los tabs
 p.setPen(backgroundColor());
 p.setBrush(backgroundColor());
 p.drawRect(25, 0, tabsZoneMaxWidth, 4); // la variable x ya ten�a sumados los 25
                                              // de la flecha izquierda

 int x = 25, xAux;
 while ((nodo != NULL) && (x < (25 + tabsZoneMaxWidth)))
 {
  xAux = x;
  p.drawPixmap(xAux, 0, QPixmap(tabLeft_icon)); xAux += 4;
  for (int i = 0; i < ((nodo->width + 1) / 10); i++)
  {
   p.drawPixmap(xAux, 0, QPixmap(tabMiddle_icon));
   xAux += 10;
  }
  p.drawPixmap(xAux, 0, QPixmap(tabRight_icon));
  xAux += 4;

  if(nodo->state != Visible)
  {
   p.setPen(white);
   p.drawLine(x, 22, x + nodo->width, 22);
   if (nodo->state == Bored)
   { p.setPen(black);
   } else {  // est� en Waiting, o sea, hay informaci�n disponible para visualizarce
     p.setPen(red);
   }
  } else {
   p.setPen(blue);
  }
  p.drawText(x + ((nodo->width - fontMetrics().width(nodo->label)) / 2), 5 +fontMetrics().ascent(), nodo->label);

  x += nodo->width;
  nodo = nodo->derecha;
 }
 p.setPen(backgroundColor());
 p.setBrush(backgroundColor());
 p.drawRect(x, 4, tabsZoneMaxWidth-x+25, 21); // la variable x ya ten�a sumados los 25
                                              // de la flecha izquierda
 // y completo la linea hasta el final
 p.setPen(white);
 p.drawLine(x, 22, tabsZoneMaxWidth+25, 22); // no le resto x porque necesito la posici�n x, no la longitud de la linea
}

void newTab::mousePressEvent(QMouseEvent *e)
{
 // en base a como deberi�n estar dibuajados me fijo en que solapa fue el click
 // ahora busco en la lista el primer nodo a mostrar
 int x = e->x(), y = e->y();

 if ((x >= 3) && (x < 22) && (y >= 3) && (y < 22))
 {
  // click en la flecha izquierda
  if (base > 0)
  {
   base--;
   repaint(false);
  }
  return;
 }

 if ((x >= 53 + tabsZoneMaxWidth) && (x < 72 + tabsZoneMaxWidth) && (y >= 3) && (y < 22))
 {
  // click en la flecha derecha
  if (base < lastBaseIndex)
  {
   base++;
   repaint(false);
  }
  return;
 }

 if ((x >= 28 + tabsZoneMaxWidth) && (x < 47 + tabsZoneMaxWidth) && (y >= 3) && (y < 22))
 {
  emit tabClose(selId);
  return;
 }

 // me ubico para saltear los que no estan en pantalla
 nodoListaTabs *nodo = lista;
 int indice = 0;
 while ((nodo != NULL) && (indice < base))
 {
  indice++;
  nodo = nodo->derecha;
 }
 // por si las dudas
 if (nodo == NULL) return; // no lo encontr�!

 // recorro la lista hasta encontrar el que est� en la posici�n indicada
 int actX = 25 + nodo->width;
 while ((nodo != NULL) && (actX < x))
 {
  nodo = nodo->derecha;
  if (nodo != NULL) actX += nodo->width;
 }
 // por si las dudas
 if (nodo == NULL) return; // no lo encontr�!

 // cambio la solapa
 if(nodo->id != selId) mostrarTab(nodo->id);

 // muestro en pantalla
 repaint(false);
}

void newTab::recalcularMaximoIndice()
{
 // estoy parado en el �ltimo elemento
 nodoListaTabs *nodo = finalLista;
 int indice = cantidadTabs-1; // -1 porque quiero el �ndice del �ltimo tab, no la cantidad de tabs

 // a menos que nodo != NULL no hago nada m�s que asignar -1 a lastBaseIndex
 // notese que ese valor no le importa porque solo le sirve a mousePressEvent
 if (nodo != NULL)
 {
  // el siguiente ciclo funciona por comprobaci�n emp�rica, no te�rica!!!!
  // no cambiar ni una letra! :)
  // el primer nodo (el que quiero que quede visible) lo tomo si o si, ya sea que entre
  // completo o parcialmente mostrado
  int suma = nodo->width;
  // si este elemento ya ocupa todo el ancho entonces no busco m�s
  if (suma < tabsZoneMaxWidth)
  {
   // sino sigo restando a indice hasta que encuentre un nodo que no quepa entero
   do {
    indice--;
    nodo = nodo->izquierda;
    if (nodo != NULL) suma += nodo->width;
   } while((nodo != NULL) && (suma < tabsZoneMaxWidth));
   indice++;
  }
 }

 // guardo el resultado
 lastBaseIndex = indice;
}
