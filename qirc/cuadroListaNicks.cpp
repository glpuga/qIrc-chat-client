/***************************************************************************
                          cuadroListaNicks.cpp  -  description
                             -------------------
    begin                : Tue Oct 17 2000
    copyright            : (C) 2000 by gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cuadroListaNicks.h"

// esta variable est� definida en tellme.cpp
extern Config *config;

#define ANCHO_LISTANICKS  300
#define ALTO_LISTANICKS   300

cuadroListaNicks::cuadroListaNicks(const char *caption, const char *text, const char *counterKey, const char *dataKeyPreffix) : QWidget(NULL, NULL, WStyle_Customize | WStyle_DialogBorder)
{
 // los datos principales
 setCaption(caption);
 setFixedSize(ANCHO_LISTANICKS, ALTO_LISTANICKS);

 label = new QLabel(text, this);
 label->setGeometry(MARCO, MARCO, ANCHO_LISTANICKS - 2*MARCO, ALTO_LABEL);
 label->setAlignment(AlignCenter);

 lista = new QListBox(this);
 lista->setGeometry(MARCO, MARCO + DISTANCIA + ALTO_LABEL, ANCHO_LISTANICKS - 2*MARCO, ALTO_LISTANICKS - 4*DISTANCIA - 2 * ALTO_BOTON - ALTO_LABEL - 2*MARCO - ALTO_ENTRADA);

 linea = new QLineEdit(this);
 linea->setGeometry(MARCO, MARCO + ALTO_LABEL + DISTANCIA*2 + lista->height(), ANCHO_LISTANICKS - 2*MARCO, ALTO_ENTRADA);

 int x1, x2;
 x1 = MARCO + (int)((ANCHO_LISTANICKS - 2*MARCO - 2*ANCHO_BOTON) / 3);
 x2 = 2*x1 - MARCO + ANCHO_BOTON;
 agregar = new QPushButton("Add", this);
 quitar = new QPushButton("Del", this);
 agregar->setGeometry(x1, ALTO_LISTANICKS - 2*ALTO_BOTON - DISTANCIA - MARCO, ANCHO_BOTON, ALTO_BOTON);
 quitar->setGeometry(x2, ALTO_LISTANICKS - 2*ALTO_BOTON - DISTANCIA - MARCO, ANCHO_BOTON, ALTO_BOTON);
 connect(agregar, SIGNAL(clicked()), SLOT(bAgregar()));
 connect(quitar, SIGNAL(clicked()), SLOT(bQuitar()));

 aceptar = new QPushButton("Accept", this);
 x1 = (int)((ANCHO_LISTANICKS - ANCHO_BOTON) / 2);
 aceptar->setGeometry(x1, ALTO_LISTANICKS - ALTO_BOTON - DISTANCIA, ANCHO_BOTON, ALTO_BOTON);
 connect(aceptar, SIGNAL(clicked()), SLOT(bAceptar()));

 int i = config->readIntValue(counterKey, 0);
 char *key = new char[strlen(dataKeyPreffix) + 31];
 for (int o = 0; o < i; o++)
 {
  sprintf(key, "%s%i", dataKeyPreffix, o);
  lista->inSort(config->readStrValue(key, "THIS IS A BUG!!! SEND ME AN EMAIL!!"));
 }
 delete [] key;

 // guardo los nombres de las claves
 cuadroListaNicks::counterKey = new char[strlen(counterKey) +1];
 cuadroListaNicks::dataKeyPreffix = new char[strlen(dataKeyPreffix) +1];
 strcpy(cuadroListaNicks::counterKey, counterKey);
 strcpy(cuadroListaNicks::dataKeyPreffix, dataKeyPreffix);

 // finalmente muestro el widget
 show();
}

cuadroListaNicks::~cuadroListaNicks()
{
 // lo unico que necesito borrar son las dos cadenas de texto
 delete [] counterKey;
 delete [] dataKeyPreffix;
}

void cuadroListaNicks::bAceptar()
{
 int i = lista->count();
 char *key = new char[strlen(dataKeyPreffix) + 21];
 for (int o = 0; o < i; o++)
 {
  sprintf(key, "%s%i", dataKeyPreffix, o);
  config->writeStrValue(key, lista->text(o));
 }
 config->writeIntValue(counterKey, i);
 delete [] key;

 // emito la se�al que indica que la ventana se quiere cerrar
 emit terminado();
}

void cuadroListaNicks::bAgregar()
{
 if (strlen(linea->text()) == 0)
 {
  // la cadena est� vac�a
  return;
 }
 if (strcspn(linea->text(), " ") != strlen(linea->text()))
 {
  // la cadena tiene espacios en blanco
  QMessageBox::warning(NULL, "Error", "Nicknames can't have spaces", "Continue");
  return;
 }

 lista->inSort(linea->text());

 // vacio la linea
 linea->clear();
}

void cuadroListaNicks::bQuitar()
{
 if (lista->currentItem() != -1)
 {
  lista->removeItem(lista->currentItem());
 }
}