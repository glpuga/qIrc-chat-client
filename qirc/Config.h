/***************************************************************************
                          Config.h  -  description
                             -------------------
    begin                : Fri Oct 6 2000
    copyright            : (C) 2000 by gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CONFIG_H
#define CONFIG_H

// para fines de depuración
// #define DEBUG_CONFIG

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "resource.h"

#ifdef DEBUG_CONFIG
  #include <iostream.h>
#endif

// estructura auxiliar
struct tipoNodoLista
{
  char *key;
  char *value;

  tipoNodoLista *enlace;
};

// clase de una archivo de configuración
class Config
{
private:
 tipoNodoLista *lista;

 char *path;

public:
 Config(const char *archivo);
 ~Config();

 const char *readStrValue(const char *key, const char *def);
 bool readBoolValue(const char *key, bool def);
 int readIntValue(const char *key, int def);

 void writeStrValue(const char *key, const char *value);
 void writeBoolValue(const char *key, bool value);
 void writeIntValue(const char *key, int value);

 void sync(); // actualiza el archivo en disco

protected:
 void cargarArchivo(const char *path);
 void guardarArchivo(const char *path);

 void parseKey(const char *linea, char *key);
 void parseValue(const char *linea, char *value);

 void agregarNodo(const char *key, const char *value);
 tipoNodoLista *buscarKey(const char *key);
 void borrarLista();

 void mystrlwr(char *str);
};

#endif