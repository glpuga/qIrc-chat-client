/***************************************************************************
                          Config.cpp  -  description
                             -------------------
    begin                : Fri Oct 6 2000
    copyright            : (C) 2000 by gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "Config.h"

Config::Config(const char *archivo)
{
 #ifdef DEBUG_CONFIG
  cout << "Creando un objeto Config" << endl;
 #endif

  // para poder utilizar la lista
  lista = NULL;

 // abre o crea el archivo apuntado por $HOME + archivo para lectura
 // primero obtengo el valor de $HOME (si hay alguno)
 if (getenv(HOME_ENVVAR) == NULL) {
   path = new char [strlen(archivo) + 1];
   strcpy(path, "");
 } else {
  path = new char [strlen(getenv(HOME_ENVVAR)) + strlen(archivo) + 2];
  strcpy(path, getenv(HOME_ENVVAR));
  strcat(path, "/");
 }
 strcat(path, archivo); // ahora en archivo est� la ruta completa al archivo

 #ifdef DEBUG_CONFIG
  cout << "La ruta del archivo es: " << path << endl;
 #endif

 // ahora abro el archivo
 cargarArchivo(path);
}

Config::~Config()
{
 guardarArchivo(path);

 borrarLista();

 delete [] path;

 #ifdef DEBUG_CONFIG
  cout << "Creando un objeto Config" << endl;
 #endif
}

const char *Config::readStrValue(const char *key, const char *def)
{
 tipoNodoLista *nodo = buscarKey(key);

 if (nodo == NULL)
 {
  // esa key no existe, la creo con el valor por defecto
  writeStrValue(key, def);
  return def;
 } else {
  return nodo->value;
 }
}

bool Config::readBoolValue(const char *key, bool def)
{
 tipoNodoLista *nodo = buscarKey(key);

 if (nodo == NULL)
 {
  // esa key no existe, la creo con el valor por defecto
  writeBoolValue(key, def);
  return def;
 } else {
   if (strcasecmp(nodo->value, "true") == 0)
   {
    return true;
   } else {
    return false;
   }
  }
}

int Config::readIntValue(const char *key, int def)
{
 tipoNodoLista *nodo = buscarKey(key);

 if (nodo == NULL)
 {
  // esa key no existe, la creo con el valor por defecto
  writeIntValue(key, def);
  return def;
 } else {
  return atoi(nodo->value);
 }
}

void Config::writeStrValue(const char *key, const char *value)
{
 tipoNodoLista *nodo = buscarKey(key);

 if (nodo == NULL)
 {
  // esa key no existe, la creo con el valor por defecto
  agregarNodo(key, value);
 } else {
  // existe, reemplazo el value actual
  delete [] nodo->value;
  nodo->value = new char[strlen(value) + 1];
  strcpy(nodo->value, value);
 }
}

void Config::writeBoolValue(const char *key, bool value)
{
 tipoNodoLista *nodo = buscarKey(key);

 if (nodo == NULL)
 {
  // esa key no existe, la creo con el valor por defecto
  if (value == true)
  {
   agregarNodo(key, "true");
  } else {
   agregarNodo(key, "false");
  }
 } else {
  // existe, reemplazo el value actual
  delete [] nodo->value;
  nodo->value = new char[6];
  if (value == true)
  {
   strcpy(nodo->value, "true");
  } else {
   strcpy(nodo->value, "false");
  }
 }
}

void Config::writeIntValue(const char *key, int value)
{
 tipoNodoLista *nodo = buscarKey(key);

 char numero[20];
 sprintf(numero, "%i", value);

 if (nodo == NULL)
 {
   // esa key no existe, la creo con el valor por defecto
  agregarNodo(key, numero);
 } else {
  // existe, reemplazo el value actual
  delete [] nodo->value;
  nodo->value = new char[strlen(numero) + 1];
  strcpy(nodo->value, numero);
 }
}

void Config::sync()
{
 guardarArchivo(path);
}

void Config::cargarArchivo(const char *path)
{
 FILE *f = fopen(path, "r");

 if (f == NULL) // el archivo no existe
 {
   #ifdef DEBUG_CONFIG
     cout << "No se pudo abrir el archivo " << path << " para leer la configuraci�n. Se usar�n los valores por defecto" << endl;
   #endif
  return;
 }

 char linea[300], key[300], value[300];
 while(fgets(linea, 299, f) != NULL)
 {
  parseKey(linea, key);
  parseValue(linea, value);

  agregarNodo(key, value);
 }

 fclose(f);
}

void Config::parseKey(const char *lineaCompleta, char *key)
{
 int indice = 0;

 strcpy(key, ""); // para asegurarme de que quede vac�a

 while(lineaCompleta[indice] == ' ') { indice++; } // elimin� todos los espacios
 while((lineaCompleta[indice] != ' ') && (lineaCompleta[indice] != 0))
 {
  // solo lo agrego a la cadena si no es ni #10 ni #13
 if ((lineaCompleta[indice] != 10) && (lineaCompleta[indice] != 13)) strncat(key, &lineaCompleta[indice], 1);
  indice++;
 }

 // pongo en min�sculas las cadenas
 mystrlwr(key);
}

void Config::parseValue(const char *lineaCompleta, char *value)
{
 int indice = 0;

 strcpy(value, ""); // para asegurarme de que quede vac�a

 while(lineaCompleta[indice] == ' ') { indice++; } // elimin� todos los espacios
 while((lineaCompleta[indice] != ' ') && (lineaCompleta[indice] != 0)) { indice++; } // elimin� la key
 while(lineaCompleta[indice] == ' ') { indice++; } // elimin� todos los espacios al medio
 while (lineaCompleta[indice] != 0)
 {
   if ((lineaCompleta[indice] != 10) && (lineaCompleta[indice] != 13)) strncat(value, &lineaCompleta[indice], 1);
  indice++;
 }
}

void Config::guardarArchivo(const char *path)
{
 FILE *f = fopen(path, "w");

 if (f == NULL) // error al abrir el archivo
 {
   #ifdef DEBUG_CONFIG
     cout << "No se pudo abrir el archivo " << path << " para guardar la configuraci�n! Se perder�n los datos" << endl;
   #endif
  return;
 }

 tipoNodoLista *nodo = lista;

 while(nodo != NULL)
 {
  fprintf(f, "%s %s\n", nodo->key, nodo->value);
  nodo = nodo->enlace;
 }


 fclose(f);
}

void Config::agregarNodo(const char *key, const char *value)
{
 // agrega los nodos ordenados por inserci�n

 tipoNodoLista *nodo = new tipoNodoLista;

 #ifdef DEBUG_CONFIG
   cout << "Agregando nodo: " << key << " / " << value << endl;
 #endif

 nodo->key = new char[strlen(key) +1];
 nodo->value = new char[strlen(value)+1];
 strcpy(nodo->key, key);
 strcpy(nodo->value, value);
 nodo->enlace = NULL;

 // lo comparo con el primer elemento
 if ((lista == NULL) || (strcmp(key, lista->key) < 0))
 {
  // es menor, va antes en la lista o bien la lista es null (primer elemento)
  nodo->enlace = lista;
  lista = nodo;
  return;
 }
 // el nuevo nodo va en alg�n punto entre el primer nodo (no incluido) y el
 // final de la lista. Necesito un apuntador al elemento anterior de la lista
 tipoNodoLista *apAnt = lista, *apPos = lista->enlace;
 while (apAnt != NULL)
 {
  if ((apPos == NULL) || (strcmp(key,apPos->key) < 0)) // gracias a la resoluci�n en cortocircuito
  {
   // es menor que el elemento apuntado por apPos o bien la lista se acab�
   // as� que lo inserto entre los dos apuntadores
   apAnt->enlace = nodo;
   nodo->enlace = apPos;
   return;
  }
  apAnt = apPos;
  apPos = apPos->enlace;
 }

 // nunca deber�a llegar ac�!
 #ifdef DEBUG_CONFIG
  cout << "En Config::insertarNodo() el ciclo termin� sin insertar el nodo!" << endl;
 #endif
}

void Config::borrarLista()
{
 tipoNodoLista *nodo;

 while (lista != NULL)
 {
  nodo = lista;
  lista = lista->enlace;

  delete [] nodo->key;
  delete [] nodo->value;
  delete nodo;
 }
}

tipoNodoLista *Config::buscarKey(const char *key)
{
 // busca en la lista la key, y si no la encuentra devuelve NULL
 tipoNodoLista *nodo = lista;

 while (nodo != NULL)
 {
  if (strcasecmp(key, nodo->key) == 0)
  {
    // lo encontr�
   return nodo;
  }
  nodo = nodo->enlace;
 }
 return NULL;
}

void Config::mystrlwr(char *str)
{
 int indice = 0;
 while (str[indice] != 0) {
  str[indice] = tolower(str[indice]);
  indice++;
 }
}