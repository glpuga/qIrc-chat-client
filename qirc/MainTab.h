/***************************************************************************
                          widgettab.h  -  description
                             -------------------
    begin                : Wed Dec 6 2000
    copyright            : (C) 2000 by Gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MAINTAB_H
#define MAINTAB_H

#include <qwidget.h>

#include "newtab.h"
#include "resource.h"

struct nodoMainTab
{
 int id;
 QWidget *widget;

 nodoMainTab *enlace;
};

class MainTab : public QWidget
{
 Q_OBJECT

private:
 newTab *tab;
 QWidget *actual;

 nodoMainTab *lista;

 int currentId;
 int idEnPantalla;

public: 
 MainTab(QWidget *parent, const char *name = 0);
 ~MainTab();

 int nextId() { return currentId; } // es la unica forma de saber la id que va a tener un
                                     // tab antes de haberlo cread, util en manejadorventanas
 void addPage(const char *label, QWidget *w);
 void removePage(int id);
 void raisePage(QWidget *w);
 void raisePage(int id);
 void setNewInfo(QWidget *w);
 void setNewInfo(int id);

private:
 void resizeEvent(QResizeEvent *e);

private slots:
 void tabChange(int);

signals:
 void tabClose(int id);
};

#endif
