/***************************************************************************
                          colamensajes.h  -  description
                             -------------------
    begin                : Thu Oct 26 2000
    copyright            : (C) 2000 by gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef COLAMENSAJES_H
#define COLAMENSAJES_H

#include <string.h>
#include <stdio.h> // para sacar NULL
#include <qcolor.h>

struct nodoColaMensajes
{
 char *identificador;
 char *mensaje;
 int color;

 nodoColaMensajes *enlace;
};

class colaMensajes
{
private:
 nodoColaMensajes *inicio, *fin;

 int numElementos;

public: 
	colaMensajes();
	~colaMensajes();

 void vaciar();
 void insertar(const char *identificador, const char *mensaje, const int color);
 void longitudDatos(int &longIdentificador, int &longMensaje);
 const char *extraerIdentificador();
 const char *extraerMensaje();
 const int extraerColor();
 void eliminar();

 bool estaVacia();

 int cantidad() {return numElementos; }
};

#endif
