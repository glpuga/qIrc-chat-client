/***************************************************************************
                          dccsend.h  -  description
                             -------------------
    begin                : Tue Nov 14 2000
    copyright            : (C) 2000 by Gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef DCCSEND_H
#define DCCSEND_H

// para la etapa de debug
// #define DEBUG_DCCSEND

#include <qwidget.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qprogressbar.h>
#include <qfiledialog.h>
#include <qstring.h>
#include <qmessagebox.h>
#include <qsocketnotifier.h>

#ifdef DEBUG_DCCSEND
 #include <iostream.h>
#endif

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/stat.h>

#include "Config.h"
#include "IServerSocket.h"
#include "IClientSocket.h"
#include "resource.h"
#include "general.h"

#define DCC_SEND_WINDOW_TITLE "DCC Send Window"

#define DCCSEND_ANCHO_LABEL 	60
#define DCCSEND_ANCHO_LAB_IP 	120
#define DCCSEND_ANCHO_BOTON_ARCHIVO  40
#define DCCSEND_ANCHO 				400
#define DCCSEND_ALTO 					MARCO*2+ALTO_LABEL*4+ALTO_BARRA_PROGRESO+DISTANCIA*6+ALTO_BOTON

class DccSend : public QWidget
{
 Q_OBJECT

private:
 QSocketNotifier *notificaConexion, *notificaDatos;
 IServerSocket *socketEspera;
 IClientSocket *socketCliente;

 QLabel *lNick, *lArchivo;
 QLabel *lIpLocal, *lIpRemota, *ipLocal, *ipRemota;
 QLabel *lStatus;
 QLineEdit *eNick, *eArchivo;
 QProgressBar *barra;
 QPushButton *buscarArchivo;
 QPushButton *conectar, *cerrar;

 int fileHandler;

 long blockSize;
 unsigned long enviado, fileSize;

 char *buffer;

 int timeout, CONNECTTIMEOUT, TRANSFERTIMEOUT;

public:
	DccSend(const char *nick = 0, const char *archivo = 0);
	~DccSend();

private:
 void closeEvent(QCloseEvent *);
 void timerEvent(QTimerEvent *);

private slots:
 void clickBuscarArchivo();
 void clickConectar();
 void clickCerrar();

 void connectionQuery(int s);
 void dataAck(int s);

signals:
 void lineaComando(const char *ventana, const char *mensaje); // para que funcione con nuevaLineaVentana en QIrc
 void dccSendCerrar(DccSend *punteroVentana);
};

#endif
