/***************************************************************************
                          notifyShowcase.cpp  -  description
                             -------------------
    begin                : Thu Dec 7 2000
    copyright            : (C) 2000 by Gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "notifyshowcase.h"

// los pixmaps que van en este widget
#include "online.xpm"
#include "offline.xpm"
#include "justconn.xpm"

// esta variable est� definida en tellme.cpp
extern Config *config;

#define Online 0
#define Offline 1
#define JustConnected 2

baseNotifyShowcase::baseNotifyShowcase(QWidget *parent, const char *name = 0) : QTableView(parent, name)
{
 // para que resetee la primera vez
 nextTimeReset = false;

 // la lista tambi�n
 lista = NULL;

 // obtengo los datos de ancho y alto de la font que est� seteada
 metrica = new QFontMetrics(fontMetrics()); // constructor de copia

 // inicialmente los anchos de las tres columnas son cero
 ancho1 = 24;
 ancho2 = 50;
 ancho3 = 100;

 // establezco el color de fondo y el comportamiento del widget
 setBackgroundColor(white);
 setTableFlags(Tbl_autoScrollBars | Tbl_smoothHScrolling);
 setFrameStyle(Panel | Sunken);
 setLineWidth(3);
 setMidLineWidth(2);

 // el ancho de las filas es variable
 setCellWidth(0);
 // el alto es igual al alto de la font (o 20, o no podr�a mostrar el �cono)
 setCellHeight(metrica->height()<20?20:metrica->height());

 // le digo cuantas columnas y filas tiene al empezar
 setNumCols(3);
 setNumRows(0);

 // finalmente cargo la lista de nicks que se notifican
 actualizarLista();

 // no hay nuevos nicks
 hayNuevosNicks = false;
 // muestro lo que tenga hasta ahora
 update();
}

baseNotifyShowcase::~baseNotifyShowcase()
{
 delete metrica;

 vaciarLista();
}

void baseNotifyShowcase::reset()
{
 // los pongo todos a offline
 nodoListaNotifies *nodo = lista;
 while (nodo != NULL)
 {
  nodo->status = Offline;
  nodo = nodo->enlace;
 }

 // actualizo la pantalla
 update();
}

void baseNotifyShowcase::isonReply(const char *reply)
{
 // actualizo la lista con los nicks que tengo que notificar
 actualizarLista();

 // marco todas los nodos como no cambiados
 nodoListaNotifies *nodo = lista;
 while (nodo != NULL)
 {
  nodo->cambiado = false;
  nodo = nodo->enlace;
 }

 // recorro la respuesta buscando nicks
 bool finalizado = false;
 int indice = 0;
 char nick[strlen(reply)+1];
 strcpy(nick, "");

 while (finalizado == false)
 {
  if ((reply[indice] == ' ') || (reply[indice] == 0))
  {
   // si fue cero, termin� con la cadena
   if (reply[indice] == 0) finalizado = true;

   // muestro que el nick est� online
   if (strlen(nick) > 0)
   {
    nodoListaNotifies *nodo = lista;
    while (nodo != NULL)
    {
     if (strcasecmp(nodo->nick, nick) == 0)
     {
      // marco el nodo para saber que fue cambiado
      nodo->cambiado = true;
      // est� conectado, me fijo si ya lo estaba antes o si se conect� ahora
      switch (nodo->status) {
       case Online    : break;
       case Offline   : // lo marco como justconnected
                        nodo->status = JustConnected;
                        // y pongo a true la bandera que indica que hay nuevos nicks
                        // conectados
                        hayNuevosNicks = true;
                        break;
       case JustConnected : // lo marco como online
                        nodo->status = Online;
                        break;
      }
     }
     nodo = nodo->enlace;
    }
    strcpy(nick, "");
   }
  } else {
   // agrego la letra al nick actual
    strncat(nick, (reply+indice), 1);
  }
  indice++;
 }

 // marco todos los nodos que no hayan tenido cambios como offline
 nodo = lista; // est� declarada m�s arriba
 while (nodo != NULL)
 {
  if (nodo->cambiado == false)
  {
   nodo->status = Offline;
  }
  nodo = nodo->enlace;
 }

 // muestro los cambios en pantalla
 update();
}

void baseNotifyShowcase::actualizarLista()
{
 // se fija que todos los nicks que estan en seteados en la configuraci�n aparezcan listados
 // y que los que ya no estan se eliminen de la lista

 // marco todos los elementos de la lista como si no existieran
 nodoListaNotifies *nodo = lista;
 while(nodo != NULL)
 {
  nodo->cambiado = false;
  nodo = nodo->enlace;
 }

 bool anchosCambiados = false;
 int i = config->readIntValue(CONFIG_NOTIFIESLISTCOUNT, 0);
 char key[strlen(CONFIG_NOTIFYNICK) + 31];
 for (int o = 0; o < i; o++)
 {
  // obtengo un nick de los que estan configurados
  sprintf(key, "%s%i", CONFIG_NOTIFYNICK, o);
  char nick[strlen(config->readStrValue(key, "This is a bug, send me an email"))+1];
  strcpy(nick, config->readStrValue(key, "This is a bug, send me an email"));

  // me fijo si est� en la lista, si est� lo marco como existente
  bool existe = false;
  nodoListaNotifies *nodo = lista;
  while (nodo != NULL)
  {
   if (strcasecmp(nick, nodo->nick) == 0)
   {
    // existe, lo marco
    nodo->cambiado = true;
    existe = true; // esta variable es para saber que no tengo que crearlo
    break; // termino el bucle
   }
   nodo = nodo->enlace;
  }

  // si el nodo no exist�a, lo creo
  if (existe == false)
  {
   nodoListaNotifies *nuevoNodo = new nodoListaNotifies;
   nuevoNodo->nick = new char[strlen(nick)+1];
   strcpy(nuevoNodo->nick, nick);
   nuevoNodo->status = Offline; // todav�a no se si est� conectado
   nuevoNodo->cambiado = true; // para que el eliminador no lo quite de la lista
   nuevoNodo->enlace = NULL;

   // no es el primero, tengo que buscar una posici�n para insertarlo
   // retutilizco nodo, definida m�s arriba
   nodo = lista;
   nodoListaNotifies *anterior = NULL;
   while ((nodo != NULL) && (strcasecmp(nuevoNodo->nick, nodo->nick) > 0))
   {
    anterior = nodo;
    nodo = nodo->enlace;
   }
   // si anterior == NULL entonces es el primer elemento de la lista
   if (anterior == NULL)
   {
    lista = nuevoNodo;
   } else {
    anterior->enlace = nuevoNodo;
   }
   nuevoNodo->enlace = nodo;

   // verifico el tema del ancho de las columnas
   if (metrica->width(nuevoNodo->nick) + SEPARACION_COLUMNAS_NOTIFIES > ancho2)
   {
    if (MAXIMO_ANCHO_COLUMNA1_NOTIFIES < metrica->width(nuevoNodo->nick) + SEPARACION_COLUMNAS_NOTIFIES)
    { ancho2 = MAXIMO_ANCHO_COLUMNA1_NOTIFIES;
    } else {
     ancho2 = metrica->width(nuevoNodo->nick) + SEPARACION_COLUMNAS_NOTIFIES;
    }
    anchosCambiados = true;
   }
  }
 }

 // elimino los nodos que fueron eliminados de la lista
 nodo = lista; // declarada m�s arriba
 nodoListaNotifies *anterior = NULL, *aux;
 while (nodo != NULL)
 {
  if (nodo->cambiado == true)
  {
   // sigo de largo
   anterior = nodo;
   nodo = nodo->enlace;
  } else {
   // tengo que eliminarlo
   if (anterior == NULL) {
    lista = nodo->enlace;
   } else {
    anterior->enlace = nodo->enlace;
   }
   aux = nodo;
   nodo = nodo->enlace;
   delete [] aux->nick;
   delete aux;
   // reestablezco la lista para que funcione el siguiente bucle
  }
 }

 // si cambiaron los anchos entonces actualizo el tama�o de la tabla
 if (anchosCambiados == true)
 {
  updateTableSize();

  emit colWidthChange(ancho1, ancho2, ancho3);
 }

 // aviso al qtableview que cambio la cantidad de filas
 if (numRows() != i)
 { setNumRows(i); }

 // esta funci�n no actualiza la pantalla, porque es utilizada por otrar funciones que si lo hacen
}

void baseNotifyShowcase::vaciarLista()
{
 // vacio la lista
 // establezco la cantidad de filas a 0
 setNumRows(0);

 // vacio la lista
 nodoListaNotifies *nodo = lista, *aux;
 while (nodo != NULL)
 {
  aux = nodo->enlace;
  delete [] nodo->nick;
  delete nodo;

  nodo = aux;
 }
 lista = NULL;
}


void baseNotifyShowcase::mousePressEvent(QMouseEvent *e)
{
 int seleccionado = findRow(e->x());

 // emito la se�al de bot�n derecho o izquierdo
 if (seleccionado < 0) return; // no se presion� sobre ning�n canal
 switch (e->button())
 {
  case RightButton : emit rightButtonPressed(e->globalPos()); break;
  case LeftButton : emit leftButtonPressed(e->globalPos()); break;

#ifdef ENABLE_QT2
  default: break; // por compatibilidad con QT 2.1.0
#endif

 }
}

void baseNotifyShowcase::mouseDoubleClickEvent(QMouseEvent *e)
{
 int seleccionado = findRow(e->y());
 if (seleccionado >= 0) emit doubleClicked(e->globalPos());
}

void baseNotifyShowcase::paintCell ( QPainter *p, int row, int col )
{
 // busco el nodo que tengo que dibujar
 nodoListaNotifies *nodo = lista;
 int o = 0;
 while ((nodo != NULL) && (o != row))
 {
  nodo = nodo->enlace;
  o++;
 }
 // por si las dudas, si llegu� al final de la lista sin encontr�r el nodo me borro
 if (nodo == NULL)
 {
  return;
 }

 switch(nodo->status) {
  case Online					: p->setPen(darkGreen); break;
  case JustConnected	: p->setPen(red); break;
  case Offline				: p->setPen(darkMagenta); break;
 }

 int x, y;
 // ahora me fijo que dato de ese me tengo que fijar
 switch(col) {
  case 0 : // dibujo el �cono correspondiente
           x = (int)((ancho1 - 16)/2);
           y = (int)((cellHeight() - 16)/2);
           switch(nodo->status) {
            case Online				: p->drawPixmap(x, y, QPixmap(online_icon)); break;
            case JustConnected	: p->drawPixmap(x, y, QPixmap(justconn_icon)); break;
            case Offline				: p->drawPixmap(x, y, QPixmap(offline_icon)); break;
           }
           break;
  case 1 : // me fijo si lo puedo mostrar entero, o si tengo que recortar el nombre
           if (metrica->width(nodo->nick) + SEPARACION_COLUMNAS_NOTIFIES > ancho2)
           {// no entra
            int i = 0; while (metrica->width(nodo->nick, i) + SEPARACION_COLUMNAS_NOTIFIES < ancho2) {i++;};
            p->drawText(0, metrica->ascent(), nodo->nick, i-1); // porque el loop me deja exedido por una letra
           } else {
            p->drawText(ancho2 - metrica->width(nodo->nick) - SEPARACION_COLUMNAS_NOTIFIES, metrica->ascent(), nodo->nick);
           }
           break;
  case 2 : switch(nodo->status) {
            case Online					: p->drawText((int)(ancho3 - metrica->width("Online"))/2, metrica->ascent(), "Online");
                                  break;
            case JustConnected	: p->drawText((int)(ancho3 - metrica->width("Just Connected"))/2, metrica->ascent(), "Just Connected");
                                  break;
            case Offline				: p->drawText((int)(ancho3 - metrica->width("Offlline"))/2, metrica->ascent(), "Offline");
                                  break;
           }
           break;
 }
}


int baseNotifyShowcase::cellWidth(int c)
{
 switch(c) {
  case 0  : return ancho1; break;
  case 1  : return ancho2; break;
  default : return ancho3; break;
 }
 return 0;
}

int baseNotifyShowcase::totalWidth()
{
 return (ancho1 + ancho2 + ancho3);
}

notifyShowcase::notifyShowcase(QWidget *parent, const char *name = 0) : QWidget(parent, name)
{
 lista = new baseNotifyShowcase(this);

 header = new QHeader(this);
 header->setOrientation(QHeader::Horizontal);
 header->addLabel("", lista->colWidth(0));
 header->addLabel("Nick", lista->colWidth(1));
 header->addLabel("Status", lista->colWidth(2));
 header->setResizeEnabled(false);
 header->setClickEnabled(false);

 header->setGeometry(0, 0, width(), (header->sizeHint()).height());
 lista->setGeometry(0, (header->sizeHint()).height(), width(), height() - (header->sizeHint()).height());

 connect(lista, SIGNAL(colWidthChange(int, int, int)), SLOT(colWidthChange(int, int, int)));

 // tralado se�ales
 connect(lista, SIGNAL(rightButtonPressed(const QPoint)), SIGNAL(rightButtonPressed(const QPoint)));
 connect(lista, SIGNAL(leftButtonPressed(const QPoint)), SIGNAL(leftButtonPressed(const QPoint)));
 connect(lista, SIGNAL(doubleClicked(const QPoint)), SIGNAL(doubleClicked(const QPoint)));
}

void notifyShowcase::colWidthChange(int a, int b, int c)
{
 header->setCellSize(0, a);
 header->setCellSize(1, b);
 header->setCellSize(2, c);

 header->update();
}

void notifyShowcase::resizeEvent(QResizeEvent *e)
{
 header->setGeometry(0, 0, width(), (header->sizeHint()).height());
 lista->setGeometry(0, (header->sizeHint()).height(), width(), height() - (header->sizeHint()).height());
}

#undef Online
#undef Offline
#undef JustConnected

