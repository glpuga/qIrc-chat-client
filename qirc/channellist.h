/***************************************************************************
                          channellist.h  -  description
                             -------------------
    begin                : Thu Dec 7 2000
    copyright            : (C) 2000 by Gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CHANNELLIST_H
#define CHANNELLIST_H

#include <qtableview.h>
#include <qfontmetrics.h>
#include <qwidget.h>
#include <qpainter.h>
#include <qheader.h>
#include <string.h>
#include <stdio.h>
#include <qkeycode.h>

#include "general.h"
#include "resource.h"

#define MAXIMO_ANCHO_COLUMNA1_CANALES		300
#define SEPARACION_COLUMNAS_CANALES			10

struct nodoColaCanales {
 char *channel;
 int users;
 char *topic;

 nodoColaCanales *enlace; // apunta al siguiente elemento
};

struct nodoListaSegmentosTopic
{
 int x; // las posiciones relativas horizontales
 int background, foreground; // est� guardado en colores del mirc (enteros del 0 al 15)
 bool bold, italic, underline;
 char *texto;

 nodoListaSegmentosTopic *enlace;
};

struct nodoListaCanales {
 char *channel;
 int users;
 nodoListaSegmentosTopic *segmentosTopic;

 nodoListaCanales *enlace; // apunta al siguiente elemento
};

class baseChannelList : public QTableView
{
 Q_OBJECT

private:
 // la info de la font
 QFontMetrics *metrica;

 // las variables que manejan la cola
 nodoColaCanales *primero, *ultimo;

 // las que manejan la lista de los que se muestran en la lista
 nodoListaCanales *lista;
 int cantidadCanales;

 // los anchos de las columnas
 int ancho1, ancho2, ancho3;

 // indica que fila est� seleccionada, si es negativa no hay selecci�n
 int seleccionado;

 // marca que forma de ordenatimento tiene la lista
 enum sortConst {unsorted, sortedByChannel, sortedByusers} orden;

public:
 baseChannelList(QWidget *parent, const char *name = 0);
 ~baseChannelList();

 void insertChannel(const char *channel, int users, const char *topic);
 void clear();

 int colWidth(int col) { return cellWidth(col); }

 void setColWidht(int col, int width); // esta es experimental

 const char *selectedChannel(); // si no hay seleccion devuelve null;

public slots:
 void sortByChannel() { sortBy(sortedByChannel); }
 void sortByusers() { sortBy(sortedByusers); }

private:
 void sortBy(sortConst s);

 int cellWidth(int c);
 int totalWidth();

 void timerEvent(QTimerEvent *e);
 void paintCell ( QPainter *, int row, int col );

 void mousePressEvent(QMouseEvent *e);
 void mouseDoubleClickEvent(QMouseEvent *e);
 void keyPressEvent(QKeyEvent *e);
 void focusInEvent(QFocusEvent *e) { repaint(false); }
 void focusOutEvent(QFocusEvent *e) { repaint(false); }

 nodoListaSegmentosTopic *segmentarTopic(const char *topic);

signals:
 void rightButtonPressed(const QPoint p);
 void leftButtonPressed(const QPoint p);
 void doubleClicked(const QPoint p);

 void colWidthChange(int, int, int);
};


class channelList : public QWidget
{
 Q_OBJECT

private:
 QHeader *header;
 baseChannelList *lista;

public:
 channelList(QWidget *parent, const char *name = 0);

 void insertChannel(const char *channel, int users, const char *topic) {lista->insertChannel(channel, users, topic);}
 void clear() { lista->clear(); }

 const char *selectedChannel() { return lista->selectedChannel(); }

private:
 void resizeEvent(QResizeEvent *e);

private slots:
 void colWidthChange(int a, int b, int c);

 void colClicked(int c);

signals:
 void rightButtonPressed(const QPoint p);
 void leftButtonPressed(const QPoint p);
 void doubleClicked(const QPoint p);
};





#endif
