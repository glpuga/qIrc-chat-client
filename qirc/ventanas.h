/***************************************************************************
                          ventanas.h  -  description
                             -------------------
    begin                : Wed Sep 13 2000
    copyright            : (C) 2000 by gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#ifndef VENTANAS_H
#define VENTANAS_H

// SOLO ACTIVAR PARA TESTEO
// #define DEBUG_VENTANACHAT

#include <qlistbox.h>
#include <qwidget.h>
#include <qlineedit.h>
#include <qevent.h>
#include <qfont.h>
#include <qstring.h>
#include <string.h>
#include <qlabel.h>
#include <qpopupmenu.h>
#include <qmessagebox.h>
#include <qkeycode.h>
#include <qcolor.h>
#include <qdatetime.h>
#include <stdio.h>

#include "IRCCodes.h"
#include "resource.h"
#include "Config.h"
#include "ChatView.h"
#include "MainTab.h"
#include "listanicks.h"
#include "general.h"

#ifdef DEBUG_VENTANACHAT
 #include <iostream.h>
#endif

// esta es una clase auxiliar para crear una linea de entrada a medida
// lo unico que hago es sobrecargar la funci�n keyPressEvent
#define MaximoMemoria 10

// los colores de fondo y frente por defecto
#define FORECOLOR			black
#define BACKCOLOR			white

class newLineEdit : public QLineEdit
{
private:
 int actual;
 int cantidad; // es a la vez la cantidad de elementos en tabla y el siguiente elemento a ser escrito

 // por ahora uso una estructura fija, no necesito m�s
 char *tabla[MaximoMemoria]; // una tabla de 10 punteros a char

public:
 newLineEdit(QWidget *parent, const char *name = NULL);
 ~newLineEdit();
 void keyPressEvent(QKeyEvent *e);
};

class VentanaChat : public QWidget
{
 Q_OBJECT

 // este widget tiene dos formas que se seleccionan al crear una instancia
 // estas formas son la de ventanas de chat con y sin barra de nombres

private:
 // los widgets que forman parte de este widget
 ChatView		 		*visor;
 newLineEdit    *entrada;
 ListaNicks     *lista;
 QLabel					*tituloLista;

 // el popup de la ventana
 QPopupMenu			*popupVentana, *popupLista;

 // la font
 QFont *font;

 // la variable que lleva cuenta de si ya se llen� la lista de nombres de determinada ventana
 // ( para que sucesivos NAMES no vuelvan a cargar la lista y aparezcan en la ventana de estado )
 bool listaLlena;
 bool tipoCanal;
 bool habilitada;

 // el nombre de la ventana (si existe)
 char *nombre;

 // estas variables las uso para llevar cuenta de las repeticiones de lineas
 QString lastIdentifier, lastMessage;
 int linesCount;

 // para loguear
 FILE *logFile;

 int winId;

private:
 void comenzarLogging();
 void terminarLogging();
 void log(const char *identificador, const char *mensaje);

 void crearControles(bool tipoCanal);

public:
 VentanaChat(QWidget *parent, const char *nombre = NULL, bool tipoCanal = false);
  ~VentanaChat();

 void escribirLinea(const char *identificador, const char *mensaje, int color = DEFAULT_FOREGROUND);
 void clear();

 void agregarNick(const char *nick); // agrega un nick individual (JOIN)
 void agregarListaNicks(const char *listaNicks); // agrega una lista (si listaLlena es false)
 void finListaNicks() { listaLlena = true; } // pone a false listaLlena

 void cambiarNick(const char *nick, const char *nuevoNick);
 void quitarNick(const char *nick); // elimina un nick de la lista (PART, QUIT, KICK, etc)
 bool existeNick(const char *nick); // averigua si determinado nick est� en el canal
 void setNickAsChanop(const char *nick);
 void unsetNickAsChanop(const char *nick);

 bool esTipoCanal() { return tipoCanal; }
 bool listaNicksIngresada() { return listaLlena; }
 bool ventanaHabilitada() { return habilitada; }
 void deshabilitarVentana() { habilitada = false; }
 void partChannel(); // usada para emitir el /part al cerrar la ventana

 void resetearVentana();

protected:
 void resizeEvent(QResizeEvent *e);
 void actualizarTituloLista();

protected slots:
 void nuevaLinea();
 void mostrarPopupVentana(const QPoint coordenada);
 void mostrarPopupLista(QPoint globalPos, const char *item);

signals:
 void lineaDisponible(const char *ventana, const char *mensaje);
 void nickDoubleClicked(const char *nick);

 void ventanaActiva(VentanaChat *v);

 // esta se�al es para retransmitir la del chatView
 void rightButtonPressed(const QPoint);
};


struct tipoNodoVentana
{
 char *nombre;
 int id; // para uso interno del manejador en coordinaci�n con el newTab
 VentanaChat *ventana;

 tipoNodoVentana *enlace;
};

class ManejadorVentanas : public QObject
{
 Q_OBJECT
 // esta clase maneja una colecci�n de ventanas creandolas y borr�ndolas cuando sea necesario

private:
 MainTab *tab; // el tab que est� en la ventana principal, se recibe como parametro

 tipoNodoVentana *lista;
 int cantidad;

public:
 ManejadorVentanas(MainTab *tab);
 ~ManejadorVentanas();

 int cantidadVentanas() { return cantidad; } // devuelve la cantidad de ventanas en la lista
 VentanaChat *crearVentana(const char *nombre, bool tipoCanal); // crea una nueva ventana
 VentanaChat *punteroVentana(const char *nombre); // devuelve el puntero a una ventana en base a su nombre, o NULL si no existe
 VentanaChat *punteroVentana(int index); // devuelve el puntero a una ventana en base a su index, o NULL si no existe
 void vaciar(); // se asegura de quitar de memoria todo lo que quede (evita problemas con los logs en ventana chat
 bool existeVentana(const char *nombre);

public slots:
 void cerrarVentana(int id); // cierra la ventana indicada por el newTab
 void doubleClickNick(const char *nick);

 void ventanaActiva(VentanaChat *v);

signals:
 void lineaDisponible(const char *ventana, const char *mensaje);
};

#endif