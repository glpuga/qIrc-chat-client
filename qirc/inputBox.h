/***************************************************************************
                          inputBox.h  -  description
                             -------------------
    begin                : Thu Oct 5 2000
    copyright            : (C) 2000 by gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef INPUTBOX_H
#define INPUTBOX_H

#include <qwidget.h>
#include <qpushbutton.h>
#include <qlineedit.h>
#include <qlabel.h>

#include "resource.h"

#define ANCHO_INPUTBOX 600
#define ALTO_INPUTBOX  2 * MARCO + 2 * DISTANCIA + ALTO_LABEL + ALTO_ENTRADA + ALTO_BOTON

class inputBox : public QWidget
{
 Q_OBJECT
// una ventana de entrada no modal ( no hereda a QDialog )
private:
 QLabel *texto;
 QLineEdit *entrada;
 QPushButton *aceptar, *cancelar;

 void closeEvent(QCloseEvent *e) { e->ignore(); }; // para que no se pueda cerrar por la cruz

public:
 inputBox(const char *texto, const char *textoPorDef = NULL, int maxLen = -1);
 ~inputBox();

private slots:
 void clickAceptar();
 void clickCancelar();

signals:
 void resultado(const char *); // devuelve el contenido del lineedit o NULL si se cancela
};

#endif