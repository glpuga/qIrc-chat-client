/***************************************************************************
                          notifyshowcase.h  -  description
                             -------------------
    begin                : Thu Dec 7 2000
    copyright            : (C) 2000 by Gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef NOTIFYSHOWCASE_H
#define NOTIFYSHOWCASE_H

#include <qtableview.h>
#include <qfontmetrics.h>
#include <qwidget.h>
#include <qpainter.h>
#include <qheader.h>
#include <qpixmap.h>
#include <string.h>
#include <stdio.h>

#include "general.h"
#include "Config.h"
#include "resource.h"

#define MAXIMO_ANCHO_COLUMNA1_NOTIFIES	150
#define SEPARACION_COLUMNAS_NOTIFIES		10

struct nodoListaNotifies {
 char *nick;
 short status;
 bool cambiado; // usado con prop�sitos m�ltiples

 nodoListaNotifies *enlace; // apunta al siguiente elemento
};

class baseNotifyShowcase : public QTableView
{
 Q_OBJECT

private:
 // la info de la font
 QFontMetrics *metrica;

 // las que manejan la lista de los que se muestran en la lista
 nodoListaNotifies *lista;

 // los anchos de las columnas
 int ancho1, ancho2, ancho3;

 // la bandera de reset
 bool nextTimeReset;

 // indica si hubo nuevos nicks desde la �ltima actualizaci�n
 bool hayNuevosNicks;

public:
 baseNotifyShowcase(QWidget *parent, const char *name = 0);
 ~baseNotifyShowcase();

 void reset();
 void isonReply(const char *reply);
 bool notifiesChanged() { bool aux = hayNuevosNicks; hayNuevosNicks = false; return aux; }

 int colWidth(int c) { return cellWidth(c); }

private:
 void actualizarLista();
 void vaciarLista();

 int cellWidth(int c);
 int totalWidth();

 void paintCell ( QPainter *, int row, int col );

 void mousePressEvent(QMouseEvent *e);
 void mouseDoubleClickEvent(QMouseEvent *e);

signals:
 void rightButtonPressed(const QPoint p);
 void leftButtonPressed(const QPoint p);
 void doubleClicked(const QPoint p);

 void colWidthChange(int, int, int);
};

class notifyShowcase : public QWidget
{
 Q_OBJECT

private:
 QHeader *header;
 baseNotifyShowcase *lista;

public:
 notifyShowcase(QWidget *parent, const char *name = 0);

 void reset() { lista->reset(); }
 void isonReply(const char *reply) {lista->isonReply(reply);}
 bool notifiesChanged() { return lista->notifiesChanged(); }

private:
 void resizeEvent(QResizeEvent *e);

private slots:
 void colWidthChange(int a, int b, int c);

signals:
 void rightButtonPressed(const QPoint p);
 void leftButtonPressed(const QPoint p);
 void doubleClicked(const QPoint p);
};

#endif
