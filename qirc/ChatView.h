/***************************************************************************
                          chatview.h  -  description
                             -------------------
    begin                : vie oct 20 13:41:53 ARST 2000
    copyright            : (C) 2000 by gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CHATVIEW_H
#define CHATVIEW_H

// only in the DEBUG stage
// #define DEBUG_CHATVIEW

// include files for QT
#include <qtableview.h>
#include <qwidget.h>
#include <qscrollbar.h>
#include <qfontmetrics.h>
#include <qpainter.h>
#include <qcolor.h>

// standard C headers
#include <string.h>
#include <stdlib.h>

#ifdef DEBUG_CHATVIEW
 #include <iostream.h>
#endif

// application specific includes
#include "resource.h"
#include "general.h"
#include "colamensajes.h"

#define MARCOH 				6
#define MARCOV 				1
#define SEPARACION 		8
#define ANCHO_MINIMO 	50

#define TIMER_INTERVAL 100

struct nodoListaSegmentos
{
 int x, y; // las posiciones relativas
 int background, foreground; // est� guardado en colores del mirc (enteros del 0 al 15)
 bool bold, italic, underline;
 char *texto;

 nodoListaSegmentos *enlace;
};

struct nodoLista
{
 int fila;

 char *identificador;
 char *mensajeOriginal;
 int color; // el color foreground por defecto, no confundir
 int alto;

 nodoListaSegmentos *listaSegmentos;
 nodoLista *enlace;
};

class ChatView : public QTableView
{
  Q_OBJECT

private:
 // variables de manejo de la lista de filas
 int cantidad; // cantidad de nodos en la lista
 nodoLista *lista;

 QFontMetrics *metrica;

 // el ancho total
 int anchoTotal;
 // los anchos de las dos columnas
 int ancho1, ancho2;

 // el justificado del prefijo
 int prefixHorAlignment, prefixVerAlignment;

 // el alto de la tabla (es m�s r�pido que totalHeight())
 int tableHeight;

 // la cola que se encarga de mantener las nuevas lineas hasta ponerlas en pantalla
 colaMensajes *cola;

 // el entero que identifica el timer de la ventana
 int timerId;

 // el color de identificador (prefijo)
 QColor idColor;

 // indica si el ChatView debe seguir mostrando las lineas que se vayan mostrando
 // y el valor de y donde la vista tiene que estar quieta mientras stickyBottom es false
 bool stickyBottom;
 int currentY;

public:
 enum { AlRight, AlLeft, AlCenterH, AlTop, AlBottom, AlCenterV };

 ChatView(QWidget *parent = 0, const char *name = 0);
 ~ChatView();

 void setIdentifierColor(int color) { idColor = colorMirc(color); repaint(); }

 void write(const char *identificador, const char *mensaje, const int color = DEFAULT_FOREGROUND); // no dibuja los mensajes, solo los encola
 void clear();

 void setFont(const QFont &f); // sobrecargada

 void setPrefixAlignment(int h, int v) { prefixHorAlignment = h; prefixVerAlignment = v; repaint(); } // la justificaci�n del prefijo

protected:
 void paintCell ( QPainter *p, int row, int col );
 void timerEvent( QTimerEvent *e ); // el encargado de poner en pantalla todos los mensajes encolados
 void resizeEvent( QResizeEvent *e);
 int cellWidth();
 int cellWidth(int col);
 int cellHeight(int row);

 void setPrefixWidth(int width);
 void ajustarTamanioCeldas();
 void calcularValoresFila(nodoLista *nodo);
 void scrollView();

 nodoLista *buscarFila(int row);

 void mousePressEvent(QMouseEvent *e);

protected slots:
 void scrollValueChanged(int newValue);

signals:
 void rightButtonPressed(const QPoint);
};


#endif


