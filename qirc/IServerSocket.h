/***************************************************************************
                          IServerSocket.h  -  description
                             -------------------
    begin                : Sun Nov 12 2000
    copyright            : (C) 2000 by Gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ISERVERSOCKET_H
#define ISERVERSOCKET_H

// solo durante la depuracion
// #define DEBUG_ISERVERSOCKET

#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>

#ifdef DEBUG_ISERVERSOCKET
 #include <iostream.h>
#endif

#define MAX_HOST_NAME   	200
#define MAX_MENSAJE_ERROR 500

class IServerSocket
{
private:
 int sock;
 bool socketCreado;

 int lastError;
 int lastErrnoValue, lastHErrnoValue; // para saber cual fue el error de bajo nivel que
                                      // ocurri�

 void inicializar();
public:
	IServerSocket();
	~IServerSocket();

 int listenAt(unsigned int puerto, int tamanioCola, bool block = true);

 int getSocket();
 int accept();

 int getLastError() { return lastError; }
 const char *strError();

 void close();
};

#endif

