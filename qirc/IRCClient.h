/***************************************************************************
                          IRCClient.h  -  description
                             -------------------
    begin                : Sun Sep 17 2000
    copyright            : (C) 2000 by gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef IRCCLIENT_H
#define IRCCLIENT_H

// para funcionar en modo debug
// #define IRCCLIENT_DEBUG_MODE

#include <qobject.h>
#include <qsocketnotifier.h>
#include <stdarg.h>

#ifdef IRCCLIENT_DEBUG_MODE
 #include <iostream.h>
#endif

#include "IClientSocket.h"

#include "IRCCodes.h"

// Constantes de datos
#define MAX_LINE_LEN 512
#define PARAM_MAX_COUNT 15

// Constantes de caracteres
#define CHR_COLON ':'
#define CHR_SPACE ' '
#define CHR_ZERO '\0'

// constantes de cadena
#define CRLF "\r\n"

#define DIGITS	"0123456789"

class IRCEvent
{
private:
 int codeNumber;
 bool numeric;

 QString nick, user, host;
 QString command;
 int  paramCount;
 QString param[PARAM_MAX_COUNT];

protected:
 int skipSpaces(const char *linea, int &index);
 QString getStringToken(const char *linea, int &index);
 QString getStringToken(QString linea, int &index);

public:
 IRCEvent(const char * serverMessage);

 bool isNumeric() { return numeric; } // si es comando o n�mero de respuesta

 QString getNick() { return nick; }
 QString getUser() { return user; }
 QString getHost() { return host; }
 QString getCommand() { return command; }
 int getNumeric(); // si es un comando devuelve -1 (error, dumb!)

 int getParamCount() { return paramCount; }
 QString getParam(int index); // index de 0 a 14
};

class IRCClient : public QObject
{
 Q_OBJECT

private:
 bool conectado;
 bool logged;
 int testedNicks;

 char linea[MAX_LINE_LEN+1]; // 513 ( para darle espacio al 0 )
 int longitud;
 IClientSocket *s;

 QSocketNotifier *lectura, *escritura;

 QString nick1, nick2, pass;
 QString *nickInUse, user, realName;

 bool automaticPong;

private:
 void tratarLinea();

public:
 IRCClient(bool autoPong = true);
 ~IRCClient();

 int getSocket();

 bool setAutoPong(bool aP);

 void conectar(const char *server, int puerto, const char *nick1, const char * nick2, const char * user, const char * realName, const char *pass, int flags);
 void desconectar(const char *razon = 0);
 void terminarConexion();

 void enviarLinea(const char *linea); // env�a una linea en crudo
 void enviarComando(int cantidad, const char *comando, ...); // numero variable de comandos

protected slots:
 void socketActivoLectura(int);
 void socketActivoEscritura(int);

signals:
 void conexionCompleta(); // se da cuando se completa la conexi�n
 void estatusConexion(const char *mensaje);
 void loginCompleto(const char * nick);
 void desconexion();

 void event(IRCEvent *ircEvent);

 //unknown commands
 void noReconocido(IRCEvent *e);

 // all the reply codes use this one
 void replyCode(IRCEvent *);

 // in the same order as seen on tv!... sorry, on the RFC  :)
 void nick(const char *nick, const char *nuevoNick) ;
 void quit(const char *nick, const char *razon);

 void join(const char *nick, const char *canal);
 void part(const char *nick, const char *canal, const char *mensaje);
 void mode(IRCEvent *evento);
 void topic(const char *nick, const char *canal, const char *topic);
 void invite(const char *nick, const char *canal);
 void kick(const char *nick, const char *canal, const char *echado, const char *razon);

 void privmsg(const char *nick, const char *destino, const char *mensaje);
 void notice(const char *nick, const char *destino, const char *mensaje);

 void ping(const char *server);
 void error(const char *mensaje);
};

#endif