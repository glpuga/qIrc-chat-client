/***************************************************************************
                          general.h  -  description
                             -------------------
    begin                : Sat Nov 11 2000
    copyright            : (C) 2000 by Gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef GENERAL_H
#define GENERAL_H

// quitar esto
// #define DEBUG_FILTRO_MIRC

// solo para la etapa de debug
// #define DEBUG_GENERAL

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <qcolor.h>
#include <sys/ioctl.h>
#include <linux/kd.h>

#ifdef DEBUG_FILTRO_MIRC
 #include <iostream.h>
#endif

#include "resource.h"

#ifdef DEBUG_GENERAL
 #include <iostream.h>
#endif

// funciones de uso variado que no estar relacionadas con ninguna clase
// en particular
char *obtenerUser();
char *obtenerHomePath();
void crearCarpetaLogs();
void filtrarMirc(char *linea);
QColor colorMirc(int iColor);
bool validNickname(const char *nick);

#endif