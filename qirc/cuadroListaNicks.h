/***************************************************************************
                          cuadroListaNicks.h  -  description
                             -------------------
    begin                : Tue Oct 17 2000
    copyright            : (C) 2000 by gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CUADROLISTANICKS_H
#define CUADROLISTANICKS_H

#include <qwidget.h>
#include <qlistbox.h>
#include <qpushbutton.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <string.h>
#include <qmessagebox.h>

#include "Config.h"
#include "resource.h"

class cuadroListaNicks : public QWidget
{
 Q_OBJECT

private:
 QLabel *label;
 QLineEdit *linea;
 QListBox *lista;
 QPushButton *agregar, *quitar, *aceptar;

 char *counterKey, *dataKeyPreffix;

public:
 cuadroListaNicks(const char *caption, const char *text, const char *counterKey, const char *dataKeyPreffix);
 ~cuadroListaNicks();

protected:
 void closeEvent(QCloseEvent *e) {e->ignore(); };

protected slots:
 void bAgregar();
 void bQuitar();
 void bAceptar();

signals:
 void terminado();
};

#endif