/***************************************************************************
                          IServerSocket.cpp  -  description
                             -------------------
    begin                : Sun Nov 12 2000
    copyright            : (C) 2000 by Gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "IServerSocket.h"

#define NOERROR 					0
#define ERROR_NOSOCK    	1
#define ERROR_NOHOSTNAME  2
#define ERROR_NOHOSTIP    3
#define ERROR_NOBIND			4
#define ERROR_NOFCNTL			5
#define ERROR_NOLISTEN    6
#define ERROR_NOACCEPT		7

char ISERVERSOCKETmensajeError[MAX_MENSAJE_ERROR + 1];

const char *TABLA_STRINGS_ERRORES[]  = {"Everything is Ok, boss",
                                        "Can't create a socket [socket()]",
                                        "Can't get the local host name [hostname()]",
                                        "Can't resolv local host name [hostbyname()]",
																				"Can't bind local address to the socket [bind()]",
                                        "Can't socket no-blocking [fcntl()]",
                                        "Can't change socket queue lenght [listen()]",
                                        "Can't accept the connection! [accept()]"};

void IServerSocket::inicializar()
{
 sock 				= -1;
 socketCreado = false;
 lastError 		= NOERROR;
}

IServerSocket::IServerSocket()
{
 #ifdef DEBUG_ISERVERSOCKET
  cout << "IServerSocket::IServerSocket(int, bool)" << endl;
 #endif

 inicializar();
}

int IServerSocket::listenAt(unsigned int puerto, int tamanioCola, bool block = true)
{
 #ifdef DEBUG_ISERVERSOCKET
  cout << "int IServerSocket::listen(int, bool)" << endl;
 #endif

 if (socketCreado == true)
 {
  // cierro la conexi�n anterior
  close();
 }

 // creo el nuevo socket
 sock = ::socket(AF_INET, SOCK_STREAM, 0);
 if (sock < 0)
 {
  // hubo un error
  socketCreado 	= false;
  lastError 		= ERROR_NOSOCK;
  lastErrnoValue= errno;

  #ifdef DEBUG_ISERVERSOCKET
   cout << "No se pudo crear el socket servidor!" << endl;
  #endif

  return -1;
 } else {
  socketCreado = true;

  // enlazo el socket a una direcci�n local
  // obtengo el nombre de mi m�quina
  char myHostName[MAX_HOST_NAME + 1];
  if (gethostname(myHostName, MAX_HOST_NAME) < 0)
  {
   // hubo un error
   close();
   lastError = ERROR_NOHOSTNAME;
   lastErrnoValue= errno;

   #ifdef DEBUG_ISERVERSOCKET
    cout << "No se pudo averiguar el nombre del host local!" << endl;
   #endif

   return -1;
  }

  #ifdef DEBUG_ISERVERSOCKET
   cout << "El nombre del host es: " << myHostName << endl;
  #endif

  hostent *hostInfo = gethostbyname(myHostName);
  if (hostInfo == NULL)
  {
   // hubo un error
   close();
   lastError = ERROR_NOHOSTIP;
   lastHErrnoValue = h_errno;

   #ifdef DEBUG_ISERVERSOCKET
    cout << "No se pudo averiguar la direcci�n del host local!: " << h_errno << endl;
   #endif

   return -1;
  }
  sockaddr_in localAddr;
  localAddr.sin_family = hostInfo->h_addrtype;
  localAddr.sin_port   = htons(puerto);
  localAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  bzero((void *)localAddr.sin_zero, 8);

  if (bind(sock, (sockaddr *)&localAddr, sizeof(localAddr)) < 0)
  {
   // hubo un error
   close();
   lastError = ERROR_NOBIND;
   lastErrnoValue= errno;

   #ifdef DEBUG_ISERVERSOCKET
    cout << "No se pudo enlazar el socket servidor a una direcci�n!: " << errno << endl;
   #endif

   return -1;
  }
  // si es no bloqueante como tal lo marco
  if (block == false)
  {
   if (fcntl(sock, F_SETFL, O_NONBLOCK) < 0)
   {
    // hubo un error
    close();
    lastError = ERROR_NOFCNTL;
    lastErrnoValue= errno;

    #ifdef DEBUG_ISERVERSOCKET
     cout << "No se hacer no bloqueante al socket servidor!: " << errno << endl;
    #endif

    return -1;
   }
  }

  if (::listen(sock, tamanioCola) < 0)
  {
   lastError = ERROR_NOLISTEN;
   lastErrnoValue = errno;
  }
 }
 return 0;
}

void IServerSocket::close()
{
 if (socketCreado == true)
 {
  ::close(sock);
  socketCreado = false;
 }
}

int IServerSocket::getSocket()
{
 return sock;
}

int IServerSocket::accept()
{
 int ret = ::accept(sock, NULL, NULL);
 if (ret < 0)
 {
  lastError = ERROR_NOACCEPT;
  lastErrnoValue = errno;
 }
 return ret;
}

IServerSocket::~IServerSocket()
{
 if (socketCreado == true)
 {
  close();
 }
}

const char *IServerSocket::strError()
{
 switch (lastError)
 {
   case NOERROR        : snprintf(ISERVERSOCKETmensajeError, MAX_MENSAJE_ERROR, "%s", TABLA_STRINGS_ERRORES[lastError]);
												 break;
   case ERROR_NOHOSTIP : snprintf(ISERVERSOCKETmensajeError, MAX_MENSAJE_ERROR, "%s - h_errno: %i", TABLA_STRINGS_ERRORES[lastError], lastHErrnoValue);
												 break;
   default						 : snprintf(ISERVERSOCKETmensajeError, MAX_MENSAJE_ERROR, "%s - %s", TABLA_STRINGS_ERRORES[lastError], ::strerror(lastErrnoValue));
												 break;
 }

 return ISERVERSOCKETmensajeError;
}
