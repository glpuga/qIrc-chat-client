/***************************************************************************
                          programcommand.h  -  description
                             -------------------
    begin                : Tue Dec 26 2000
    copyright            : (C) 2000 by Gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef PROGRAMCOMMAND_H
#define PROGRAMCOMMAND_H

#define MAX_COMMANDS	20

// #define DEBUG_PROGRAMCOMMAND

#include <string.h>

#ifdef DEBUG_PROGRAMCOMMAND
 #include <iostream.h>
#endif

class ProgramCommand
{
private:
 int indices[MAX_COMMANDS]; // el indice 0 es el que indica d�nde est� el comando, los dem�s son los parametros

 char *copiaOriginal, *copiaModificada;
 int cantParametros;
public: 
	ProgramCommand(const char *linea);
	~ProgramCommand();

 void setCommand(const char *linea);
 const char *getCommand();
 const char *getParam(int param);
 const char *getFinalParam(int param);
 int paramCount() {  return cantParametros; }

private:
 const char *getToken(int param, bool ultimo = false);
};

#endif
