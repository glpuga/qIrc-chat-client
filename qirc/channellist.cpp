/***************************************************************************
                          channellist.cpp  -  description
                             -------------------
    begin                : Thu Dec 7 2000
    copyright            : (C) 2000 by Gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "channellist.h"

baseChannelList::baseChannelList(QWidget *parent, const char *name = 0) : QTableView(parent, name)
{
 // ordenado por nada
 orden = sortedByChannel;

 // la cola est� vacia
 primero = ultimo = NULL;

 // la lista tambi�n
 lista = NULL;
 cantidadCanales = 0;

 // no hay ninguno seleccionado
 seleccionado = -1;

 // inicialmente los anchos de las tres columnas son cero
 ancho1 = 70;
 ancho2 = 50;
 ancho3 = 70;

 // obtengo los datos de ancho y alto de la font que est� seteada
 metrica = new QFontMetrics(fontMetrics()); // constructor de copia

 // establezco el color de fondo y el comportamiento del widget
 setBackgroundColor(white);
 setTableFlags(Tbl_autoScrollBars | Tbl_smoothHScrolling);
 setFrameStyle(Panel | Sunken);
 setLineWidth(3);
 setMidLineWidth(2);

 // el ancho de las filas es variable
 setCellWidth(0);
 // el alto es igual al alto de la font
 setCellHeight(metrica->height());

 // le digo cuantas columnas y filas tiene al empezar
 setNumCols(3);
 setNumRows(0);

 // arranco el timer que maneja las actualizaciones en pantalla
 startTimer(700);

 // para poder manejar presiones de teclado...
setFocusPolicy(QWidget::ClickFocus);
}

baseChannelList::~baseChannelList()
{
 delete metrica;

 clear();
}

void baseChannelList::insertChannel(const char *channel, int users, const char *topic)
{
 // encola el canal que hay que agregar a la lista
 nodoColaCanales *nodo = new nodoColaCanales;
 nodo->channel = new char[strlen(channel) + 1];
 strcpy(nodo->channel, channel);
 nodo->users = users;
 nodo->topic = new char[strlen(topic) + 1];
 strcpy(nodo->topic, topic);
 nodo->enlace = NULL;

 if (primero == NULL)
 {
  primero = ultimo = nodo;
 } else {
  ultimo->enlace = nodo;
  ultimo = nodo;
 }
}

void baseChannelList::clear()
{
 // limpio la pantalla

 // establezco la cantidad de filas a 0
 setNumRows(0);
 cantidadCanales = 0;
 seleccionado = -1; // para que no lo dibuje

 // vacio la lista
 nodoListaCanales *nodo = lista, *aux;
 while (nodo != NULL)
 {
  aux = nodo->enlace;
  delete [] nodo->channel;
  nodoListaSegmentosTopic *segmento = nodo->segmentosTopic, *segmentoAux;
  while (segmento != NULL)
  {
   segmentoAux = segmento->enlace;
   delete [] segmento->texto;
   delete segmento;
   segmento = segmentoAux;
  }
  delete nodo;

  nodo = aux;
 }
 lista = NULL;

 // devuelvo los anchos de las columnas a los valores iniciales
 ancho1 = 70;
 ancho2 = 50;
 ancho3 = 70;
}

void baseChannelList::setColWidht(int col, int width)
{
 switch (col) {
  case 0 : ancho1 = width; break;
  case 1 : ancho2 = width; break;
  case 2 : ancho3 = width; break;
 }
 updateTableSize();

 update();
}

const char *baseChannelList::selectedChannel()
{
 if (seleccionado < 0) return NULL;

 nodoListaCanales *nodo = lista;
 int i = 0;
 while (nodo != NULL)
 {
  if (i == seleccionado)
  {
   return nodo->channel;
  }
  nodo = nodo->enlace;
  i++;
 }
 return NULL;
}

void baseChannelList::timerEvent(QTimerEvent *e)
{
 bool anchosCambiados = false;

 // me fijo que haya algo para pasar a la lista
 if (primero == NULL) return; // no hace nada m�s

 // tomo todos los elementos de la cola y los agrego en la lista
 nodoColaCanales *nodoCola = primero, *nodoColaAux;

 while (nodoCola != NULL)
 {
  nodoListaCanales *nuevoNodo = new nodoListaCanales;
  cantidadCanales++;
  nuevoNodo->channel = new char[strlen(nodoCola->channel) + 1];
  strcpy(nuevoNodo->channel, nodoCola->channel);
  nuevoNodo->users = nodoCola->users;
  nuevoNodo->segmentosTopic = segmentarTopic(nodoCola->topic);
  nuevoNodo->enlace = NULL;

  // ahora inserto el nuevo nodo en la lista ordenado seg�n est� especificado
  nodoListaCanales *p = lista, *p2 = NULL;
  if (p == NULL)
  {
   // la lista est� vac�a, as� que lo pongo como nodo inicial
   lista = nuevoNodo;
  } else {
   // la lista tiene algo, as� que la recorro hasta encontrar la posici�n adecuada o llegar al final
   switch (orden) {
    case unsorted : // recorro la lista hasta el final as� lo inserta ah�
                    while (p != NULL)
                    { p2 = p; p = p->enlace; }
                    break;
    case sortedByChannel :
                    while ((p != NULL) && (strcasecmp(p->channel, nuevoNodo->channel) < 0))
                    { p2 = p; p = p->enlace; }
                    break;
    case sortedByusers :
                    while ((p != NULL) && (p->users > nuevoNodo->users))
                    { p2 = p; p = p->enlace; }
                    break;
   }

   // si p2 == null, entonces tengo que insertarlo al principio de la lista
   if (p2 == NULL)
   {
    nuevoNodo->enlace = lista;
    lista = nuevoNodo;
   } else {
    // lo coloco a continuaci�n de p2, y apuntando a p
    nuevoNodo->enlace = p;
    p2->enlace = nuevoNodo;
   }
   // done, ya est� insertado
  }

  // me fijo si no tengo que cambiar el ancho de las columnas
  if (metrica->width(nuevoNodo->channel) + SEPARACION_COLUMNAS_CANALES > ancho1)
  { ancho1 = metrica->width(nuevoNodo->channel) + SEPARACION_COLUMNAS_CANALES; anchosCambiados = true;
   if (ancho1 > MAXIMO_ANCHO_COLUMNA1_CANALES) ancho1 = MAXIMO_ANCHO_COLUMNA1_CANALES; // para que no pueda ocupar m�s de lo debido en pantalla
  }
  char numero[31];
  snprintf(numero, 30, "%i", nuevoNodo->users);
  if (metrica->width(numero) + SEPARACION_COLUMNAS_CANALES > ancho2)
  { ancho2 = metrica->width(numero)  + SEPARACION_COLUMNAS_CANALES; anchosCambiados = true; }
  if (metrica->width(nodoCola->topic) + SEPARACION_COLUMNAS_CANALES > ancho3)
  { ancho3 = metrica->width(nodoCola->topic) + SEPARACION_COLUMNAS_CANALES; anchosCambiados = true; }

  // elimino el nodo de la cola que ya copi�  a la lista
  nodoColaAux = nodoCola->enlace;
  delete [] nodoCola->channel;
  delete [] nodoCola->topic;
  delete nodoCola;

  // paso al siguiente elemento de la cola
  nodoCola = nodoColaAux;
 }

 // la cola est� vac�a, hago que las variables lo reflejen
 primero = ultimo = NULL;

 // me fijo si cambiaron los anchos de las columnas
 if (anchosCambiados == true)
 { emit colWidthChange(ancho1, ancho2, ancho3);
  updateTableSize();
 }

 // termino de actualizar la tabla
 setNumRows(cantidadCanales);
 update(); // estoy cambiando el orden de entrada de la tabla, as� que tengo que repintar todo
}

void baseChannelList::mousePressEvent(QMouseEvent *e)
{
 // selecciona la fila
 int aux = findRow(e->y());

 // evito parpadeos extra
 if (aux != seleccionado)
 {
  int anterior = (seleccionado>cantidadCanales)?(-1):(seleccionado); // para evitar posibles errores, (aunque no as� como est� el programa esto no deber�a pasar)
  seleccionado = aux;
  if (anterior >= 0) { for (int i = 0; i < 3; i++)  { updateCell(anterior, i); } }
  if (seleccionado >= 0) { for (int i = 0; i < 3; i++) { updateCell(seleccionado, i); } }
 }

 // emito la se�al de bot�n derecho o izquierdo
 if (seleccionado < 0) return; // no se presion� sobre ning�n canal
 switch (e->button())
 {
  case RightButton : emit rightButtonPressed(e->globalPos()); break;
  case LeftButton : emit leftButtonPressed(e->globalPos()); break;
#ifdef ENABLE_QT2
  default: break; // por compatibilidad con QT 2.1.0
#endif
 }
}

void baseChannelList::mouseDoubleClickEvent(QMouseEvent *e)
{
 if (seleccionado >= 0) emit doubleClicked(e->globalPos());
}

void baseChannelList::keyPressEvent(QKeyEvent *e)
{
 if ((seleccionado < 0) || (seleccionado >= cantidadCanales)) return; // no est� marcado ning�n canal

 int anterior = seleccionado;
 switch(e->key())
 {
  case Key_Left :
  case Key_Up   :// va pa arriba una fila
                 seleccionado = (seleccionado>0)?(seleccionado-1):(0);
                 break;
  case Key_Right:
  case Key_Down :// va pa abajo una fila
                 seleccionado = (seleccionado<cantidadCanales-1)?(seleccionado+1):(cantidadCanales-1);
                 break;
  case Key_PageUp: // arriba 20 lineas
                 seleccionado = (seleccionado>20)?(seleccionado-20):(0);
                 break;
  case Key_PageDown: // abajo 20 lineas
                 seleccionado = (seleccionado<cantidadCanales-21)?(seleccionado+20):(cantidadCanales-1);
                 break;
  case Key_Home : // al primero de la lista
                  seleccionado = 0;
                  break;
  case Key_End  : // al �ltimo de la lista
                  seleccionado = cantidadCanales-1;
  default       :// no se que hacer con la tecla
                 e->ignore(); break;
 }
 // muestro el cambio en pantalla
 if (seleccionado != anterior) // evito cambios al dope
 {
  if (seleccionado >= 0)
  {
   updateCell(seleccionado, 0);
   updateCell(seleccionado, 1);
   updateCell(seleccionado, 2);
  }
  if (anterior >= 0)
  {
   updateCell(anterior, 0);
   updateCell(anterior, 1);
   updateCell(anterior, 2);
  }
 }
 // me aseguro de que sea visible
 if ((seleccionado < topCell()) || (seleccionado > lastRowVisible()))
 {
  setTopCell(seleccionado);
 }
 // listo, chau
}


void baseChannelList::paintCell ( QPainter *p, int row, int col )
{
 // busco el nodo que tengo que dibujar
 nodoListaCanales *nodo = lista;
 int o = 0;
 while ((nodo != NULL) && (o != row))
 {
  nodo = nodo->enlace;
  o++;
 }
 // por si las dudas, si llegu� al final de la lista sin encontr�r el nodo me borro
 if (nodo == NULL)
 {
  return;
 }

 if ((seleccionado == row) && (col == 0))
 {
  p->setBrush(blue);
  p->setBackgroundMode(TransparentMode);
  int ancho;
  switch (col) {
   case 0 : ancho = ancho1; break;
   case 1 : ancho = ancho2; break;
   case 2 : ancho = ancho3; break;
  }
  p->drawRect(0, 0, ancho - SEPARACION_COLUMNAS_CANALES, metrica->height());
  p->setPen(white);
 } else {
  p->setBackgroundMode(OpaqueMode);
  p->setPen(black);
 }

 // ahora me fijo que dato de ese me tengo que fijar
 switch(col) {
  case 0 : // me fijo si lo puedo mostrar entero, o si tengo que recortar el nombre
           if (metrica->width(nodo->channel) + SEPARACION_COLUMNAS_CANALES > ancho1)
           { // no entra
             int i = 0; while (metrica->width(nodo->channel, i) + SEPARACION_COLUMNAS_CANALES < ancho1) {i++;};
             p->drawText(0, metrica->ascent(), nodo->channel, i-1); // porque el loop me deja exedido por una letra
           } else {
            p->drawText(0, metrica->ascent(), nodo->channel);
           }
           break;
  case 1 : char numero[31];
           snprintf(numero, 30, "%i", nodo->users);
           p->drawText(0, metrica->ascent(), numero);
           break;
  case 2 : // voy dibujando uno por uno los segmentos del nodo
           nodoListaSegmentosTopic *segmento = nodo->segmentosTopic;
           while(segmento != NULL)
           {
            QColor color;
            color = colorMirc(segmento->foreground);
            p->setPen(color);
            color = colorMirc(segmento->background);
            p->setBackgroundColor(color);
            p->setBackgroundMode(OpaqueMode);
       	    p->drawText(segmento->x, metrica->ascent(), segmento->texto);
            if (segmento->bold == true)
            {
             // dibujo el mismo texto con un corrimiento de un pixel
             p->setBackgroundMode(TransparentMode);
       	     p->drawText(segmento->x+1, metrica->ascent(), segmento->texto);
            }
            if (segmento->underline == true)
            {
             p->drawLine(segmento->x, metrica->height(), segmento->x + metrica->width(segmento->texto), metrica->height());
            }
            segmento = segmento->enlace;
  				 }
           // vuelvo el color a la normalidad
           p->setBackgroundColor(colorMirc(DEFAULT_BACKGROUND));
 			     break;
 }
}

void baseChannelList::sortBy(sortConst s)
{
 // para que las inserciones se hagan correctamente
 orden = s;

 // ahora reordeno la lista. Recorro la lista desde el principio hasta el ante�ltimo elemento
 // fij�ndome si tengo que intercambiar los nodos. Repito hasta llegar al final de la lista sin
 // hacer ning�n cambio
 // en otras palabras, ordenaci�n de burbuja
 bool cambio;
 nodoListaCanales *puntero, *anterior;
 int ciclos = 0;
 do {
  int ordenarCant = cantidadCanales - ciclos; // para no ordenar la parte que ya est� ordenada
  cambio = false;
  puntero = lista;
  anterior = NULL;
  while ((puntero != NULL) && (ordenarCant > 0))
  {
   ordenarCant--; // con cada ciclo hay un nodo menos que tengo que revisar.

   // me fijo si a este nodo le sigue alg�n otro
   if (puntero->enlace != NULL)
   {
    // hay otro elemento, as� que me fijo si los tengo que intercambiar
    bool intercambiar = false;
    switch (orden) {
     case unsorted				: // ac� nunca hace nada (solo para evitar warnings, nunca deber�a ser unsorted ac�)
                            break;
     case sortedByChannel : if (strcasecmp(puntero->channel, (puntero->enlace)->channel) > 0)
                            { intercambiar = true; }
                            break;
     case sortedByusers   : if (puntero->users < (puntero->enlace)->users)
                            { intercambiar = true; }
                            break;
    }
    if (intercambiar) {
     cambio = true;

     // si anterior es null, este es el primer elemento de la lista
     nodoListaCanales *aux = puntero->enlace;
     puntero->enlace = aux->enlace;
     aux->enlace = puntero;
     if (anterior == NULL) {
      lista = aux;
     } else {
      anterior->enlace = aux;
     }
    }
   }
   anterior = puntero;
   puntero = puntero->enlace;
  }
 } while (cambio == true);

 // redibujo el widget para mostrar los cambios
 update();
}

int baseChannelList::cellWidth(int c)
{
 switch(c) {
  case 0  : return ancho1; break;
  case 1  : return ancho2; break;
  default : return ancho3; break;
 }
 return 0;
}

int baseChannelList::totalWidth()
{
 return (ancho1 + ancho2 + ancho3);
}

nodoListaSegmentosTopic *baseChannelList::segmentarTopic(const char *topic)
{
 // voy segmentando la lista y obtengo valores hasta que cambia el valor de color o
 // estilo, o bien hasta que alcanzo el final de una linea

 nodoListaSegmentosTopic *nodo = NULL;

 // ac� voy a ir acumulando los caracteres de cada segmento
 char linea[strlen(topic)+1];
 int indice = 0;
 char c;
 int foreground = DEFAULT_FOREGROUND;
 int background = DEFAULT_BACKGROUND;
 int xActual;
 bool bold, italic, underline;

 linea[0] = 0;
 xActual = 0; // parto del origen
 bold = italic = underline = false; // por defecto texto limpio
 // es un ciclo do, as� que al finalizar la cadena c vale el caracter de terminaci�n cero
 do {
  c = topic[indice];
  indice++;
  // si cambia el estilo o si llegu� al final de la linea agarro lo que tengo hasta ahora y
  // lo mando a la fila
  if ((c == MIRC_CTRL_B) || (c == MIRC_CTRL_U) || (c == MIRC_CTRL_R) ||
      (c == MIRC_CTRL_O) || (c == MIRC_CTRL_K) || (c == 0))
  {
   // se dio uno de esos casos
   // me fijo si tengo m�s de una letra (para evitar nodos sin texto)
   if (strlen(linea) > 0)
   {
    nodoListaSegmentosTopic *nuevo = new nodoListaSegmentosTopic;
    nuevo->texto = new char[strlen(linea)+1];
    strcpy(nuevo->texto, linea);
    nuevo->foreground = foreground;
    nuevo->background = background;
    nuevo->x 		 = xActual;
    nuevo->bold  = bold;
    nuevo->italic= italic;
    nuevo->underline = underline;
    // lo engancho en la lista
    nuevo->enlace = nodo;
    nodo = nuevo;
   }
   switch(c) {
    case 0           : break; // se termin� la cadena, no hago nada m�s.
    case MIRC_CTRL_B : bold = !bold; break;
    case MIRC_CTRL_U : underline = !underline; break;
    case MIRC_CTRL_R : italic = !italic; break;
    case MIRC_CTRL_O : bold = underline = italic = false; break; // vuelvo todo a cero
    case MIRC_CTRL_K : int contador = 0;
                       char strColor[3];
                       strColor[0] = 0; // es m�s r�pido que llamar a una funcion
                       while ((topic[indice] != 0) && (strchr("0123456789", topic[indice]) != NULL) && (contador < 2))
                       {
                        strncat(strColor, &topic[indice], 1);
                        contador++; indice++;
                       }
                       // si no hab�a ning�n n�mero es que hay que volver los colores a
                       // los valores por defecto
                       if (strlen(strColor) == 0)
                       {
                        foreground = DEFAULT_FOREGROUND;
                        background = DEFAULT_BACKGROUND;
                       } else {
                        // si hab�a n�meros
                        foreground = atoi(strColor);
                        strColor[0] = 0; // es m�s r�pido que llamar a una funcion
                        if (topic[indice] == ',')
  		                 {
       								  indice++;
                         contador = 0;
                         while ((topic[indice] != 0) && (strchr("0123456789", topic[indice]) != NULL) && (contador < 2))
                         {
                          strncat(strColor, &topic[indice], 1);
                          contador++; indice++;
                         }
                         background = atoi(strColor); // si hab�a coma tiene que haber otro n�mero
                        }
                       }
  									  break;

   } // switch()
   xActual += metrica->width(linea);
   linea[0] = 0; // limpio la cadena
  } else {
   // no hay nada en particular en esta letra, as� que simplemente la acumulo
   strncat(linea, &c, 1);
  }
 } while(c != 0); // termin� cuando se haya procesado el cero

 return nodo;
}

channelList::channelList(QWidget *parent, const char *name = 0) : QWidget(parent, name)
{
 lista = new baseChannelList(this);

 header = new QHeader(this);
 header->setOrientation(QHeader::Horizontal);
 header->addLabel("Channel", lista->colWidth(0));
 header->addLabel("Users", lista->colWidth(1));
 header->addLabel("Topic", lista->colWidth(2));
 header->setResizeEnabled(false);
 header->setClickEnabled(false, 2);

 header->setGeometry(0, 0, width(), (header->sizeHint()).height());
 lista->setGeometry(0, (header->sizeHint()).height(), width(), height() - (header->sizeHint()).height());

 connect(lista, SIGNAL(colWidthChange(int, int, int)), SLOT(colWidthChange(int, int, int)));
 connect(header, SIGNAL(sectionClicked(int)), SLOT(colClicked(int)));

 // tralado se�ales
 connect(lista, SIGNAL(rightButtonPressed(const QPoint)), SIGNAL(rightButtonPressed(const QPoint)));
 connect(lista, SIGNAL(leftButtonPressed(const QPoint)), SIGNAL(leftButtonPressed(const QPoint)));
 connect(lista, SIGNAL(doubleClicked(const QPoint)), SIGNAL(doubleClicked(const QPoint)));
}

void channelList::colWidthChange(int a, int b, int c)
{
 header->setCellSize(0, a);
 header->setCellSize(1, b);
 header->setCellSize(2, c);

 header->update();
}

void channelList::resizeEvent(QResizeEvent *e)
{
 header->setGeometry(0, 0, width(), (header->sizeHint()).height());
 lista->setGeometry(0, (header->sizeHint()).height(), width(), height() - (header->sizeHint()).height());
}

void channelList::colClicked(int c)
{
 switch (c) {
  case 0 : lista->sortByChannel(); break;
  case 1 : lista->sortByusers(); break;
 }
}