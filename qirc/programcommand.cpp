/***************************************************************************
                          programcommand.cpp  -  description
                             -------------------
    begin                : Tue Dec 26 2000
    copyright            : (C) 2000 by Gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "programcommand.h"

ProgramCommand::ProgramCommand(const char *linea)
{
 // inicializo los punteros
 copiaOriginal = copiaModificada = NULL;
 // el contador de parametros es igual a cero
 cantParametros = 0;
 // pongo todos los indices a -1 para indicar que no son un puntero v�lido
 for (int i = 0; i < MAX_COMMANDS; i++)
 {
  indices[i] = -1;
 }

 if (linea != NULL) setCommand(linea);
}

ProgramCommand::~ProgramCommand()
{
 // libero la memoria utilizada por los punteros a cadenas
 if (copiaOriginal != NULL) delete [] copiaOriginal;
 if (copiaModificada != NULL) delete [] copiaModificada;
}

void ProgramCommand::setCommand(const char *linea)
{
 // recorro la cadena recolectando los tramos de la cadena, los cuales tienen esta
 // forma:
 // [Command] *MAX_COMMANDS[Parameter]

 // antes que nada hago mi propia copia del comando
 if (copiaOriginal != NULL) delete [] copiaOriginal;
 if (copiaModificada != NULL) delete [] copiaModificada;
 copiaOriginal = new char[strlen(linea)+1];
 copiaModificada = new char[strlen(linea)+1];
 strcpy(copiaOriginal, linea);
 strcpy(copiaModificada, linea);

 // y vac�o la tabla de indices
 for (int i = 0; i < MAX_COMMANDS; i++)
 {
  indices[i] = -1;
 }

 // ahora recorro la copia buscando los trozos
 bool fueEspacio = true;
 int token = 0, indice = 0;
 char c;
 while ((copiaOriginal[indice] != 0) && (token < MAX_COMMANDS))
 {
  c = copiaOriginal[indice]; // solo para ahorrar teclas
  if ((c != 32) && (fueEspacio == true))
  {
   // ac� empieza un nuevo token
   indices[token] = indice;
   // incremento el token
   token++;

   fueEspacio = false;
  }
  if (c == 32)
  {
   fueEspacio = true;
   // en la cadena modificada cambio el espacio por un cero
   copiaModificada[indice] = 0;
  }

  // muevo el indice por la cadena
  indice++;
 }
 // token - 1 = cantidad de parametros siempre que token > 0
 cantParametros = (token > 0)?(token - 1):0;

 #ifdef DEBUG_PROGRAMCOMMAND
  cout << "Tokens en programCommand" << endl;
  for (int i = 0; i < cantParametros +1; i++)
  {
   cout << "* tok. " << i << " - Cat. " << ((i == 0)?"COMMAND  ":"PARAMETER") << " - Index " << indices[i] << " -> [" << getToken(i) << "]" << endl;
  }
  cout << "* Fin Tabla" << endl;
 #endif
}

const char *ProgramCommand::getCommand()
{
 // regreso la cadena apuntada por el indice de la tabla 0

 return getToken(0);
}

const char *ProgramCommand::getParam(int param)
{
 // los parametros est�n a partir del indice 1 de la tabla
 // param 0 - token 1 ; param 1 - token 2; etc...
 return getToken(param+1);
}

const char *ProgramCommand::getFinalParam(int param)
{
 // los parametros est�n a partir del indice 1 de la tabla
 // param 0 - token 1 ; param 1 - token 2; etc...
 // le digo a getToken que regrese todo lo que est� de ese indice en adelante
 return getToken(param+1, true);
}

const char *ProgramCommand::getToken(int token, bool ultimo = false)
{
 // regreso la cadena apuntada por el indice token de la tabla
 // me fijo de que exista
 if ((token < 0) || (token >= MAX_COMMANDS) || (indices[token] == -1)) // no existe ese token
 {
  return ""; // nunca devuelve null
 } else {
  // copio en copiaModificada el token especificado
  if (ultimo == false)
  {
   // regreso la cadena desde copiaOriginal (donde los tokens est�n aislados por ceros)
   return (const char *)(copiaModificada + indices[token]);
  } else {
   // regreso la cadena de copiaOriginal donde los espacios no fueron reemplazados
   return (const char *)(copiaOriginal + indices[token]);
  }
 }
 return NULL; // solo para que no se queje el compilador
}