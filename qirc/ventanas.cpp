/***************************************************************************
                          ventanas.cpp  -  description
                             -------------------
    begin                : Wed Sep 13 2000
    copyright            : (C) 2000 by gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "ventanas.h"

// ** ESTO CORRESPONDE A NEWLINEEDIT **********************************************

newLineEdit::newLineEdit(QWidget *parent, const char *name = NULL) : QLineEdit(parent, name)
{
 // lo �nico que hago es inicializar las variables propias de esta clase
 actual = 0; // esta variable s�lo ayuda a saber en que parte de la lista estoy parado
 cantidad = 0; // esta me dice cuantos elementos hay en la tabla, el m�ximo es 10

 for (int i = 0; i < MaximoMemoria; i++)
 {
  tabla[i] = NULL;
 }
}

newLineEdit::~newLineEdit()
{
 // elimino la memoria utilizada
 for (int i = 0; i < MaximoMemoria; i++)
 {
  if (tabla[i] != NULL) delete [] tabla[i];
 }
}

void newLineEdit::keyPressEvent(QKeyEvent *e)
{
 // si se presion� arriba o abajo reemplazo el contenido del la entrada por el elemento
 // que corresponda de la tabla, si se presion� enter o return lo agrega a la lista y sino
 // hace el tratamiento que corresponda
 char *aux;

 switch (e->key()) {
  case Key_Up: 		if (actual < cantidad-1)
									{ actual++;
									 setText(tabla[actual]);
									}
									break;
  case Key_Down:	if (actual > 0)
									{ actual--;
										setText(tabla[actual]);
									}
									break;
	case Key_Return:
	case Key_Enter:	// lo agrego a la lista
                  if((text() == NULL) || (strlen(text()) == 0)) return;
									aux = new char[strlen(text())+1];
									strcpy(aux, text());
  								// corro tabla una posici�n para arriba
									if (tabla[MaximoMemoria-1] != NULL) delete [] tabla[MaximoMemoria-1];
									for (int i = MaximoMemoria-1; i > 0; i--)
									{
										tabla[i] = tabla[i-1];
									}
									tabla[0] = aux;
									if (cantidad < MaximoMemoria) cantidad++;
									// ya lo agregu�, ahora reinicio actual
									actual = -1; // para que al hacer hacia arriba el primero en mostrarse
															 // sea el elemento 0 de la lista
									// el que no haya puesto break es a prop�sito
	default:				// reemito el evento a su controlador originario
									QLineEdit::keyPressEvent(e);
 }										
}

// ** ESTO CORRESPONDE A VENTANACHAT **********************************************


extern Config *config; // el objeto que contiene la configuraci�n (declarado en tellme.h)

VentanaChat::VentanaChat(QWidget *parent, const char *nombre = NULL, bool tipoCanal = false) : QWidget(parent)
{
 // ************************************
 // REVISAR ESTE CONSTRUCTOR, REESCRIBIR
 // ************************************

 // si recibe nombre, es una ventana de canal o privado,
 // si nombre es null es una ventana de status (como la principal)

 setFocusPolicy(QWidget::StrongFocus);

 // creo los widgets que forman parte de seguro. Si es un canal hago un QSplit que contenga la lista y el Multiline
 entrada = new newLineEdit(this);
 setFocusProxy(entrada);

 visor = new ChatView(this);
 visor->setBackgroundColor(BACKCOLOR);
 visor->setIdentifierColor(1); // negro

 if (tipoCanal == true)
 {
  // y pongo la lista, el label y el visor
  tituloLista = new QLabel(this);
  tituloLista->setAlignment(AlignCenter);
  lista = new ListaNicks(this);
  connect(lista, SIGNAL(nickDoubleClicked(const char *)), SIGNAL(nickDoubleClicked(const char *)));
 }

 // para que nunca puedan achicar la ventana m�s de un l�mite
 // setMinimumSize(4*ANCHO_LISTA_NICKS, 0 /*6*ALTO_LABEL + ALTO_CUADRO_ENTRADA*/);

 // establezco la font
 font = new QFont(config->readStrValue(CONFIG_FONT1, DEF_FONT1), config->readIntValue(CONFIG_FONTSIZE1, DEF_FONTSIZE1));
 font->setFixedPitch(true); // para que si no encuentra la font seleccionada
													  // busque un de ancho igual para todos los caracteres
 visor->setFont(*font);
 entrada->setFont(*font);

 // guardo el tipo de ventana
 VentanaChat::tipoCanal = tipoCanal; // tiene que hacerse antes de llamar a resetearVentana()!!!!!!!!!!!

 // reseteo las variables
 resetearVentana();

 // conecto las se�ales
 connect(entrada, SIGNAL(returnPressed()), SLOT(nuevaLinea()));

 // a partir de ac� hago un tratatiento diferencial para las ventana de status y
 // las normales
 // las primeras no tienen popup por defecto, las segundas si

 if (nombre == NULL)
 {
  VentanaChat::nombre = NULL;
  connect(visor, SIGNAL(rightButtonPressed(const QPoint)), SIGNAL(rightButtonPressed(const QPoint)));
 } else {
  VentanaChat::nombre = new char [strlen(nombre)+1];
  strcpy(VentanaChat::nombre, nombre);

  // connecto la se�al para mostrar el popup de las ventanas
  connect(visor, SIGNAL(rightButtonPressed(const QPoint)), SLOT(mostrarPopupVentana(const QPoint)));

  // creo el popup del chatview
  popupVentana = new QPopupMenu();
  popupVentana->insertItem("Clean screen");
  if (tipoCanal == true)
  {
   popupVentana->insertSeparator();
   popupVentana->insertItem("Part");
  }

  // si es una ventana de canal creo el popup de la lista
  if (tipoCanal == true)
  {
   popupLista = new QPopupMenu();
   popupLista->insertItem("Whois");
   popupLista->insertItem("Ignore");
   popupLista->insertSeparator();
   popupLista->insertItem("CTCP VERSION");
   popupLista->insertItem("CTCP USERINFO");
   popupLista->insertItem("CTCP FINGER");
   // conecto la se�al reci�n ahora (tendr�a que hacerlo al crear la lista)
   connect(lista, SIGNAL(rightButtonClicked(QPoint , const char *)), SLOT(mostrarPopupLista(QPoint, const char *)));
  } else {
   popupLista = NULL;
  }

  // comenzar el logging
  logFile = NULL;
  if (nombre != NULL)
  {
   if (tipoCanal == true)
   {
    if (config->readBoolValue(CONFIG_LOGCHANNELS, DEF_LOGCHANNELS) == true) comenzarLogging();
   } else {
    if (config->readBoolValue(CONFIG_LOGPRIVATES, DEF_LOGPRIVATES) == true) comenzarLogging();
   }
  }
 }
 // muestro la ventana
 show();
}

VentanaChat::~VentanaChat()
{
 // termino el log
 terminarLogging();

 // dejo que QT vac�e la memoria
 if (nombre != NULL) delete [] nombre;
 if (popupVentana != NULL) delete popupVentana;
 if (popupLista != NULL) delete popupLista;
}

void VentanaChat::escribirLinea(const char *identificador, const char *mensaje, int color = DEFAULT_FOREGROUND)
{
 // solo llevo cuenta de las lineas en las ventanas de canales, no en los privados
 if ((tipoCanal == true) && (habilitada == true))
 {
  // me fijo si tengo que llevar cuenta de las lineas ( si las lineas repetidas en la
	// configuraci�n valen 0 entonces significa que est� desactivado
  if (config->readIntValue(CONFIG_MAXLINEASREPETIDAS, DEF_MAXLINEASREPETIDAS) > 0)
  {
   // si la cuenta de lineas son menores o iguales que el l�mite, aumento la cuenta y
   // muestro el mensaje. De otra forma simplemente ignoro el mensaje
   if ((lastIdentifier == identificador) && (lastMessage == mensaje))
   {
    linesCount++;
    if (linesCount > config->readIntValue(CONFIG_MAXLINEASREPETIDAS, DEF_MAXLINEASREPETIDAS))
		{
      // ya se escribieron demasiadas copias
			return;
		}		
   } else {
    lastIdentifier = identificador;
 		lastMessage		 = mensaje;
    linesCount = 0;
   }
  }
 }

 // la muestro en el chatView
 visor->write(identificador, mensaje, color);
 // emito la se�al indicando que la ventana est� activa para que el manejador de ventanas haga lo que le venga en gana
 emit ventanaActiva(this);
 // y la loggueo unicamente si la ventana no est� desactivada
 if (habilitada == true) log(identificador, mensaje);
}

void VentanaChat::clear()
{
 // limpio la ventana
 visor->clear();
}

void VentanaChat::agregarNick(const char *nick)
{
 // agrego un s�lo nick a a la lista ( se supone que se uni� a un canal )
 // Si la ventana no tiene lista de nicks (no es un canal) el pedido se ignora
 if (tipoCanal == true)
 {
  // inserto a la lista de forma ordenada
  lista->addNick(nick);

  // actualizo el contador de nicks
  actualizarTituloLista();
 }
}

void VentanaChat::quitarNick(const char *nick)
{
 // si no es un canal no hace nada
 if (tipoCanal == false) return;

 lista->removeNick(nick);
}

void VentanaChat::agregarListaNicks(const char *listaNicks)
{
 // si no es un canal no hace nada
 if (tipoCanal == false) return;

 // si la lista ya fue llenada, no hace nada
 if (listaLlena == true) return;

 // seteo el autoupdate de la lista a false para que no se redibuje sola
 lista->setAutoUpdate(false);

 char newNick[strlen(listaNicks)+1]; // cualquier cosa para evitar stackOverflow
 bool finalizado = false;
 strcpy(newNick, "");
 int i = 0;
 while (finalizado == false)
 {
  if ((listaNicks[i] == ' ') || (listaNicks[i] == 0))
  {
   if (listaNicks[i] == 0) finalizado = true;
   if  (strlen(newNick) > 0)
   {
    bool op = false;
    if (newNick[0] == '@') op = true;
    if ((newNick[0] == '@') || (newNick[0] == '+'))
    {
     lista->addNick(newNick+1, op); // el +1 para eliminar el @/+
    } else {
     lista->addNick(newNick);
    }
    strcpy(newNick, "");
   }
  } else {
   strncat(newNick, listaNicks+i, 1); // concateno el caracater
  }
  i++;
 }

 // seteo el autoupdate de la lista a true para que vuelva a redibujarse automaticamente
 lista->setAutoUpdate(true);
 // repinto la lista para que se la vea
 lista->repaint();

 // por �ltimo actualizo el contador de nicks
 actualizarTituloLista();
}

void VentanaChat::resetearVentana()
{
 // CUIDADO CON ESTA FUNCI�N!
 // vuelve la ventana a su estado original: lista vac�a y habilitada
 habilitada = true;

 if (tipoCanal == true) lista->clear();
 listaLlena = false;

 // inicializo las variables que me dicen de las repeticiones
 lastIdentifier = "";
 lastMessage  	= "";
 linesCount			= 0;
}

bool VentanaChat::existeNick(const char *nick)
{
 // si no es un canal solo responde true si es un privado con el nick pedido
 if (tipoCanal == true)
 {
  char opVersion[strlen(nick) + 2];
  strcpy(opVersion, "@");
  strcat(opVersion, nick);

  // primero tengo que buscar la posici�n del nick en la lista (se asume que no pueden haber repeticiones)
  for (unsigned int i = 0; i < lista->count(); i++)
  {
   if ((strcasecmp(nick, lista->list(i)) == 0) || (strcasecmp(opVersion, lista->list(i)) == 0))
   {
    // el nick existe
    return true;
   }
  }
  return false;
 } else {
  // es una ventana de privado, me fijo si es con el nick que me pidieron
  if (strcasecmp(nick, (const char *)nombre) == 0)
  {
   return true; // es el nick
  } else {
   return false; // no es el nick
  }
 }
}

void VentanaChat::cambiarNick(const char *nick, const char *nuevoNick)
{
 // por si las dudas, si no es una ventana tipo canal no hace nada
 if (tipoCanal == false) return;

 lista->changeNick(nick, nuevoNick);
}

void VentanaChat::actualizarTituloLista()
{
 if (tipoCanal == false) return;

 char linea[40 + strlen(" nicks") +1];

 if(lista->count() <= 1) {
  sprintf(linea, "%i nick", lista->count());
 } else {
  sprintf(linea, "%i nicks", lista->count());
 }

 tituloLista->setText(linea);
}

void VentanaChat::resizeEvent(QResizeEvent *e)
{
 entrada->setGeometry(0, height() - ALTO_CUADRO_ENTRADA, width(), ALTO_CUADRO_ENTRADA);
 if (tipoCanal == true)
 {
  // divisor se encarga de mantener el tama�o de sus widgets hijos
  visor->setGeometry(0, 0, width() - ANCHO_LISTA_NICKS, height() - ALTO_CUADRO_ENTRADA);
  lista->setGeometry(width() - ANCHO_LISTA_NICKS, ALTO_LABEL, ANCHO_LISTA_NICKS, height() - ALTO_LABEL - ALTO_CUADRO_ENTRADA);
  tituloLista->setGeometry(width() - ANCHO_LISTA_NICKS, 0, ANCHO_LISTA_NICKS, ALTO_LABEL);
 } else {
  visor->setGeometry(0, 0, width(), height() - ALTO_CUADRO_ENTRADA);
 }
}

void VentanaChat::nuevaLinea()
{
 // si la entrada no tiene nada, no mando nada
 if  ((entrada->text() != NULL) && (strcmp(entrada->text(), "")  != 0))
 {
   // si la ventana est� deshabilitado no manda nada
  if (habilitada == false)
  {
   escribirLinea(PREFIJO_AVISOPROGRAMA, "You are not in this channel anymore", COLOR_PROGRAMMESSAGE);
  } else {
   emit lineaDisponible(nombre, entrada->text());
  }
  entrada->clear();
 }
}


void VentanaChat::mostrarPopupVentana(const QPoint coordenada)
{
 // muestra el popup del la ventana
 int eleccion = popupVentana->exec(coordenada);

 switch (eleccion) {
	case 0	: // clear
						visor->clear();
						break;
	case 2	: // part del canal
					 char *linea = new char[1 +strlen(COMMAND_PART) + 1 + strlen(nombre) + 2 + strlen(config->readStrValue(CONFIG_PARTMESSAGE, DEF_PARTMESSAGE)) + 1];
				   sprintf(linea, "/%s %s :%s", COMMAND_PART, nombre, config->readStrValue(CONFIG_PARTMESSAGE, DEF_PARTMESSAGE));
				   emit lineaDisponible(nombre, linea);
		       delete [] linea;
					 break;
 }
}

void VentanaChat::mostrarPopupLista(QPoint globalPos, const char *item)
{
 // envio un comando seg�n sea el item del men� en el que el usuario haya clickeado
 char *linea = NULL, *pregunta = NULL;
 char nick[strlen(item)+1];
 // si el nick empieza por @ se la quito porque es la que indica que es un op
 if (item[0] == '@')
 {
  strcpy(nick, item+1); // salte� la @
 } else {
  strcpy(nick, item); // desde el principio
 }

 // muestra el popup del la ventana
 int eleccion = popupLista->exec(globalPos);

 switch (eleccion) {
  case 0  : // whois
					 linea = new char[strlen("/WHOIS") + 1 + strlen(nick) + 1];
				   sprintf(linea, "/WHOIS %s", nick);
				   emit lineaDisponible(nombre, linea);
					 break;
	case 1	: // ignore
           // primero pregunto para asegurarme
           pregunta = new char[strlen("Do you really want to ignore")+1+strlen(nick)+3];
           sprintf(pregunta, "Do you really want to ignore %s?", nick);
           if (QMessageBox::warning(NULL, "Ignore", pregunta, "Yes, I do", "No, I don't") == 0)
           {
					  linea = new char[strlen("/IGNORE") + 1 + strlen(nick) + 1];
				    sprintf(linea, "/IGNORE %s", nick);
				    emit lineaDisponible(nombre, linea);
           }
           delete [] pregunta;
					 break;
	case 3	: // ctcp version
					 linea = new char[strlen("/VERSION") + 1 + strlen(nick) + 1];
				   sprintf(linea, "/VERSION %s", nick);
				   emit lineaDisponible(nombre, linea);
					 break;
	case 4	: // ctcp version
					 linea = new char[strlen("/USERINFO") + 1 + strlen(nick) + 1];
				   sprintf(linea, "/USERINFO %s", nick);
				   emit lineaDisponible(nombre, linea);
					 break;
  case 5	: // ctcp version
					 linea = new char[strlen("/FINGER") + 1 + strlen(nick) + 1];
				   sprintf(linea, "/FINGER %s", nick);
				   emit lineaDisponible(nombre, linea);
					 break;
 }
 //  si reserv� espacio para linea lo libero
 if (linea != NULL) delete [] linea;
}

void VentanaChat::setNickAsChanop(const char *nick)
{
 // por si la dudas, si la ventana no es de canal no hace nada
 if (tipoCanal == false) return;

 lista->setOperatorStatus(nick, true);
}

void VentanaChat::unsetNickAsChanop(const char *nick)
{
 // por si las dudas, si la ventana no es de canal no hace nada
 if(tipoCanal == false) return;

 lista->setOperatorStatus(nick, false);
}

void VentanaChat::comenzarLogging()
{
 QTime hora = QTime::currentTime();
 QDate dia  = QDate::currentDate();

 char path[strlen(obtenerHomePath()) + 1 + strlen(CARPETA_LOGS) + 1 + strlen(LOGFILEPREFIX) + strlen(nombre) + 1];
 sprintf(path, "%s/%s/%s%s", obtenerHomePath(), CARPETA_LOGS, LOGFILEPREFIX, nombre);

 logFile = fopen(path, "a"); // para agregar lineas, si no existe el archivo se crea
 if (logFile != NULL) fprintf(logFile, "\n--- Logging start at %s on %s\n", (const char *)hora.toString(), (const char *)dia.toString());
}

void VentanaChat::terminarLogging()
{
 QDate dia  = QDate::currentDate();
 QTime hora = QTime::currentTime();

 if (logFile != NULL)
 {
  fprintf(logFile, "--- Logging end at %s on %s\n", (const char *)hora.toString(), (const char *)dia.toString());
  fclose(logFile);
 }
}

void VentanaChat::log(const char *identificador, const char *mensaje)
{
 if (logFile != NULL)
 {
  char mensajeSinMirc[strlen(mensaje)+1];
  strcpy(mensajeSinMirc, mensaje);

  if (config->readBoolValue(CONFIG_BORRARMIRCLOGS, DEF_BORRARMIRCLOGS) == true)
  {
   filtrarMirc(mensajeSinMirc);
  }

  fprintf(logFile, "%s \t%s\n", identificador, mensajeSinMirc);
 }
}

// *************************************************************************

ManejadorVentanas::ManejadorVentanas(MainTab *tab)
{
 ManejadorVentanas::tab = tab;
 // lo conecto al slot cerrarVentana
 connect(tab, SIGNAL(tabClose(int)), SLOT(cerrarVentana(int)));

 lista = NULL;
 cantidad = 0;
}

ManejadorVentanas::~ManejadorVentanas()
{
 // vac�a la lista de ventanas
 vaciar();
}

void ManejadorVentanas::ventanaActiva(VentanaChat *v)
{
 // busca el id de la ventana y se fija si
 //  a)  es una ventana canal - remarca el titulo en los tabs
 //  b1) es una ventana Privada - est� configurado que se la eleve a primer plano
 //  b2) es una ventana privada - se remarca el t�tulo en los tabs

 // si as� est� configurado, elevo las ventanas de privados al recibir mensajes
 // sino simplemente remarco su t�tulo
 if ((v->esTipoCanal() == true) || (config->readBoolValue(CONFIG_ELEVARPRIVADOS, DEF_ELEVARPRIVADOS) == false))
 {
  tab->setNewInfo(v);
 } else {
  tab->raisePage(v);
 }
}

void ManejadorVentanas::vaciar()
{
 tipoNodoVentana *nodo = lista, *aux;

 while (nodo != NULL)
 {
  aux = nodo;
  nodo = nodo->enlace;

  delete [] aux->nombre;
  delete aux->ventana;
  delete aux;
 }

 // s�lo por futuras modificaciones
 cantidad = 0;
}

VentanaChat *ManejadorVentanas::crearVentana(const char *nombre, bool tipoCanal)
{
 // se fija si la ventana no existe y si no existe crea una ventana
 // con ese nombre
 // si ya existe una ventana con ese nombre no borrada, devuelve null
 if (existeVentana(nombre) == true) return NULL;

 // creo un nuevo nodo y lo pongo al principio de la lista
 tipoNodoVentana *nuevoNodo = new tipoNodoVentana;
 nuevoNodo->nombre  = new char[strlen(nombre)+1];
 strcpy(nuevoNodo->nombre, nombre);
 nuevoNodo->ventana = new VentanaChat(tab, nombre, tipoCanal);
 nuevoNodo->id 			= tab->nextId(); // as� lo engancho en la lista antes de agregar el tab
 nuevoNodo->enlace  = lista;
 lista = nuevoNodo;

 // ahora si puedo agregarlo
 tab->addPage(nombre, nuevoNodo->ventana);

 connect(nuevoNodo->ventana, SIGNAL(nickDoubleClicked(const char *)), SLOT(doubleClickNick(const char *)));
 connect(nuevoNodo->ventana, SIGNAL(lineaDisponible(const char*, const char *)), SIGNAL(lineaDisponible(const char *, const char *)));
 connect(nuevoNodo->ventana, SIGNAL(ventanaActiva(VentanaChat *)), SLOT(ventanaActiva(VentanaChat*)));

 // incremento el contador de ventanas
 cantidad++;

 return nuevoNodo->ventana;
}

VentanaChat *ManejadorVentanas::punteroVentana(const char *nombre)
{
 // devuelve el puntero a la ventana de ese nombre, y si esa ventana no existe
 // devuelve NULL

 tipoNodoVentana *nodo = lista;

 while (nodo != NULL)
 {
  if (strcasecmp(nodo->nombre, nombre) == 0) return nodo->ventana;

  nodo = nodo->enlace;
 }

 // si termin� el ciclo significa que no existe la ventana
 return NULL;
}

VentanaChat *ManejadorVentanas::punteroVentana(int index)
{
 // devuelve un puntero a la ventana n�mero index en la lista

 // si el indice no est� en el rango valido (0 <= index < cantidad) devuelvo NULL
 if ((index < 0) || (index >= cantidad)) return NULL;

 // declaro la variable
 tipoNodoVentana *nodo = lista;

 int i = 0;
 while (nodo != NULL)
 {
  if (i == index)
  {
   // esta es la ventana
   return nodo->ventana;
  }
  i++;
  nodo = nodo->enlace;
 }

 // no deber�a llegar nunca ac� pero por si la dudas
 return NULL;
}

bool ManejadorVentanas::existeVentana(const char *nombre)
{
 // devuelve true si existe una ventana determinada, y false en caso contrario
 // (NO, REALLY?????? :)

 tipoNodoVentana *nodo = lista;

 while (nodo != NULL)
 {
  if (strcasecmp(nodo->nombre, nombre) == 0) return true;
  nodo = nodo->enlace;
 }

 // si sal� del bucle significa que no existe la ventana
 return false;
}

void ManejadorVentanas::cerrarVentana(int id)
{
 tipoNodoVentana *nodo = lista, *aux = NULL;

 while ((nodo != NULL) && (nodo->id != id))
 {
  aux = nodo;
  nodo = nodo->enlace;
 }
 if (nodo == NULL) return; // no lo encontr�!

 //elimino la ventana
 if (aux  == NULL)
 {
  // es el primero
  lista = nodo->enlace;
 } else {
  aux->enlace = nodo->enlace;
 }
 (nodo->ventana)->partChannel();

 tab->removePage(id);

 delete [] nodo->nombre;
 delete nodo->ventana;
 delete nodo;
 cantidad--;
}

void ManejadorVentanas::doubleClickNick(const char *nick)
{
 // si la ventana existe no hace nada, pero si no existe crea una ventana de privado para ese nick
 // notese que esta informaci�n no se pasa al programa
 VentanaChat *v = punteroVentana(nick);
 if (v == NULL)
 {
  crearVentana(nick, false);
 }
}


void VentanaChat::partChannel()
{
 // si no es ventana de canal no hago nada
 if ((tipoCanal == true) && (nombre != NULL) && (habilitada == true))
 {
  char linea[1+strlen(COMMAND_PART)+1+strlen(nombre)+2+strlen(config->readStrValue(CONFIG_PARTMESSAGE, DEF_PARTMESSAGE))+1];
  sprintf(linea, "/%s %s :%s", COMMAND_PART, nombre, config->readStrValue(CONFIG_PARTMESSAGE, DEF_PARTMESSAGE));

  // emito la se�al corresp
  emit lineaDisponible(nombre, linea);
 }
}