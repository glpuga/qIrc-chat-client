/***************************************************************************
                          qicq.h  -  description
                             -------------------
    begin                : mi� sep 13 10:25:05 ART 2000
    copyright            : (C) 2000 by gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef QICQ_H
#define QICQ_H

// solo para usarse durante la etapa de debug
// #define DEBUG_TELLME

//  ---------- experimental ----------
#define ENABLE_TOOLBAR

#include "dccsend.h"
#include "dccget.h"

// include files for QT
#include <qapplication.h>
#include <qmainwindow.h>
#include <qwidget.h>
#include <qmenubar.h>
#include <qstatusbar.h>
#include <qsplitter.h>
#include <qlistview.h>
#include <qmessagebox.h>
#include <qaccel.h>
#include <qtimer.h>
#include <qwidgetstack.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

// my own header files for the screen
#include "MainTab.h"
#include "ventanas.h"
#include "inputBox.h"
#include "Config.h"
#include "cuadroConectarCon.h"
#include "cuadroConfiguracion.h"
#include "cuadroListaNicks.h"
#include "channellist.h"
#include "notifyshowcase.h"
#include "programcommand.h"
#include "general.h"
#include "resource.h"

// for the test phase
#ifdef DEBUG_TELLME
 #include <iostream.h>
#endif

#ifdef ENABLE_TOOLBAR
 #include <qtoolbar.h>
 #include <qtoolbutton.h>
#endif

// include files for IRC
#include "IRCClient.h"
#include "IRCCodes.h"

// unas constantes de comos se ve la pantalla
#define ANCHO_COLUMNA_CANAL 		100
#define ANCHO_COLUMNA_CANTIDAD 	60

// solo un n�mero al azar, uno que no pueda ser el de los dem�s men�s
#define ID_AWAY										600

class QIrc : public QMainWindow
{
 Q_OBJECT

private:
  // los controles que voy a usar para crear la pantalla principal
 MainTab *mainWidget;
 channelList *listaCanales;
 notifyShowcase *visorNotifies;
 VentanaChat *screen;
 QMenuBar *menu;

 // los menues
 QPopupMenu *archivo, *comando, *setup, *dcc, *ayuda;
 QPopupMenu *popupCanales, *popupScreen;

#ifdef ENABLE_TOOLBAR
 // la barra de herramientas
 QToolBar *tBar;
#endif

 // el cliente de IRC
 IRCClient cliente;

 // el controlador que lleva cuenta de las ventanas
 ManejadorVentanas *ventanas;

 // utilizado por los men�es interactivos
 inputBox *cuadroEntrada;

 // la ventana de conectarcon
 cuadroConectarCon *conectarCon;

 // la ventana de conf
 cuadroConfiguracion *configuracion;

 // la ventana de ignores y la de notifies
 cuadroListaNicks *cuadroIgnores, *cuadroNotificar;

 // el nick con el que estoy registrado en el server
 QString registeredNick;

 // variables varias, banderas, etc
 bool recibiendoListaCanales; // se usa para saber cuando borrar la lista de canales
 bool noMotd;
 bool debeEstarConectado; // detecta desconexiones no originadas por el usuario
 int ctcpDelay; // para evitar floods por ctcp y el segundo para evitar
                             // los notices que vienen al entrar a un canal

 // estas dos banderas son muy importantes para desconectarse apropiadamente cuando sea
 // necesario interrumpir una conexi�n
 bool logged, conectado;

 int cantidadVentanasDccAbiertas;

 // temporiza el envio de isons
 int proximoIson;

private:
 // las funciones que crean la pantalla e inicializan la aplicaci�n
 void crearToolBar();
 void crearMenus();
 void crearPantallaPrincipal();
 void inicializarVariables();
 void conectarSenialesIRC();
 void inicializarTemporizador();
 void mostrarGpl();

 // gracias al autor del texto sobre CTCP por usar octal... OCTAL IMBECIL!!!???
 void extraerCtcpQuery(const char *nick, const char *destino, char *linea); // extrae los tokens ctcp, los responde y los quita de la cadena original
 void extraerCtcpReply(const char *nick, char *linea); // extrae los tokens ctcp, los responde y los quita de la cadena original
 void sendCtcpQuery (const char *nick, const char *ctcpCommand, const char *datos = NULL);
 void sendCtcpReply (const char *nick, const char *ctcpCommand, const char *datos = NULL);
 // para saber si un nick est� en la lista de ignorados
 bool ignorado(const char *nick);

 // maneja los intentos de cerrar la ventana
 void closeEvent(QCloseEvent *e);
 // controla el temporizador
 void timerEvent(QTimerEvent *e);

public:
  QIrc(QWidget *parent = 0, char *nombre = 0);
  ~QIrc();

public slots:
 // los slots de control de men�s
 void menuConectar();
 void menuConectarCon();
 void entradaConectarCon(Server *s);
 void menuDesconectar();
 void menuSalir();

 void menuCambiarNick();
 void menuUnirseCanal();
 void menuSalirCanal();
 void menuQuienEs();
 void entradaCambiarNick(const char *entrada);
 void entradaUnirseCanal(const char *entrada);
 void entradaSalirCanal(const char *entrada);
 void entradaQuienEs(const char *entrada);
 void menuAway();
 void menuMotd();
 void menuListarCanales();

 void menuDccSend();
 void entradaDccSend(DccSend *v);
 void entradaDccGet(DccGet *v);

 void menuConfigurar();
 void borrarVentanaConfiguracion();
 void menuIgnorar();
 void entradaIgnorar();
 void menuNotificar();
 void entradaNotificar();

 void menuInstrucciones();
 void menuGracias();
 void menuAbout();
 void menuAboutQt();

// los slots de control del objeto cliente irc
 void nuevoMensajeVentana(const char *ventana, const char * mensaje);

 void conexionCompleta();
 void estatusConexion(const char *mensaje);
 void loginCompleto(const char *);
 void desconexion();

 void noReconocido(IRCEvent *e);

 void replyCode(IRCEvent *evento);

 void nick(const char *viejoNick, const char *nuevoNick);
 void quit(const char *nick, const char *razon);
 void join(const char *, const char *);
 void part(const char *, const char *, const char *);
 void mode(IRCEvent *evento);
 void topic(const char*, const char *, const char*);
 void invite(const char *nick, const char *canal);
 void kick(const char *, const char *, const char *, const char *);
 void privmsg(const char *, const char *, const char *);
 void notice(const char *, const char *, const char *);
 void ping(const char *server);
 void error(const char * mensaje);

private slots:
 // slots varios de mandejo interno
 void ventanaPrincipalActiva(VentanaChat *v) { mainWidget->setNewInfo(v); }

 void doubleClickEnCanal(const QPoint globalCoordinates);
 void abrirPopupCanales(const QPoint globalCoordinates);
 void abrirPopupScreen(const QPoint coordinates);
};

#endif