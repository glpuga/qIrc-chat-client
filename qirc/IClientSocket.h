/***************************************************************************
                          IClientSocket.h  -  description
                             -------------------
    begin                : Fri Sep 15 2000
    copyright            : (C) 2000 by gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef  ICLIENTSOCKET_H
#define ICLIENTSOCKET_H

// MODO DEBUG
// #define DEBUG_MODE

#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>

#ifdef DEBUG_MODE
 #include <iostream.h>
#endif

// un n�mero muy alto
// #define E_NOTRESOLV 1001

class IClientSocket
{
private:
 int handler; // el entero asociado al socket abiert
 in_addr ip;
 unsigned int puerto;

 int lastError, lastErrnoValue;

 bool conectado;
 bool socketCreado; // s�lo para saber cuando puedo devolver el handler
 bool blocking;

 void inicializar(); // inicializa las variables
 int connect(); // conecta con los datos actuales, solo para uso interno

public:
 IClientSocket(); // establecen los datos, no conectan
 ~IClientSocket();

 int getSocket(); // devuelve el n�mero de descriptor del socket, o -1 si no est�s conectado
 const char *strError();

 int connect(int socket, bool block = true); // conecta con los datos actuales
 int connect(in_addr addr, unsigned int puerto, bool blocking = true); // conecta con los nuevos datos
 int connect(const char * server, unsigned int puerto, bool blocking = true); // conecta con los nuevos datos

 long int read(char *, long count); // lee count bytes (signal resistant), devuelve cantidad datos leidos
 long int write(const char *, long count); // escribe count bytes, devuelve cantidad datos escritos

 void close(); // cierra el socket

 int resolv(const char * fqdn, in_addr * ip); // transforma un nombre o ip de cadena a una estructura in_addr
};

#endif