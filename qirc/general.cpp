/***************************************************************************
                          general.cpp  -  description
                             -------------------
    begin                : Sat Nov 11 2000
    copyright            : (C) 2000 by Gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "general.h"

char *obtenerUser()
{
 // obtiene el username de las variables de entorno
 // se utilizan dos variables comunes, USER y LOGNAME
 // que se utilizan en sistemas FREEBSD y SYSTEM-V
 // respectivamente; si no existe una se prueba la siguiente
 // si no existe ninguna devuelve una cadena null

 char *userName = getenv("USER");
 if (userName == NULL) userName = getenv("LOGNAME");
 if (userName == NULL)
 {
  // no existe ninguna de las dos!!!!
  #ifdef DEBUG_GENERAL
   cout << "No se pudo obtener el nombre de usuario de las variables de entorno!" << endl;
  #endif
 }
 return userName;
}

char *obtenerHomePath()
{
 char *nada = ""; // notese que la string no est� en la pila, sino en el c�digo
 char *path = getenv(HOME_ENVVAR);
 if (path == NULL)
 {
  #ifdef TELLME_DEBUG
   cout << "No se pudo hallar una variable de entrono HOME!!!" << endl;
  #endif
  path = nada; // para no devolver un valor NULL
 }
 return path;
}

void crearCarpetaLogs()
{
 // se asegura de que exista la carpeta para almacenar los logs
 // si no existe la crea
 char path[strlen(obtenerHomePath())+strlen(CARPETA_LOGS)+2];
 sprintf(path, "%s/%s", obtenerHomePath(), CARPETA_LOGS);

 #ifdef DEBUG_GENERAL
  cout << "El path de la carpeta de logs es: " << path << endl;
 #endif

 if (mkdir(path, S_IRWXU) == 0)
 {
  #ifdef DEBUG_GENERAL
   cout << " Se creo la carpeta para los logs" << endl;
  #endif
 } else {
  #ifdef DEBUG_GENERAL
   if (errno == EEXIST)
   {
    cout << " La carpeta para los logs ya existe" << endl;
   } else {
    cout << " Se produjo un error indeterminado al crear la carpeta para los logs" << endl;
    cout << " *Error: " << errno << " - " << strerror(errno) << endl;
   }
  #endif
 }
}

void filtrarMirc(char *linea)
{
 // borra todos los caracteres de control del mirc
 int origen, destino, contador;
 origen = destino = contador = 0;

 #ifdef DEBUG_FILTRO_MIRC
  cout << "A la entrada del filtro: " << linea << endl << "Filtrando: " << endl;
 #endif

 while(linea[origen] != 0)
 {
  switch(linea[origen]) {
   case MIRC_CTRL_B : // bold
   case MIRC_CTRL_U : // underline
   case MIRC_CTRL_R : // it�lica
   case MIRC_CTRL_O : // letra com�n
											// lo salteo, no tienen parametros
                      #ifdef DEBUG_FILTRO_MIRC
                       cout << "(" << (short int)linea[origen] << ")";
                      #endif
											origen++; break;
   case MIRC_CTRL_K : // colores, este si tiene par�metros
											// los parametros viene as�
											// <ascii 3>21*2(digito)[<,>1*2(digito)]
                      #ifdef DEBUG_FILTRO_MIRC
                       cout << "(" << (short int)linea[origen] << ")";
                      #endif
	 										origen++;
                      contador = 0;
                      #ifdef DEBUG_FILTRO_MIRC
                       cout << "[";
                      #endif

                      // es increible pero la implementaci�n de strchr hace que si el caracter
                      // es cero STRCHR DICE SIEMBRE QUE ESE CARACTER APARECE EN LA CADENA
                      // tal parece que considera el terminador de cadena como parte de la misma
                      // por esto era que se estaba colgando el programa.. claro! cuando los mensajes
                      // terminaban en CTRL_K este while segu�a de largo
                      while ((linea[origen] != 0) && (strchr("0123456789", linea[origen]) != NULL) && (contador < 2))
                      {
                       #ifdef DEBUG_FILTRO_MIRC
                        cout << linea[origen];
                       #endif
	                     contador++; origen++;
                      }

											if (linea[origen] == ',')
											{
                       #ifdef DEBUG_FILTRO_MIRC
                        cout << linea[origen];
                       #endif
	    								 origen++;
                       contador = 0;
                       while ((linea[origen] != 0) && (strchr("0123456789", linea[origen]) != NULL) && (contador < 2))
                       {
                        #ifdef DEBUG_FILTRO_MIRC
                         cout << linea[origen];
                        #endif	
                        contador++; origen++;
                       }
                      }
                      #ifdef DEBUG_FILTRO_MIRC
                       cout << "]";
                      #endif
                      // eso es todo
											break;
   default:	          linea[destino] = linea[origen];
                      #ifdef DEBUG_FILTRO_MIRC
                       cout << linea[origen];
                      #endif
	                    origen++;
											destino++;
											break;
  } // switch
 } // while
 linea[destino] = 0; // el cero de terminaci�n

 #ifdef DEBUG_FILTRO_MIRC
  cout << endl << "A la salida del filtro: " << linea << endl;
 #endif
}

QColor colorMirc(int iColor)
{
 // devuelve un color correspondiente a la paleta del mIrc
 QColor color;
 switch (iColor % 16) // para que siempre est� en el margen 0-15
 {
  case 0		: color = QColor(255,255,255); break;
  case 1		: color = QColor(0,0,0); break;
  case 2		: color = QColor(0, 9, 198); break;
  case 3		: color = QColor(0, 158, 0); break;
  case 4		: color = QColor(255, 0, 0); break;
  case 5		: color = QColor(183, 88, 0); break;
  case 6		: color = QColor(230, 0, 230); break;
  case 7		: color = QColor(255, 126, 0); break;
  case 8		: color = QColor(219, 215, 0); break;
  case 9		: color = QColor(175, 239, 0); break;
  case 10		: color = QColor(0, 163, 239); break;
  case 11		: color = QColor(63, 109, 211); break;
  case 12		: color = QColor(0, 0, 255); break;
  case 13		: color = QColor(244, 129, 221); break;
  case 14		: color = QColor(204, 204, 204); break;
  case 15		: color = QColor(224, 224, 224); break;
 }
 return color;
}

bool validNickname(const char *nick)
{
 const char letters[] = "ABCDEFGHIJKLMN�OPQRSTUVWXYZabcdefghijklmn�opqrstuvwxyz";
 const char digits[]  = "0123456789";
 const char special[] = "[]\\`_^{|}.";
 const char dash[] = "-";

 char fistChar[strlen(letters)+strlen(special)+1];
 strcpy(fistChar, letters);
 strcat(fistChar, special);
 char allChars[strlen(letters)+strlen(special)+strlen(digits)+strlen(dash)+1];
 strcpy(allChars, letters);
 strcat(allChars, special);
 strcat(allChars, digits);
 strcat(allChars, dash);

 // seg�n secci�n 2.3.1 del rfc del protocolo irc cliente (RFC 2812).
 // la primera letra solo puede ser una letra o un signo especial
 if (strchr(fistChar, nick[0]) == NULL)
 {
  // no es v�lido
  return false;
 }
 // todas las dem�s pueden ser de cualquiera de los grupos, o tambi�n un dash ('-')
 if (strspn(nick, allChars) != strlen(nick))
 {
  // no es v�lido
  return false;
 }
 // es un nick correcto
 return true;
}