/***************************************************************************
                          IClientSocket.cpp  -  description
                             -------------------
    begin                : Fri Sep 15 2000
    copyright            : (C) 2000 by gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#include <IClientSocket.h>

#define ERROR_ICLIENTSOCKET_NOERROR		0
#define ERROR_ICLIENTSOCKET_NOSOCKET	1
#define ERROR_ICLIENTSOCKET_NOCONNECT	2
#define ERROR_ICLIENTSOCKET_NORESOLV	3
#define ERROR_ICLIENTSOCKET_NOREAD		4
#define ERROR_ICLIENTSOCKET_NOWRITE		5
#define ERROR_ICLIENTSOCKET_NOFCNTL		6

const char *ICLIENTSOCKET_MENSAJES[] = {"Nothing has happened, boss",
  																		"Can't create the socket to initiate the connection",
                                      "Can't connect to the destination",
 																			"Can't resolv the server name",
																			"Problems during read operation",
																			"Problems during write operation",
																			"Can't make the socket non-blocking"};

#define ICLIENT_MAX_MENSAJE_ERROR 300

char ICLIENTSOCKETmensajeError[ICLIENT_MAX_MENSAJE_ERROR+1];

void IClientSocket::inicializar()
{
 #ifdef DEBUG_MODE
  cout << "IClientSocket::Inicializar()" << endl;
 #endif

 // inicializo todas las variables del objeto
 handler = 0;
 puerto = 0;
 bzero((void *)&ip, sizeof(in_addr));

 conectado = false;
 socketCreado = false;
 blocking = true;
 lastError = ERROR_ICLIENTSOCKET_NOERROR;
}

IClientSocket::IClientSocket()
{
 #ifdef DEBUG_MODE
  cout << "IClientSocket::IClientSocket()" << endl;
 #endif
 inicializar();
}


IClientSocket::~IClientSocket()
{
 #ifdef DEBUG_MODE
  cout << "IClientSocket::~IClientSocket()" << endl;
 #endif

 // si todav�a est� conectado, lo desconecto
 if (conectado == true)
 {
  close();
 }
}

int IClientSocket::getSocket()
{
 #ifdef DEBUG_MODE
  cout << "IClientSocket::getSocket()" << endl;
 #endif
 return handler;
}

const char *IClientSocket::strError()
{
 switch (lastError)
 {
   case ERROR_ICLIENTSOCKET_NORESOLV : snprintf(ICLIENTSOCKETmensajeError, ICLIENT_MAX_MENSAJE_ERROR, "%s", ICLIENTSOCKET_MENSAJES[lastError]);
												 break;
   default						 : snprintf(ICLIENTSOCKETmensajeError, ICLIENT_MAX_MENSAJE_ERROR, "%s - %s", ICLIENTSOCKET_MENSAJES[lastError], ::strerror(lastErrnoValue));
												 break;
 }
 return ICLIENTSOCKETmensajeError;
}

int IClientSocket::connect()
{
 #ifdef DEBUG_MODE
  cout << "IClientSocket::connect()" << endl;
 #endif

 // si todav�a est� conectado, primero desconecta
 if(conectado == true)
 {
  close();
 }

 // creo el socket
 if((handler = socket(AF_INET, SOCK_STREAM, 0)) < 0)
 {
  lastError = ERROR_ICLIENTSOCKET_NOSOCKET;
  lastErrnoValue = errno;
  return -1;
 }
 socketCreado = true;

 // si el socket es tiene que ser bloqueante, que as� sea
 if (blocking == false)
 {
  if (fcntl(handler, F_SETFL, O_NONBLOCK) < 0)
  {
   lastError = ERROR_ICLIENTSOCKET_NOFCNTL;
   lastErrnoValue = errno;
  }
 }

 // finamente realizo la conexi�n
 sockaddr_in addr;
 addr.sin_family = AF_INET;
 addr.sin_port   = htons(puerto);
 addr.sin_addr   = ip;
 bzero(&(addr.sin_zero), 8);
 int respuesta = ::connect(handler, (sockaddr *)&addr, sizeof(sockaddr_in));

  // si connect devolvi� error, me fijo si es porque es no bloqueante (si corresponde) sino, lo traslado
 if ((respuesta == 0) || ((respuesta < 0) && (errno == EINPROGRESS) && (blocking == false)))
 {
  conectado = true; // notese que esto no es muy util para el usuario en caso de que
                    // el socket sea no bloqueante
 } else {
  lastError = ERROR_ICLIENTSOCKET_NOCONNECT;
  lastErrnoValue = errno;
  return -1;
 }

 return 0;
}


int IClientSocket::connect(int socket, bool block = true)
{
 #ifdef DEBUG_MODE
  cout << "IClientSocket::connect(in_addr *, int, bool)" << endl;
 #endif

 // si todav�a est� conectado, primero desconecta
 if(socketCreado == true)
 {
  close();
 }

 handler = socket;
 bzero((void *)&ip, sizeof(in_addr));

 conectado = true;
 socketCreado = true;
 blocking = block;
 lastError = 0;

 // si corresponde lo hago no bloqueante
 if (block == false)
 {
  fcntl(handler, F_SETFL, O_NONBLOCK);
 }

 return 0; // como el n�mero de socket me fue dado, presupongo que ya est� conectado
}

int IClientSocket::connect(in_addr addr, unsigned int p, bool block = true)
{
 #ifdef DEBUG_MODE
  cout << "IClientSocket::connect(in_addr *, int, bool)" << endl;
 #endif

 // si todav�a est� conectado, primero desconecta
 if(socketCreado == true)
 {
  close();
 }

 // cambia los datos y llama a connect()
 ip = addr;
 puerto = p;
 blocking = block;

 return connect();
}

int IClientSocket::connect(const char * server, unsigned int p, bool block = true)
{
 #ifdef DEBUG_MODE
  cout << "IClientSocket::connect(const char *, int, bool) -> " << server << ":" << p << endl;
 #endif

 // si todav�a est� conectado, primero desconecta
 if(socketCreado == true)
 {
  close();
 }

 // seteo los datos que ya tengo
 puerto = p;
 blocking = block;

 // resuelvo el nombre
 if (resolv(server, &ip) < 0)
 {
  #ifdef DEBUG_MODE
   cout << "No se pudo resolver el nombre del server! " << endl;
  #endif
  lastError = ERROR_ICLIENTSOCKET_NORESOLV;

  return -1;
 }
 return connect();
}

long int IClientSocket::read(char *data, long count)
{
 #ifdef DEBUG_MODE
  cout << "IClientSocket::read(char *, long)" << endl;
 #endif

 long done = 0;
 long res = 0;

 do {
  res = recv(handler, data + done, count - done, 0);

  if (res > 0)
  {
   done += res;
  }
 } while ((((done < count) && (res > 0)) && (blocking == true)) || ((res < 0) && (errno == EINTR)));
 // } while ((blocking == true) && ( || ((res < 0) && (errno == EINTR))));
 // nose que si el socket es no bloqueante s�lo lee una vez y sale a menos que la lectura haya sido
 // interrumpida

 if (res < 0)
 {
  if (errno == EWOULDBLOCK)
  {
   return 0; // no se ley� nada
  } else {
   lastError = ERROR_ICLIENTSOCKET_NOREAD;
   lastErrnoValue = errno;
   return -1;
  }
 } else {
  return done;
 }
}

long IClientSocket::write(const char *data, long count)
{
 #ifdef DEBUG_MODE
  cout << "IClientSocket::write(const char *, long)" << endl;
 #endif

 long sent = 0;
 long response = 0;

 do {
  response = send(handler, data + sent, count - sent, 0);

  if (response > 0)
  {
   sent += response;
  }
 } while (((sent < count) && (response >= 0)) || ((response < 0) && (errno == EWOULDBLOCK)));
 // EWOULDBLOCK es para que vuelva a intentar hasta poder escribir todos los datos
 // si, la hice bloqueante, �Y? :)

 if (response < 0)
 {
  // se sali� del bucle por un error que no fue EWOULDBLOCK
  lastError = ERROR_ICLIENTSOCKET_NOWRITE;
  lastErrnoValue = errno;
  return -1;
 }
 return sent; // no error
}

void IClientSocket::close()
{
 #ifdef DEBUG_MODE
  cout << "IClientSocket::close()" << endl;
 #endif

 if (socketCreado == true)
 {
  // cierro el handle
  ::close(handler);

  // reinicio el objeto
  conectado = false;
  socketCreado = false;
  blocking = true;
 }
}

int IClientSocket::resolv(const char * fqdn, in_addr * ip)
{
 #ifdef DEBUG_MODE
  cout << "IClientSocket::resolv(const char *, in_addr *)" << endl;
 #endif

 // fqdn es "full qualified domain name"
 hostent *host; // un puntero para leer la info que me devuelve el gethostbyname

 host = gethostbyname(fqdn); // thats all!
 // si el puntero devuelto es null, entonces no se pudo resolver la direcci�n
 if (host == NULL)
 {
  return -1; // error, unico error posible: NO SE ENCONTR� EL HOSTNAME
 }

 // copio la direcci�n ip
 bcopy((void *)host->h_addr, (void *)ip, sizeof(in_addr));

 return 0; // no hay error
}
