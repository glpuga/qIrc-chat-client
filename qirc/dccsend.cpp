/***************************************************************************
                          dccsend.cpp  -  description
                             -------------------
    begin                : Tue Nov 14 2000
    copyright            : (C) 2000 by Gerardo Puga
    email                : gere@mailroom.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "dccsend.h"

// esta variable est� definida en tellme.cpp
extern Config *config;

// en esta variable almaceno la direcci�n para usarla en los dccsend
extern struct in_addr localIpAddress;

DccSend::DccSend(const char *nick = 0, const char *archivo) : QWidget(NULL, NULL, WStyle_Customize | WStyle_DialogBorder)
{
 // inicCializo las variables
 timeout				= 0;
 CONNECTTIMEOUT = config->readIntValue(CONFIG_DCCCONNECTTIMEOUT, DEF_DCCCONNECTTIMEOUT);
 TRANSFERTIMEOUT= config->readIntValue(CONFIG_DCCTRANSFERTIMEOUT, DEF_DCCTRANSFERTIMEOUT);
 fileHandler 		= -1;
 enviado 				= 0;
 socketEspera 	= NULL;
 socketCliente 	= NULL;
 notificaConexion = notificaDatos = NULL;
 blockSize 			= config->readIntValue(CONFIG_DCCBLOCKSIZE, DEF_DCCBLOCKSIZE);
 buffer 				= new char[blockSize];

 setFixedSize(DCCSEND_ANCHO, DCCSEND_ALTO);
 setCaption(DCC_SEND_WINDOW_TITLE);

 lNick = new QLabel("Nick:", this);
 lNick->setAlignment(AlignRight | AlignVCenter);
 lNick->setGeometry(MARCO, MARCO, DCCSEND_ANCHO_LABEL, ALTO_LABEL);
 eNick = new QLineEdit(this);
 if (nick != 0) eNick->setText(nick);
 eNick->setGeometry(MARCO + DCCSEND_ANCHO_LABEL + DISTANCIA, MARCO, width() - 2*MARCO - DCCSEND_ANCHO_LABEL - DISTANCIA, ALTO_LABEL);
 eNick->setFocus();

 lArchivo = new QLabel("File:", this);
 lArchivo->setAlignment(AlignRight | AlignVCenter);
 lArchivo->setGeometry(MARCO, MARCO + ALTO_LABEL + DISTANCIA, DCCSEND_ANCHO_LABEL, ALTO_LABEL);
 eArchivo = new QLineEdit(this);
 if (archivo != 0) eArchivo->setText(archivo);
 eArchivo->setGeometry(MARCO + DCCSEND_ANCHO_LABEL + DISTANCIA, MARCO + ALTO_LABEL + DISTANCIA, width() - 2*MARCO - DCCSEND_ANCHO_LABEL - DCCSEND_ANCHO_BOTON_ARCHIVO - DISTANCIA, ALTO_LABEL);
 buscarArchivo = new QPushButton("...", this);
 buscarArchivo->setGeometry(width() - MARCO - DCCSEND_ANCHO_BOTON_ARCHIVO, MARCO + ALTO_LABEL + DISTANCIA, DCCSEND_ANCHO_BOTON_ARCHIVO, ALTO_LABEL);

 lIpLocal = new QLabel("Local Ip:", this);
 lIpLocal->setAlignment(AlignRight | AlignVCenter);
 lIpLocal->setGeometry(MARCO, MARCO + ALTO_LABEL*2 + DISTANCIA * 3, DCCSEND_ANCHO_LABEL, ALTO_LABEL);
 ipLocal = new QLabel("--", this);
 ipLocal->setAlignment(AlignVCenter);
 ipLocal->setGeometry(MARCO + DCCSEND_ANCHO_LABEL + DISTANCIA, MARCO + ALTO_LABEL*2 + DISTANCIA*3, DCCSEND_ANCHO_LAB_IP, ALTO_LABEL);

 lIpRemota = new QLabel("Remote Ip:", this);
 lIpRemota->setAlignment(AlignRight | AlignVCenter);
 lIpRemota->setGeometry(width() - MARCO - DCCSEND_ANCHO_LABEL - DCCSEND_ANCHO_LAB_IP - DISTANCIA, MARCO + ALTO_LABEL*2 + DISTANCIA*3, DCCSEND_ANCHO_LABEL, ALTO_LABEL);
 ipRemota = new QLabel("--", this);
 ipRemota->setAlignment(AlignVCenter);
 ipRemota->setGeometry(width() - MARCO - DCCSEND_ANCHO_LAB_IP, MARCO + ALTO_LABEL*2 + DISTANCIA*3, DCCSEND_ANCHO_LAB_IP, ALTO_LABEL);

 lStatus = new QLabel(this);
 lStatus->setAlignment(AlignCenter);
 lStatus->setGeometry(MARCO, MARCO + ALTO_LABEL*3 + DISTANCIA*4, DCCSEND_ANCHO - MARCO, ALTO_LABEL);


 barra = new QProgressBar(100, this); // una escala de 0 a 100
 barra->setGeometry(MARCO, DCCSEND_ALTO - MARCO - ALTO_BARRA_PROGRESO - DISTANCIA - ALTO_BOTON, width() - 2*MARCO,  ALTO_BARRA_PROGRESO);

 conectar = new QPushButton("Send", this);
 conectar->setGeometry(MARCO, DCCSEND_ALTO - MARCO - ALTO_BOTON, (int)((DCCSEND_ANCHO-MARCO) / 2), ALTO_BOTON);
 cerrar  = new QPushButton("Cancel", this);
 cerrar->setGeometry((int)((DCCSEND_ANCHO - MARCO) / 2), DCCSEND_ALTO - MARCO - ALTO_BOTON, (int)((DCCSEND_ANCHO-MARCO) / 2), ALTO_BOTON);

 connect(buscarArchivo, SIGNAL(clicked()), SLOT(clickBuscarArchivo()));
 connect(conectar, SIGNAL(clicked()), SLOT(clickConectar()));
 connect(cerrar, SIGNAL(clicked()), SLOT(clickCerrar()));

 // inicio el temporizador
 startTimer(1000);

 show();
}

DccSend::~DccSend()
{
 delete [] buffer;
}

void DccSend::closeEvent(QCloseEvent *e)
{
 // ignoro el evento y hago como si hubieran hecho click en cerrar
 e->ignore();

 clickCerrar();
}

void DccSend::timerEvent(QTimerEvent *)
{
 if ((notificaConexion != NULL) && (timeout == 0))
 {
  // estaba esperando que se concrete la conexi�n pero se venci� el timeout
  #ifdef DEBUG_DCCSEND
   cout << "Se venci� el tiempo de espera para que se produzca una conexi�n" << endl;
  #endif
  ::close(fileHandler); fileHandler = -1;
  socketEspera->close();
  delete socketEspera; socketEspera = NULL;
  delete notificaConexion; notificaConexion = NULL;

  QMessageBox::warning(NULL, "Connection error", "Connection timed out, transfer aborted", "Exit");
  lStatus->setText("Connection timed out");
  return;
 }
 if ((notificaDatos != NULL) && (timeout == 0))
 {
  // estaba esperando que se concrete la conexi�n pero se venci� el timeout
  #ifdef DEBUG_DCCSEND
   cout << "El socket est� silente, conexi�n abortada" << endl;
  #endif
  ::close(fileHandler); fileHandler = -1;
  socketCliente->close();
  delete socketCliente; socketCliente = NULL;
  delete notificaDatos; notificaDatos = NULL;

  QMessageBox::warning(NULL, "Connection error", "Transfer timed out, transfer aborted", "Exit");
  lStatus->setText("Transfer timed out");
  return;
 }

 if (timeout > 0) timeout--;
}

void DccSend::clickBuscarArchivo()
{
 // muestra un dialogo de b�squeda de archivos y modifico el editLine si es neceario
 QString archivo = QFileDialog::getOpenFileName(obtenerHomePath());

 if ((archivo.isNull() == false) && (archivo != ""))
 {
  eArchivo->setText((const char *)archivo);
 }
 raise();
}

void DccSend::clickConectar()
{
 // verifico que los datos
 if (((eNick->text() == NULL) || (strlen(eNick->text()) == 0)) ||
     ((eArchivo->text() == NULL) || (strlen(eArchivo->text()) == 0)))
 {
  // alguno de los datos no es v�lido
  QMessageBox::warning(NULL, "Invalid data", "You have to fill the nick and file fields", "Retry");
  return;
 }

 // abro el archivo que voy a env�ar
 if ((fileHandler = open(eArchivo->text(), O_RDWR)) < 0)
 {
  #ifdef DEBUG_DCCSEND
   cout << "El fichero a enviar no existe o no se tiene permiso sobre el" << endl;
   cout << " - errno: " << strerror(errno) << endl;
  #endif
  QString mensaje = "";
  switch(errno)
  {
   case EACCES : mensaje = "You don't have permissions to read the file";
								 break;
   case ENOENT : mensaje = "The selected file doesn't exist!";
								 break;
   default     : mensaje = "Unknown error: ";
                 mensaje = mensaje + strerror(errno);
                 break;
  }
  QMessageBox::warning(NULL, "File error", (const char *)mensaje, "Retry");
  return;
 }
 // obtengo el tama�o del archivo y lo almadeno en una cadena
 struct stat fileInfo; // me va a servir para averigurar el tama�o del archivo
 fstat(fileHandler, &fileInfo);
 fileSize = fileInfo.st_size;

 // me fijo que el tama�o del archivo sea mayor que cero. Por la forma en que est� hecho dcc
 // no se puede transmitir un archivo que mida 0 bytes
 if (fileSize == 0)
 {
  // cierro el archivo
  ::close(fileHandler); // el :: es para que no se confunda con la funcion close(bool) del QWidget (DUH! :)
  // le aviso al usuario
  QMessageBox::warning(NULL, "Dumb error", "You can't send a 0 lenght file over DCC", "Retry");
  return;
 }

 #ifdef DEBUG_DCCSEND
  cout << "Abriendo puerto servidor" << endl;
 #endif

 // desabilito los dos textbox
 eNick->setEnabled(false);
 eArchivo->setEnabled(false);
 // deshabilito el bot�n conectar y el de buscar archivo
 conectar->setEnabled(false);
 buscarArchivo->setEnabled(false);

 // creo el socket que espera la conexi�n
 socketEspera = new IServerSocket();
 // hago un rastreo en los primeros 30 puertos desde el puerto DCC base, si
 // no logro abr�r ninguno de esos 30 doy error
 int puerto = -1, retListenAt = -1;
 for (int intento = 0; (intento < DCCPORTSCANMAX) && (retListenAt < 0); intento++)
 {
  puerto = DCCBASEPORT+intento;
  retListenAt = socketEspera->listenAt(puerto, 5, false);
 }
 if (retListenAt < 0) // no se pudo crear el puerto para escuchar
 {
  // se produjo un error
  #ifdef DEBUG_DCCSEND
   cout << "Se produjo un error al crear el socket que espera conexiones" << endl;
   cout << "Error: " << socketEspera->strError() << endl;
  #endif

  // cierro el archivo
  ::close(fileHandler); // el :: es para que no se confunda con la funcion close(bool) del QWidget (DUH! :)
  // le aviso al usuario
  QMessageBox::warning(NULL, "Connection error", "Can't create the server socket to listen for connections", "Exit");
  lStatus->setText("Connection error, transfer aborted");
  return;
 }
 notificaConexion = new QSocketNotifier(socketEspera->getSocket(), QSocketNotifier::Read, this);
 connect(notificaConexion, SIGNAL(activated(int)), this, SLOT(connectionQuery(int)));
 notificaConexion->setEnabled(true);

 // ya cree el socket maestro, ahora env�o el mensaje ctcp
 // para eso recurro a los mismos comandos que se env�an por una ventana
 // como cualquier hijo de vecino :)
 #ifdef DEBUG_DCCSEND
  cout << "Enviando CTCP de oferta de DCC" << endl;
 #endif
 QString strAddress; // la verdad no tengo ni idea de cuanto pueda ocupar
 QString strPort; // idem
 QString strSize;

 // la direcci�n tienen que ir (citando textualmente) como representaci�n ascii del
 // entero decimal formado al convertir los valores al orden de bytes en de host y
 // trat�ndolos como unsigned long y unsigned short respectivamente
 // Host order? y eso no es dependiente de la plataforma?
 // busco la direcci�n y el puerto del socket
 // la variable localIpAddress est� definida en QIrc.cpp
 strAddress.setNum(ntohl(localIpAddress.s_addr));
 strPort.setNum(puerto);
 // ya que estamos aprovecho y pongo la ip local en el label
 ipLocal->setText(inet_ntoa(localIpAddress));

 // pongo el tama�o del archivo en forma de texto
 strSize.setNum(fileSize);
 // muestro el tama�o en el pantalla
 QString strFileSize;
 strFileSize.setNum(fileSize);
 strFileSize = "Sending " + strFileSize + " bytes";
 lStatus->setText((const char *)strFileSize);

 char linea[strlen("/CTCP") + 1 + strlen((const char *)eNick->text()) + 1 + strlen("DCC") + 1 + strlen("SEND")
            + 1 + strlen((const char *)eArchivo->text()) + 1 + strlen((const char *)strAddress)
            + 1 + strlen((const char *)strPort) + 1 + strlen((const char *)strSize) + 1];

 // me aseguro de quitar la ruta de los nombres de los archivos
 char nombreSinRuta[strlen((const char *)eArchivo->text())+1];
 if (strrchr((const char *)eArchivo->text(), '/') != NULL)
 {
  // si hay alguna '/'
  strcpy(nombreSinRuta, strrchr((const char *)eArchivo->text(), '/')+1);
 } else {
  // si no hay ningun '/' (el nombre no tiene la ruta especificada, lo uso tal cual)
  strcpy(nombreSinRuta, (const char *)eArchivo->text());
 }
 sprintf(linea, "/CTCP %s DCC SEND %s %s %s %s", (const char *)eNick->text(), nombreSinRuta, (const char *)strAddress,
        (const char *)strPort, (const char *)strSize);

 #ifdef DEBUG_DCCSEND
  cout << "El comando es: " << linea << endl;
 #endif

 // emito el comando
 emit lineaComando(NULL, linea);

 // muestro en pantalla que es lo que estoy haciendo
 lStatus->setText("Waiting for connection...");
 // doy inicio al temporizador del timeout
 timeout = CONNECTTIMEOUT;
}

void DccSend::clickCerrar()
{
 // me fijo si alguno de los dos sockets est� abierto y lo cierro si es el caso
 // los notifier no es necesario borrarlos porque heredan a qobject
 if (socketEspera != NULL)
 {
  socketEspera->close();
  delete socketEspera; socketEspera = NULL;
 }
 if (socketCliente != NULL)
 {
  socketCliente->close();
  delete socketCliente; socketCliente = NULL;
 }

 emit dccSendCerrar(this);
}

void DccSend::connectionQuery(int s)
{
 // se gener� movimiento en el socket que estaba esperando, as� que acepto la conexi�n
 // y cierro el socket servidor. La conexi�n pasa a hacerse en el socket cliente
 #ifdef DEBUG_DCCSEND
  cout << "Un cliente se conect� al socket que espera, aceptando conexi�n" << endl;
 #endif
 notificaConexion->setEnabled(false); // para que no entre otra conexi�n m�s mientras muestro alg�n QMesageBox

 int ret = socketEspera->accept();
 if (ret < 0)
 {
  // se produjo un error al aceptar la conexi�n
  #ifdef DEBUG_DCCSEND
   cout << "Se produjo un error al aceptar la conexi�n" << endl;
   cout << "Error: " << socketEspera->strError() << endl;
  #endif

  // le aviso al usuario
  QMessageBox::warning(NULL, "Connection error", "Can't accept the connection!", "Exit");
  lStatus->setText("Connection error, transfer aborted");
 } else {
  // pongo el timeout de la conexi�n
  timeout = TRANSFERTIMEOUT;

  // creo el socket cliente con el numero de file handler que acabo de crear al aceptar
  socketCliente = new IClientSocket(); // no-bloqueante
  socketCliente->connect(ret, false);
  notificaDatos = new QSocketNotifier(socketCliente->getSocket(), QSocketNotifier::Read, this);
  connect(notificaDatos, SIGNAL(activated(int)), this, SLOT(dataAck(int)));
  notificaDatos->setEnabled(true);

  // muestro la ip del receptor
  struct sockaddr_in remoteAddr;
  unsigned int len = sizeof(sockaddr_in);
  getpeername(socketCliente->getSocket(), (sockaddr *)&remoteAddr, &len);
  ipRemota->setText(inet_ntoa(remoteAddr.sin_addr));

  // env�o el primer bloque de datos
  long bloque = read(fileHandler, buffer, blockSize);
  if (bloque < 0)
  {
   // se produjo un error al leer el archivo
   #ifdef DEBUG_DCCSEND
    cout << "Se produjo un error al leer el archivo a enviar" << endl;
   #endif

   ::close(fileHandler);  // el :: es para que no se confunda con la funcion close(bool) del QWidget (DUH! :)
   socketCliente->close();
   delete socketCliente; socketCliente = NULL; // PARA QUE CERRAR NO TRATE DE BORRARLO
   delete notificaDatos; notificaDatos = NULL;
   // le aviso al usuario
   QMessageBox::warning(NULL, "File error", "Can't read a block from the file!", "Exit");
   lStatus->setText("File error, transfer aborted");
  } else {
   socketCliente->write(buffer, bloque);
   enviado += bloque;
  }
 }

 // eliminio el qsocketnotifier del socket maestro
 delete notificaConexion; notificaConexion = NULL;
 // cierro el socket maestro
 socketEspera->close();
 delete socketEspera; socketEspera = NULL; // para que clickCerrar no trate de borrarlo
}

void DccSend::dataAck(int s)
{
 // pongo el timeout de la conexi�n
 timeout = TRANSFERTIMEOUT;

 // cuando recibo confirmaci�n del bloque anterior env�o el siguiente y actualizo el
 // progressBar
 U_4BYTES_INT recibido; // por si las dudas no cuadran los tama�os
 if(socketCliente->read((char *)&recibido, 4) <= 0) // incluyo los errores del socket
 {
  // se produjo un error al leer el archivo
  #ifdef DEBUG_DCCSEND
   cout << "El receptor cerr� el socket de forma prematura" << endl;
  #endif
  ::close(fileHandler); // el :: es para que no se confunda con la funcion close(bool) del QWidget (DUH! :)
  socketCliente->close();
  delete socketCliente; socketCliente = NULL; // para que clickCerrar no trate de borrarlo
  delete notificaDatos; notificaDatos = NULL;
  // le aviso al usuario
  QMessageBox::warning(NULL, "Connection error", "The client has closed the connection, transfer interrupted", "Exit");
  lStatus->setText("The client closed the connection, transfer aborted");
  return;
 }
 recibido = (U_4BYTES_INT)ntohl((unsigned long)recibido);

 // actualizo la barra
 barra->setProgress((int)((recibido * 100) / fileSize));

 if (recibido < enviado) return; // tengo que esperar a que le lleguen los bloques enteros
 if (recibido >= fileSize)
 {
  // ya termin�, cierro el socket
  #ifdef DEBUG_DCCSEND
   cout << "Transferencia DCC finalizada con �xito" << endl;
  #endif

  ::close(fileHandler); // el :: es para que no se confunda con la funcion close(bool) del QWidget (DUH! :)
  socketCliente->close();
  delete socketCliente; socketCliente = NULL; // para que clickCerrar no trate de borrarlo
  delete notificaDatos; notificaDatos = NULL;

  // le cambio el caption al boton cancelar y aviso que termin� la transferencia
  lStatus->setText("Transfer complete");
  cerrar->setText("Close");

  return;
 } else {
  // leo el siguiente bloque y lo envio
  long bloque = read(fileHandler, buffer, blockSize);
  if (bloque < 0)
  {
   // se produjo un error al leer el archivo
   #ifdef DEBUG_DCCSEND
    cout << "Se produjo un error al leer del archivo a enviar" << endl;
   #endif
   ::close(fileHandler); // el :: es para que no se confunda con la funcion close(bool) del QWidget (DUH! :)
   socketCliente->close();
   delete socketCliente; socketCliente = NULL; // para que clickCerrar no trate de borrarlo
   delete notificaDatos; notificaDatos = NULL;
   // le aviso al usuario
   QMessageBox::warning(NULL, "File error", "Can't read a block from the file!", "Exit");
   lStatus->setText("File error, transfer aborted");
   return;
  }
  long bEnv = 0; // puedo tener que hacer varias veces la escritura
  int ret = 0;
  while(bEnv < bloque)
  {
   ret = socketCliente->write(buffer+bEnv, bloque-bEnv);
   if (ret < 0)
   {
    // se produjo un error al leer el archivo
    #ifdef DEBUG_DCCSEND
    cout << "Se produjo un error al leer del archivo a enviar" << endl;
    #endif

    ::close(fileHandler); // el :: es para que no se confunda con la funcion close(bool) del QWidget (DUH! :)
    socketCliente->close();
    delete socketCliente; socketCliente = NULL; // para que clickCerrar no trate de borrarlo
    delete notificaDatos; notificaDatos = NULL;
    // le aviso al usuario
    QMessageBox::warning(NULL, "Connection error", "There's been an error while writing to the socket, transfer aborted", "Exit");
    lStatus->setText("Connection error, transfer aborted");
    return;
   }
   bEnv += ret;
  }
  enviado += bloque;
 }
}
